**Fecha de Creación:** 7 de Febrero de 2018

## Pre-Requisitos ##
* Tener configurada la VPN a traves de Check Point con Acceso a Jenkins, Bitbucket, Policy Center (Laboratorio y Producción)
* Tener permisos de aprobación en el Pipeline
* Tener permisos en el repositorio GIT
* Tener usuario para acceder a Policy Center Producción

## Pasos a Seguir ##
1.En el momento en el que se comunica que se inicia la regresión, el disponible debe validar el estado del pipeline en la rama de Quality, ya que la última ejecución de esta, es la versión que se desplegara en producción. El estado del pipeline debe estar en verde después de las pruebas de aceptación (*AcceptanceTest*) y debe estar a la espera de la aprobación por parte de TI (Stage *AskDeploy-TI* en el pipeline). Para validar esto se debe:

>**a.** Ingresar al Pipeline de Policy en la siguiente URL:
 https://jenkinscoreseguros.suramericana.com.co/blue/organizations/jenkins/Core_Seguros%2FPipeline%2FGuidewire%2FPolicyCenter/branches

>**b.** Pararse en la rama Quality y mirar el stage que se referencia en el punto 1 
           ![Manual Disponible Policy - Word-1.png](https://bitbucket.org/repo/akpjXAz/images/3292671653-Manual%20Disponible%20Policy%20-%20Word-1.png)
           
>**Observaciones:** Tener en cuenta que el Stage de *DeployStagging* puede fallar y aun así marcarse en verde, se debe validar en el detalle del Stage, si ocurrió algún error. 

>**-** El pipeline está programado para que en el caso de que fallen las pruebas de aceptación continúe, y es responsabilidad del equipo de calidad, dar el visto bueno para la salida a producción.

>**-** Si se hacen modificaciones de bases de datos a través del plugin database se debe poner el archivo properties en el servidor de stagging tantas veces se vaya a desplegar (Solo aplica para Stagging).
	
>**c.** Si no hay error y el Stage *AskDeploy-TI* se encuentra esperando aprobación, se debe validar si ya se tiene el aval de la regresión, si no se tiene este aval, se debe dejar que el Stage siga corriendo hasta que se agoté el tiempo de espera.
		
            i. Si el Stage AskDeploy-TI se encuentra en rojo, es porque se canceló o pasó  el tiempo para tomar una decisión. Recordar 
            que se debe aprobar una vez tengamos aprobación por parte de la regresión (Se informa por parte de los facilitadores a 
            través del chat de Whatsapp)

>**d.** En caso de que se encuentre algún error que no permite llegar al Stage AskDeploy-TI, se debe validar en el log la causa del error y escalarlo al equipo correspondiente:

            i. Si el error es de conexión en Jenkins, temas de Slave, el pipeline no corre, conexión a Git, se debe escalar al 
            disponible de Devops. En caso contrario no  se debe escalar nada a Devops.
		
            ii. Si es un error en DeployStaging o Rollback, se debe ver el log del Stage que falló y determinar si es un tema de 
            desarrollo o de infraestructura.
 
                1. Si es Desarrollo, se debe identificar el código que genera el error, de  ahí el analista que realizó el cambio y 
                notificarle a él y al facilitador para que sea corregido cuanto antes
			
                2. Si es un error de infraestructura, se debe identificar el error, viendo en el log del pipeline (parado en el stage 
                que falló y mirar el paso que se marca en rojo) o en la consola de Rundeck, si es un tema de despliegue, para 
                esto en el paso de despliegue que falló se da click en la ejecución de rundeck (Se ingresa con analislabo/analislabo
                como usuario/contraseña)
   ![2019-02-18 19_00_51-Manual Disponible Policy - Word-2.png](https://bitbucket.org/repo/akpjXAz/images/2067209939-2019-02-18%2019_00_51-Manual%20Disponible%20Policy%20-%20Word-2.png)
                
            iii. Una vez solucionado el problema, se debe lanzar o relanzar el pipeline de ser necesario, para darle continuidad al 
            proceso
			
                1. Si es un tema de desarrollo, se debe lanzar el release change que arregla el error y volver a hacer la validación 
                del punto 1. (Esta actividad la debe realizar el desarrollador que generó el error)
			
                2. Si es un tema de infraestructura, el disponible de infraestructura debe relanzar el paso que falló desde un 
                checkpoint del pipeline
			
                3. Si es un tema de Devops, el disponible de Devops debe relanzar el pipeline.
			
                4. Si le solicitan hacer el relanzamiento del pipeline, se debe relanzar desde el checkpoint inmediatamente anterior, 
                en nuestro caso las pruebas de aceptación, para esto debe ir a la vista clásica de Jenkins, como se muestra en la 
                siguiente imagen.
 ![2019-02-18 19_02_06-Manual Disponible Policy - Word-3.png](https://bitbucket.org/repo/akpjXAz/images/3437127754-2019-02-18%2019_02_06-Manual%20Disponible%20Policy%20-%20Word-3.png)
			
                5. Luego seleccionar la opción de checkpoint en el menú lateral
 ![2019-02-18 19_02_35-Manual Disponible Policy - Word-4.png](https://bitbucket.org/repo/akpjXAz/images/3785476904-2019-02-18%2019_02_35-Manual%20Disponible%20Policy%20-%20Word-4.png)
			
                6. Se le da play al checkpoint *UAT-Testing Done*
 ![2019-02-18 19_03_08-Manual Disponible Policy - Word-5.png](https://bitbucket.org/repo/akpjXAz/images/553088796-2019-02-18%2019_03_08-Manual%20Disponible%20Policy%20-%20Word-5.png)
			
                7. Se debe volver a la visual de Open Blue Ocean, y estar pendiente de la ejecución nueva.

2.Después que se notifique que se da el aval por parte de la regresión, se debe proceder a aprobar el Stage de *AskDeploy-TI* (Como se muestra la siguiente imagen) y comunicar a través del chat de Whatsapp de despliegue para que Calidad y negocio hagan sus aprobaciones respectivas.

![2019-02-18 19_03_32-Manual Disponible Policy - Word-6.png](https://bitbucket.org/repo/akpjXAz/images/1895079089-2019-02-18%2019_03_32-Manual%20Disponible%20Policy%20-%20Word-6.png)

>**a.** Si usted no posee permisos para realizar la aprobación, en primera instancia solicítele a un compañero que le colabore aprobando, ya que para poder aprobar tendrá que modificar el archivo PolicyCenter.properties que se encuentra en el proyecto git de Policy y agregarse en la lista de aprobadores de la siguiente forma: 

            i. En la raíz del proyecto se encuentra el archivo PolicyCenter.properties, como se muestra en la imagen
![2019-02-20 22_36_58-suracore _ policy_center — Bitbucket1.png](https://bitbucket.org/repo/akpjXAz/images/3757689226-2019-02-20%2022_36_58-suracore%20_%20policy_center%20%E2%80%94%20Bitbucket1.png)

            ii. Dentro del archivo, en los campos approversUserTI y approversMailTI, se debe agregar el usuario de red y el mail, 
            respectivamente.
![2019-02-20 22_39_01-suracore _ policy_center _ PolicyCenter.properties — Bitbucket2.png](https://bitbucket.org/repo/akpjXAz/images/1446831510-2019-02-20%2022_39_01-suracore%20_%20policy_center%20_%20PolicyCenter.properties%20%E2%80%94%20Bitbucket2.png)

           ii. Después de realizar esto, se debe hacer un commit, Pull Request y mergear para que corra de nuevo el pipeline y se 
           active el usuario para aprobación

           Nota: Si se realiza modificación de las properties de la aplicación los checkpoints no cargarán la configuración realizada en este caso se debe relanzar el pipeline desde el principio.

3.Después de la aprobación por parte de negocio se debe validar que el Stage del *GitPromotion* se ejecute correctamente, como se muestra en la imagen.

 ![2019-02-18 19_03_56-Manual Disponible Policy - Word-7.png](https://bitbucket.org/repo/akpjXAz/images/2505508335-2019-02-18%2019_03_56-Manual%20Disponible%20Policy%20-%20Word-7.png)
	
>**a.** Si se presenta un error de credenciales o permisos de GIT se debe contactar al disponible de Devops o de        arquitectura de ser necesario.

>**b.** Si se presenta un problema de conflictos de clase, se debe llevar a cabo el procedimiento definido en el        manual para hacer merge, que se puede encontrar en el siguiente link de la Wiki [Manual para resolver conflictos de merge desde SourceTree](https://bitbucket.org/suracore/corewiki/wiki/Manual%20para%20resolver%20conflictos%20de%20merge%20desde%20SourceTree)
		
            i. Si al tratar de resolver los conflictos, estos son muy complejos, se debe identificar al último desarrollador(es) 
            que modificó la clase y solicitarle que solucione o le indique como solucionar los conflictos.
                
            ii. Una vez se hayan soluciona los conflictos y se haya hecho el Pull Request, se debe solicitar al disponible de 
            arquitectura la aprobación y el merge.
		
            iii. Cuando el disponible de arquitectura confirme la ejecución del paso anterior, se debe ejecutar automáticamente el 
            pipeline de la rama de master.
		
            iv. Cuando aparezca la pregunta "El ajuste es para un paso normal de producción con regresión? (Nota: si es un Hotfix, 
            por favor dar en cancelar)" se debe seleccionar la opción Proceder, ya que es una salida normal y se debe garantizar 
            que los toggles pasen correctamente a producción 
	
>**c.** Si no se presentan errores en el *GitPromotion* debe terminar la ejecución del pipeline en verde (Cómo se muestra la imagen anterior) y se debe lanzar automáticamente el pipeline de la rama de master.
	
>**d.** En el pipeline de la rama de master, se debe esperar a que la ejecución llegue hasta el Stage de *WaitDeploy* cómo se muestra en la imagen. Después de validar que quede en dicho Stage, se debe  revisar la hora que tenga  el archivo properties "deployStartHourPdn0" sea correcta y confirmar en el chat de Whatsapp que el despliegue quedó agendado.

 ![2019-02-18 19_04_23-Manual Disponible Policy - Word-8.png](https://bitbucket.org/repo/akpjXAz/images/2628334140-2019-02-18%2019_04_23-Manual%20Disponible%20Policy%20-%20Word-8.png)
		
            i. En caso de que falle alguno de los Stage anteriores al WaitDeploy se debe validar con los disponibles que correspondan
			
                1. Si el pipeline corre los Stage UnitTest y Static Analysis and Integrity, se debe validar si es por errores en 
                desarrollo, si es así se debe validar la prueba con el objetivo de determinar el código que hace que falle y 
                reportarlo al analista responsable y al facilitador. Si falla por temas de pipeline (Variables, conexión, Slave,
                cache) se debe reportar al disponible devops
			
                2. Si falla el Stage Artifactory Publish, se debe validar si es un tema de conexión, en caso de ser así, reportar al 
                disponible de infraestructura.

                3. Si falla en los Stage Setup, GitPullStatic, GitPullModule y Build, por temas de conexión a Git, permisos o Shell,
                reportar al disponible de Devops

                4. Si por alguna razón solicitaron cancelar el despliegue y luego se tiene que relanzar para agendarlo de nuevo, 
                se debe relanzar desde el CheckPoint Build Done, siguiendo los pasos que se indican en el numeral 1 apartado d (iii/4)

4.El día del despliegue a producción, se debe validar que en primera instancia el stage de *Deploy* corra correctamente y luego que los Toggles se hayan aplicado correctamente
	
>**a.** Si sale un error en el Stage de *Deploy*, seguir los pasos detallados del numeral 1, paso d(ii)
	
>**b.** Para validar si los toggles se aplicaron o no, se debe validar en el Stage de *Deploy*, en la actividad de rundeck que se encuentra después del Undeploy y antes del deploy, como se muestra en la imagen. Si dicha actividad indica que el "Proceso de sincronizacion terminado con exito" es por que se aplicaron correctamente los toggles. Si sale un error en dicha actividad, comunicarse con los disponibles de Devops e Infraestructura

![2019-02-19 18_58_50-jenkins _ Core_Seguros_Pipeline_Guidewire_PolicyCenter _ master _ #31.png](https://bitbucket.org/repo/akpjXAz/images/3284123904-2019-02-19%2018_58_50-jenkins%20_%20Core_Seguros_Pipeline_Guidewire_PolicyCenter%20_%20master%20_%20%2331.png)

            i. Cuando se solucione el inconveniente, uno de estos disponibles debe relanzar el pipeline para garantizar que la 
            ejecución termine completamente o informar que se puede proceder con la validación del funcionamiento de policy.
	
>**c.** Una vez que se valide que se desplegó correctamente policy en producción, se debe validar que la aplicación está disponible, para esto se debe ingresar a la URL de policy en producción https://coreseguros.suramericana.com/pc/PolicyCenter.do