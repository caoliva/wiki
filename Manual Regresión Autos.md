MANUAL DE REGRESIÓN AUTOS

**1.**	Al comenzar con la regresión los escenarios del proceso (s) a ejecutar deben estar en estado Por Hacer, excepto los bloqueados por ajuste y en Automatización.

![imagen1.png](https://bitbucket.org/repo/akpjXAz/images/1043947160-imagen1.png)

**2.** Cuando se encuentra un bug asociado al escenario que se está ejecutando debe hacerse lo siguiente:

 2.1    Durante cada regresión debe existir un desarrollador designado para solucionar bugs (Procesos financieros, Reaseguro, Reaseguro de cobros, Policy center, GC)

     •  El QA debe analizar el origen del bug, en caso de que sea un bug del mismo equipo debe reportarse el bug al encargado durante la regresión de solucionar bugs de regresión y que pertenece al equipo.

     •  Si el QA desconoce el origen de bug puede guiarse por el aplicativo en donde encontró el bug y reportárselo al disponible de aplicativo.

     •  El escenario con el bug reportado debe pasar a estado Bloqueado por ajuste hasta que sea solucionado.



 2.2    Cuando se tenga claridad de quién debe revisar el bug se  crea de la siguiente manera:
![imagen2.png](https://bitbucket.org/repo/akpjXAz/images/1563180071-imagen2.png)

• Proyecto: Core de Seguros - Mejora Continua (CDSPDN)

• Tipo de Incidencia: Error

• Origen del error: Regresión

• Resumen: resumen del error encontrado

• Equipo - Mejora Continua: Regresión manual

• Descripción: descripción del error encontrado

• Informador: Nombre del QA que reporta el bug.

• Responsable: Nombre del Desarrollador responsable de regresión que se encargará de solucionar el bug.

• Crear, el número del bug debe asociarse al escenario en el cual se encontró el bug de la siguiente manera:

![imagen3.png](https://bitbucket.org/repo/akpjXAz/images/550401505-imagen3.png)

![imagen4.png](https://bitbucket.org/repo/akpjXAz/images/700346615-imagen4.png)


**3.** El QA debe analizar junto con el analista de negocio si es un bug inyectado o existente. Cuando el análisis este hecho se debe asociar al bug las siguientes etiquetas:

       •	Si es un bug inyectado debe llevar la etiqueta “BugNuevo”

       •	Si es un bug existente debe llevar la etiqueta “BugExistente”

       •	Independientemente del bug todos los bug deben llevar la etiqueta “BugRegresión”

![imagen5.png](https://bitbucket.org/repo/akpjXAz/images/153647245-imagen5.png)

**4.** El QA debe estar pendiente del estado en el que se encuentra el bug y es el desarrollador el encargado de actualizar el estado del Bug en Jira, si el QA vuelve a probar y el bug continua debe reabrirse por parte del QA e informar al desarrollador.

**5.** Los escenarios ejecutados deben pasar por lo estados del tablero, al finalizar la regresión los escenarios que están en done deben quedar en estado “Con regresión”, excepto los escenarios bloqueados por ajuste y en Automatización.

![imagen5.png](https://bitbucket.org/repo/akpjXAz/images/2957321061-imagen5.png)


NOTAS:

• Al comenzar la regresión esta será informada por medio de correo electrónico y por medio de canales de slack indicando el día de inicio y fin de la misma.

• La regresión manual es ejecutada durante tres días con una versión estable lo que implica (No certificación de historias, No toggle en estado diferente a como se van a ir a producción)

• El QA debe estar en constante dialogo con el facilitador para hacer análisis de bug y para que exista una colaboración para la solución de dicho bug.

• El QA es el encargado de adicionar nuevos escenarios a los procesos de regresión de acuerdo a las historias del sprint.