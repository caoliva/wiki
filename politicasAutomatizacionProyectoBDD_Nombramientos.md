# NOMBRAMIENTOS #

[Volver al menu](politicasAutomatizacionProyectoBDD)

##PAQUETES
El nombrar un paquete por favor tenga en cuenta las siguientes características:

Característica  | Ejemplo correcto                     | Ejemplo incorrecto            
:---------------|:-------------------------------------|:------------------------------
Formato: **minúscula** sostenida |billingcenter |billingCenter
Formato: sin espacios ni caracteres separadores |asesoriaventa|asesoria_venta
Estructura: para nombrar **carpetas de Runner** se recomienda usar||
palabra "lote"|lote|Lote
número consecutivo|1|01
nombre de la solución|autos|AutosIndividual
nombre de la funcionalidad que se ejecutan las pruebas contenidas|polizanueva|PolizaNueva
Ejemplo completo:|lote1autospolizanueva |Lote01AutosIndividualPolizaNueva

### CLASES ###
- El nombre de una clase debe estar en español. Ejemplo correcto: "Poliza" (clase dentro del paquete de Modelos) | Ejemplo incorrecto: "Coverage" (clase dentro del paquete de Modelos)

- En el nombramiento de una clase se exceptúa los siguientes sufijos: "Runner", "Definition", "Step", "Page" y "Factory" (éste último asociado a clases relacionadas con la asignación de valores previo a un consumo de servicios). Ejemplo correcto: "ExpedicionPolizaPpalEmpresarialFactory", "RenovacionPolizaAutoIndividualDefinition" | Ejemplo incorrecto: "ExpedicionMasterEmpresarialFactory", "CoverageBasicPlanDefinition"

- En el nombramiento de una clase se exceptúan aquellos nombres que son generadas (de forma automática) para serializar objetos (como por ejemplo: la estructura de un JSon o un XML). Ejemplo correcto: "PerdidaClaimsInformationLoss" (clase relacionada con la serialización de un objeto JSon)

- El nombramiento de cualquier clase debe ser: nemotécnico, diciente, descriptivo y representativo; debe estar en singular; No debe contener verbos; y se recomienda el uso de sustantivos. Ejemplo correcto: "InformacionTomador" | Ejemplo incorrecto: "InformacionTomadores", "RegistrarTomador"

- El nombramiento de cualquier clase debe estar en formato UpperCamelCase (no hacer uso de guión bajo "_" u otro caractér para separar las palabras que componen el nombre). Ejemplo correcto: "SolicitudCancelacionPolizaColectivaExpedida" | Ejemplo incorrecto: "solicitudCancelacionPOLIZA", "solicitudcancelacionpolizacolectivaexpedida", "Solicitud_CancelacionPoliza"

- En los nombres de clase No se debe incluir la palabra: “Escenario” ni la palabra: “Transversal”. Ejemplo: "EscenarioCancelacionPolizaColectiva", "TransversalCancelacionPolizaColectiva"

- El nombre de las clases asociadas a runner, definitions, steps o page deben tener los sufijos: "Runner", "Definition", "Step" o "Page" - respectivamente (y en singular). Ejemplo correcto: "CancelacionFacturacionAseguradoRunner", "CancelacionFacturacionAseguradoDefinition", "CancelacionFacturacionAseguradoStep", "CancelacionFacturacionAseguradoPage" | Ejemplo incorrecto: "CancelacionFacturacionAseguradosRunner", "CancelacionFacturacionAseguradoPages"

- Los nombres de las clases PAGES deben estar asociados a la pagina web que se mapee. Es decir, si la vista que se automatiza se llama "DATOS PERSONA", entonces el Page tendrá un nombre como: DatosPersonaPage. Tener en cuenta que este nombramiento debe ser: nemotécnico, diciente y estar relacionado con la pantalla con la que se interacción. Ejemplo: sí la pantalla es de Datos Complementarios; se debe evitar crear un Page con el nombre de: "DatosComplementariosPage" ya que no es diciente.

- Los nombres de las clases asociadas a modelos deben corresponder con el nombre del objeto en el mundo real.

### ATRIBUTOS ###
- Los nombres de los atributos de clase deben estar en español (excepto los atributos que sean propios de las clases generadas y asociadas a los POJO de proyectos externos como por ejemplo: servicesConector - consumo de servicios). Ejemplo: en las clases xxxxxFactory (relacionada con el consumo de: servicesConector) es normal y aceptado un atributo de clase nombrado como: periodEnd; sin embargo en una clase de tipo Page o propia del proyecto BDD no debe presentarse un atributo nombrado en otro idioma.

- Los nombre de los atributos de clase debe estar en formato lowerCamelCase (no hacer uso de guión bajo "_" u otro caractér para separar las palabras que componen el nombre). Ejemplo correcto: valorBonificacionEsperado | Ejemplo incorrecto: revisarValorbonificacionesperado

- El nombramiento de cualquier atributo de clase debe ser: nemotécnico, diciente, descriptivo y representativo; No debe contener verbos; y se recomienda el NO uso de abreviaturas específicas que atenten contra el entendimiento del atributo por parte de otro commiter. Ejemplo correcto: nroPoliza, vlrAsegurado | Ejemplo incorrecto: contratoCP, coberturaPPAutos

### MÉTODOS ###
A continuación se enuncian las características que debe y NO debe poseer el nombramiento:

Característica | Ejemplo correcto                     | Ejemplo incorrecto            
:------------------------ |:-------------------------------------|:------------------------------
Usar idioma __español__         |enviarSolicitudPolizaMaster|enviarSolicitudPolicyMaster
         ||consumirConsultaDatosPolizaRiesgo|consumirConsultaDatosPolicyRisk
Usar formato __lowerCamelCase__ |ingresarInformacionTomador             |IngresarInformacionTomador
Comenzar por __verbo en infinitivo__|consultarListaRiesgosPoliza|consultaListaRiesgosPoliza
        |expedirPolizaColectiva|expedicionPolizaColectiva
|actualizarRiesgosPolizaColectiva|editarGuardarRiesgosPolizaColectiva
|editarRiesgosPolizaColectiva|editarRiesgosPolizaColectivaGuardarDatos
__Descriptivo y Diciente__: Indicar de forma natural la __función que cumple el método__, facilitando el uso o reuso por cualquier persona nueva en el proyecto|obtenerDatosPoliza|obtenerRiesgosPolizaEmpresarial
|diligenciarComisionPactadaPolizaEmpresarial |diligenciarValorComision
Nemotécnico: Que permita su __fácil recordación__ (y reutilización)|ingresarInformacionContacto|intentarIngresoInformacionContacto
|||
__NO__ incluir tipo de objeto |guardarAsegurado| clickGuardarAsegurado
|obtenerEstrategiaComercial |obtenerChkEstrategiaComercial
__NO__ incluir detalles de implementación|obtenerListaPolizasRiesgo | obtenerElementosListadosPolizasRiesgo
|activarIngresoInformacionTomador | seleccionarTomador
__NO__ usar guiones, espacios o cualquier caracter separar las palabras que componen el nombre| diligenciarDatosBeneficiarioOneroso | diligenciarDatosBeneficiario_Oneroso
__NO__ usar Artículos, preposiciones o conectores (En, Para, La, Le, El, De, A, Al, Desde etc)|verificarCantidadRiesgosPolizaMaster|verificarCantidadRiesgosPorPolizaMaster
|obtenerDatosDesdeCSV|obtenerDatosCSV


> **Notas importantes**:

>- Cuando se trata de un **método genérico**: evitar palabras que indiquen especificidad sobre algún: modelo, área, frente de trabajo, tipo de póliza o similar.

>- El **nombre de los parámetros de entrada** de un método deben estar en español. Deben ser dicientes, nemotécnicos y descriptivos según el dato que contiene el parámetro. Debe estar en formato lowerCamelCase, y no se debe usar guión u otro separador entre palabras. Ejemplo correcto:  nroEmpleados, valorBonificacionTecnica | Ejemplo incorrecto: numberEmployees, valor, split_productos.

>- En caso que se presente el uso de artículos, preposiciones o conectores (Estándar Clean Code): el autor debe argumentar dicho uso.



### CONSTANTES Y ATRIBUTOS ENUM ###
A continuación se enuncian las características que debe y NO debe poseer el nombramiento:

Característica | Ejemplo correcto                     | Ejemplo incorrecto            
:------------------------|:-------------------------------------|:------------------------------
Usar idioma __español__ y __Mayúscula__ sostenida         |PRIMA|Prima, prima, Formatear_Montos
Usar SOLO "guión bajo" ( _ ) como __separador__ de palabras|FORMATEAR_MONTOS|Formatear-Montos
__NO__ usar caracteres especiales NI caracteres que generen deuda técnica|VALOR_PRIMA_ANIO|VALOR_PRIMA_AÑO, NRO_COTIZACIÓN_AUTOS
__Descriptivo y Diciente__: Indicar de forma natural el valor almacenado en la constante definida, facilitando la reutilización de la constante por cualquier persona nueva en el proyecto|PORCENTAJE_PRIMA_MINIMA_POLIZAS_TRANSPORTE|PORCENTAJE_PRIMA_MíNIMA
||
Para constantes relacionadas con __variables de sesión__ usar el formato:|              |
Inicie con el prefijo: "SESION_" |SESION_|SESSION_
Luego indique las iniciales aplicativo desde donde se captura el valor almacenado |GC_             |GCENTER_
Luego indique el nombre de la constante (tener en cuenta todas las políticas prevías para el nombramiento de constantes)|NRO_POLIZA_MASTER|NRO_POLIZA
__Ejemplo__ completo:|SESION_GC_NRO_POLIZA_MASTER |GC_NRO_POLIZA_MASTER


### FORMATO PARA NOMBRAR ATRIBUTOS EN UN ENUM ###

Característica | Ejemplo correcto                     | Ejemplo incorrecto            
:------------------------|:-------------------------------------|:------------------------------
Inicie con las iniciales del aplicativo donde aplica el valor almacenado (en caso que aplique)|GC_             |GCENTER_
Luego indique el nombre que identifique a cuál Enum pertenece el atributo |PROCESOBATCH_|PBATCH_
Luego indique el nombre del atributo enum (tener en cuenta todas las políticas prevías para el nombramiento de constantes)|GENERACION_FACTURAS |FACTURAS
__Ejemplo__ completo de un atributo del Enum Nombre Procesos Batch |BC_PROCESOBATCH_AUDITORIA|PROCESO_FACTURAS
__Ejemplo__ completo (cuando No aplica nombre del aplicativo) de un atributo del Enum Formatos|FORMATO_FECHA_DMY|FECHA_DMY


### VARIABLES ###
- El nombre de las variables deben estar en español y deben ser: dicientes, nemotécnicas y descriptivas según el dato que almacena la variable. Para el nombramiento se debe usar formato lowerCamelCase y No usar guión bajo u otro separador. Ejemplo correcto: fechaMovimiento, listaProductos | Ejemplo incorrecto: FechaMovimiento, splitProductos


### RELACIONADO CON CSV ###
- El nombre de un archivo CSV debe ser en minúscula sostenida, comenzar con el prefijo: "datos_" y las palabras que componen el nombre deben estar separadas por guión bajo "_". Ejemplo correcto: datos_vehiculo | Ejemplo incorrecto: datosVehiculo

- Los archivos CSV deben tener por lo menos las siguientes columnas: "id" (que sirve como conteo de los registros) e "idfiltro" (que corresponde a la columna por la cual se filtrarán los registros).

- Los valores que se ubiquen en la columna "idfiltro" deben ser: descriptivos, nemotécnicos, dicientes y que faciliten identificar las características que posee la línea de datos prueba que se están usando. Ejemplo correcto: tomador riesgo PEP | Ejemplo incorrecto: tomador01

- Los valores que se ubiquen en la columna "idfiltro" No deben incluir la palabra: “Escenario” ni la palabra: “Transversal”. Ejemplo correcto: tomador riesgo PEP | Ejemplo incorrecto: escenario fraude version2

- El nombre de las columnas contenidas en el archivo CSV deben ser: dicientes, nemotécnicas y descriptivas. Para el nombramiento se debe usar formato lowerCamelCase y No usar guión bajo u otro separador. Ejemplo correcto: tipoArticulo, nombreSede | Ejemplo incorrecto: TipoArticulo, sede


### FEATURE / HISTORIAS DE USUARIO ###

A continuación las pautas para el nombramiento de archivos .Feature [ historias de usuario ]

Característica | Ejemplo correcto                     | Ejemplo incorrecto            
:--------------|:-------------------------------------|:------------------------------
Debe ser: **Diciente** y **descriptivo** según las funcionalidades objeto de prueba||
Debe ser: **Nemotécnico**||
Se recomienda iniciar por un verbo en infinitivo||
Debe estar en: minúscula **sostenida**||
Usar **guión bajo** "_" como separador de palabras|financiar_poliza_multiriesgo.feature|financiacion_poliza_multiriesgo.feature
| |financiarPolizaMultiriesgo.feature
| |Financiar_poliza_multiriesgo.feature
**No usar espacios en blanco** en el nombre|expedir_poliza_nueva_patrimonial.feature|expedir poliza nueva patrimonial.feature


### CARACTERÍSTICA / FUNCIONALIDAD INCLUIDA DENTRO DEL .FEATURE ###

A continuación las pautas para describir el valor de la "Característica:" [ ítem incluido dentro de archivos .Feature ]

Característica | Ejemplo correcto                     | Ejemplo incorrecto            
:--------------|:-------------------------------------|:------------------------------
Debe ser: **Diciente** y **descriptivo** según los escenarios de prueba contenidos dentro del .feature||
Debe ser: **Nemotécnico**||
Debe estar en: minúscula **sostenida**||
Longitud máxima de: **50 caracteres**||
**No usar letras tildadas** ni caracteres especiales||


### ESCENARIOS / ESCENARIOS DE PRUEBA ASOCIADOS A UNA CARACTERÍSTICA ###

A continuación las pautas para describir el nombre de un escenario de prueba

Característica | Ejemplo correcto                     | Ejemplo incorrecto            
:--------------|:-------------------------------------|:------------------------------
Debe ser: **Diciente** y **descriptivo** según el proceso y/o funcionalidad que es objeto de prueba||
Debe ser **entendible** por las personas del negocio||
**No usar letras tildadas** ni caracteres especiales||
Recomendación: Evitar dividir el nombre en dos o más líneas||



A continuación las pautas para describir el nombre de las columnas asociadas a los datos de prueba

- El nombre de las columnas en la sección del Example ("Ejemplos") debe ser: nemotécnico y diciente para el negocio (evitar el uso de camelCase y guión bajo). Ejemplo:

![nombramiento_columnas_CSV_20181227.png](https://bitbucket.org/repo/Lodj655/images/3423652533-nombramiento_columnas_CSV_20181227.png)


### ELEMENTOS WEB ###
- Para el nombramiento de elementos web se sugiere la siguiente lista:

![nomenclatura_elementos_web.PNG](https://bitbucket.org/repo/akpjXAz/images/2411349699-nomenclatura_elementos_web.PNG)

### **Excepciones** ###
- Se permite el uso de palabras en idioma inglés solo sí la palabra al ser traducida al español: difiere del concepto que entiende el negocio. Esta excepción aplica para palabras incluidas en el nombramiento de: clases, atributos, métodos, parámetros de método, y variables. Ejemplo correcto: nroPolizaMaster | Ejemplo incorrecto: numberEmployees



# PENDIENTE POR REVISAR Y/O APROBAR SU CONTINUIDAD EN LA WIKI #

- revisión de tabla de nomenclatura para nombres de elementos web

- saber si se incluye la nueva pauta de: usar prefijos para el tipo de dato para: atributos de clase y definición de variables o instancias de clase (objetos de clase). Ejemplo: usar "str" para variables de tipo String

- **NOMBRAMIENTO DE STEPS PARA COTIZADOR AUTOS Y EMPRESARIAL**
* Si es una clase del proyecto de **Autos** el nombre de la clase debe comenzar con el prefijo "Autos". Ejemplo: AutosDatosTomadorStep.java
* Si es una clase de **Empresarial** el nombre de la clase debe comenzar con el prefijo "Empr". Ejemplo: EmprDatosTomadorStep.java

*Lo anterior con el fin de evitar archivos duplicados dentro de los paquetes que puedan generar conflictos en el merge con los archivos.*

[Volver al menu](politicasAutomatizacionProyectoBDD)