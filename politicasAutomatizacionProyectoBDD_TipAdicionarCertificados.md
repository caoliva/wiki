## AGREGAR CERTIFICADOS DE UNA APLICACIÓN ##

[Volver al menu](politicasAutomatizacionProyectoBDD)

Si al momento de lanzar una ejecución por consola lanza un mensaje de error como el siguiente

![image.png](https://bitbucket.org/repo/Lodj655/images/2166063104-image.png)

Significa que falta instalar los certificados de la aplicación sobre la cual se desea ejecutar la prueba. 

Para esto se debe hacer lo siguiente:

Descargar el certificado de la aplicación en el ambiente al cual se le desea aplicar la prueba. Para esto se abre el link en el navegador y al lado izquierdo de la URL donde se encuentra el certificado de seguridad (SSL) se da click. Este desplegara unas opciones y en la parte de abajo encontramos la ocpión certificado con la palabra **válido** en azul. Damos click en esta opción.

![1.png](https://bitbucket.org/repo/Lodj655/images/1895652849-1.png)

Esta opción nos abre el siguiente cuadro de dialogo con unas opciones

![2018-08-30_10h06_36.png](https://bitbucket.org/repo/Lodj655/images/3198965960-2018-08-30_10h06_36.png)

En esta ventana vamos a seleccionar la pestaña Detalles, en la parte inferior seleccionamos la opción **Copiar en archivo** esto nos abre un asistente para exportar el certificado de la aplicación, seguimos los pasos como este nos lo indica teniendo en cuenta que la opción para exportar el certificado es: **DER binario codificado X.509 (.CER)**, seleccionamos la ruta donde vamos a exportar el certificado y finalizamos. 

Esto nos creara un archivo en la ruta seleccionada con extensión .CER el cual es el que vamos a importar. 

Para importarlo. Vamos a abrir la consola de windows (ejecutando como administrador) en esta vamos a ejecutar la siguiente linea: 

"C:\\Program Files\Java\jdk1.8.0_171\bin\keytool.exe" -import -v -noprompt -trustcacerts -storepass changeit -keystore "C:\\Program Files\Java\jdk1.8.0_171\jre\lib\security\cacerts" -file ruta/del/certificado/certificado_pc.cer -alias pc_certificado

**IMPORTANTE** La versión del JDK depende de la que tenga instalada en su equipo.

ruta/del/certificado/certificado_pc.cer es la ruta donde se guardo el certificado y el nombre guardado.

El alias puede ser cualquier nombre que desee. Se recomienda un nombre relacionado a la aplicación.

Una vez realizado esto tendrá un mensaje de que el certificado ha sido agregado y se puede ejecutar las pruebas que antes fallaban por falta del certificado.