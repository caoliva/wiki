# CONSIDERACIONES GENERALES #

[Volver al menu](politicasAutomatizacionProyectoBDD)

## **PULL REQUEST (PULLR)** ##
A continuación se describe una serie de características a tener en cuenta antes, durante y luego de solicitar un pullR:

### CONSTANTEMENTE tenga presente:

Característica | A tener en cuenta
:--------------|:-----------------------
Analizar, planear y estructurar el pullR|**Objetivo**: planear la funcionalidad que se automatizará, y planificar un pullR acorde a las políticas vigentes.
Consultar y descargar los últimos cambios de la rama de develop.|**Objetivo**: evitar inconvenientes asociados a la desactualización frente a los ajustes de otros compañeros.
Estar actualizado con la información de la wiki.|**Objetivo**: tener claridad de las políticas vigentes.


### ANTES DE COMENZAR UNA AUTOMATIZACIÓN tenga presente:

Característica | A tener en cuenta
:--------------|:-----------------------
Analizar las posibles formas de automatizar el escenario.| Ejemplo: diagramar, consultar, solicitar asesoria, observar mejores prácticas, entre otros.
Consultar sí ya pre-existe artefactos que puedan ser reutilizados en la automatización que se iniciará.|**Objetivo**: Evitar desarrollar artefactos ya desarrollados.
Se recomienda instalar la herramienta de Sonar en local (y configurar las políticas de SonarQube Sura).|Revisar [aquí](InstalacionSonarQubeLocal) el manual para la instalación.


### DURANTE LA AUTOMATIZACIÓN tenga presente:

Característica | A tener en cuenta
:--------------|:-----------------------
Cumplir con las pautas de la wiki|
Recordar que el límite de archivos contenidos en un pullR es de **15**.|Para el conteo de archivos límite **Sí** se contabilizan: clases, enums, feature.
|Para el conteo de archivos límite **NO** se contabilizan: clases POJO, ni runner, ni archivos eliminados, ni archivos CSV, ni de configuración (como por ejemplo: Build.gradle)
|Nota: __TODO tipo de archivo debe ser revisado__.
Se debe crear clases Definitions por cada Feature|Nota: *Esto puede que de algún modo pueda generar duplicidad de código; sin embargo es un tema que está en estudio*.
**NO** crear Definitions genéricos|En caso que sea netamente necesario y en coordinación con el revisor: **solo** se podrá crear Definitions genéricos para pasos relacionados con la instrucción GIVEN.


### AL FINALIZAR LA AUTOMATIZACIÓN tenga presente:

Característica | A tener en cuenta
:--------------|:-----------------------
**Antes de** hacer **PUSH** de la rama: Ejecute el escenario automatizado por lo menos una (1) vez en el ambiente donde se ejecutará. Esto con el fin de reducir una falla en al momento de una ejecución automática.|Usar por Ejemplo: gradle clean test -Denv=**uat** aggregate [Para mayor detalle ver el README del proyecto]


### ANTES DE SOLICITAR UN PULLR tenga presente:

Característica | A tener en cuenta
:--------------|:-----------------------
Compilar el proyecto usando el comando de gradle: gradle compileJava|o ejecutar: gradle clean assemble
Ejecutar por lo menos un (1) escenario (runner) para confirmar que los cambios contenidos en el pullR **NO impacten negativamente** la ejecución de escenarios pre-existentes (Para cualquier solución contenida en el proyecto BDD):|1. Sí solo se impactaron archivos de una determinada solución: Por favor ejecutar por lo menos un (1) runner de dicha solución y que abarque cambios contemplados en el pullR
|2. Sí se impactaron archivos de **varias** soluciones: Por favor ejecutar por lo menos un (1) runner de cada solución y que abarque cambios contemplados en el pullR


### DURANTE LA REVISIÓN DE UN PULLR tenga presente:
>En el caso que, entre el reviewer y el autor de un pull Request (pullR); se llegue al acuerdo de realizar un determinado ajuste a futuro; entonces el reviewer ***creará y asignará una tarea al autor***; la cual debe ser ejecutada y aprobada antes que se le pueda revisar y/o aprobar un próximo pullR


### LUEGO DE UN MERGE SURGIDO DE UN PULLR tenga presente:

Característica | A tener en cuenta
:--------------|:-----------------------
Compilar el proyecto usando el comando de gradle: gradle compileJava| o ejecutar: gradle clean assemble
|La ejecución del o de los runner NO debe ser completa. Cada autor (commiter) es libre de pausar o cancelar cada ejecución al observar que el cambio incluido en el pullR **NO impactó negativamente** la ejecución de escenarios pre-existentes.

> El límite de archivos **SOLO se puede exceder** cuando el pullR sea asociado a un refactor que contempla una **única y exclusiva** característica modificada. A continuación se presenta un ejemplo:

Ejemplo correcto | Ejemplo incorrecto 
:----------------|:-----------------------
Sí el pullR excede la cantidad límite de archivos y dicho pullR se relaciona con la generación de una enumeración que reemplazará el uso de un conjunto de constantes.|Sí el pullR excede la cantidad límite de archivos y dicho pullR se relaciona con el renombramiento de un método **y adicional** se incluye el renombramiento de una variable de sesión. 

## POLÍTICAS PARA DECLINAR UN PULLR ##
A continuación se enuncian los criterios para declinar un pullR:

Criterio | Descripción
:--------|:----------------------
Cantidad de archivos| Exceder, en un mismo pullR; la cantidad límite de archivos permitidos por cada pullR.
Cantidad de refactor| Cuando en un mismo pullR se incluye 2 o más refactor.
Cantidad de comentarios| Poseer 10 o más comentarios, en un mismo pullR; que impliquen correcciones/ajustes originados por faltas a la wiki y/o a buenas practicas [NO se contabilizan los comentarios hechos como: recomendaciones, sugerencias u oportunidades de mejora]


## CLASES ##
A continuación se enuncian las características a revisar en un pullR con respecto a las Clases:

Característica | A tener en cuenta
:--------------|:-----------------------
Deuda técnica  | Toda clase nueva incluida en un pullR debe estar libre de deuda técnica (según las políticas de SonarQube Sura).
Buena práctica| Toda clase debe cumplir con el principio SOLID.
Buena práctica| No es permitido el uso de clases o métodos obsoletos [ver: Tabla de métodos Obsoletos]
Buena práctica| Evitar usar import de la forma **import org.apache.*** [ en su lugar adicione la librería  específica que se va a utilizar en el desarrollo como por ejemplo: **import org.apache.cxf.service.Service**;
Implementación capas BDD |Desde una clase **Page** NO se permite la invocación de otro Page. Se recomienda hacer un análisis para que NO ocurra esta invocación.
Implementación capas BDD |Desde una clase **Step** NO se permite la invocación de otro Step. Se recomienda hacer un análisis para que no ocurra esta invocación. 
|**Nota**: En caso que el análisis indique una probable causa para usar un método Step dentro de otro Step, el Autor debe argumentar dicha implementación.
Implementación capas BDD |Una clase **Step** NO debe contener métodos que solo invoquen a un solo métodos de una clase Page. Es decir: un método incluido en una clase Step debe estar enfocado a denotar el paso a paso para ejecutar una acción; y por ende: se espera que el método contenga la invocación a varios métodos de una clase Page.
|**Nota**: En caso que se encuentre un método que falte a la presente pauta: se solicitará al autor del pullR la argumentación que soporte la implementación realizada.
Implementación capas BDD|Una clase **Page** NO debe contener métodos que implementen un paso a paso. Es decir: un método incluido en una clase Page debe estar enfocado a un solo comportamiento.
|**Nota**: Por lo anterior un método en una clase Page NO debe incluir líneas/sentencias de forma consecutiva que indiquen un paso a paso para ejecutar una acción y en la que se cuente con la interacción con 2 o más objetos (este tipo de lógica se asemeja a un método de una clase Step).
|** EXCEPCIÓN**: En caso que se vea la *posibilidad* de definir un método Page que agrupe la interacción de **unos pocos** objetos, se debe **analizar MUY bien** su implementación sin atentar contra los principios SOLID.
|Ejemplo **correcto**: Dado que se tienen los campos "txtDatoBuscar" y un botón "btnEjecutarBusqueda", y se tiene definido por el negocio que el comportamiento **siempre** es: diligenciar un valor a buscar y solamente se activa la consulta mediante clic en el botón de "ejecutar búsqueda"; entonces se puede pensar en tener un solo método Page para hacer esta interacción de forma conjunta. [ ver imagen 1 - tabla de Imágenes ]
|Ejemplo **incorrecto**: Dado que se tienen 3 campos relacionados con la dirección de residencia de una persona: "txtPais", "txtDepartamento" y "txtCiudad", y se tiene definido por el negocio que el campo "Ciudad" **puede** llegar a aparecer diligenciado en pantalla de forma predeterminada según el departamento diligenciado; pero que a su vez es un campo editable; entonces NO se debería pensar en tener un solo método Page para hacer esta interacción de forma conjunta.


## MÉTODOS ##
A continuación se enuncian las características a revisar en un pullR con respecto a los métodos:

Característica | A tener en cuenta
:--------------|:-----------------------
Deuda técnica  | Todo método impactado (editado/ajustado) incluido en un pullR debe estar libre de deuda técnica (según las políticas de SonarQube Sura)
|**Nota**: Lo anterior aplica **incluso** para métodos creados por un Autor diferente al que solicita el pullR.
Buena práctica| Toda método debe cumplir con el principio SOLID.
Buenas prácticas y wiki| Cualquier método editado durante el proceso de automatización debe cumplir con las buenas prácticas y con la wiki del proyecto.
|**Nota**: Lo anterior aplica **incluso** para:
|1. Métodos creados por un Autor diferente a quien realiza la edición o ajuste en el método.
|2. Métodos aprobados en un pull Request (pullR) previo. En este caso: el autor del pullR podría solicitar que el comentario específico o revisión del pullR hecho por un reviewer sea realizado por otro reviewer


### ASERCIONES
A continuación se enuncian las características a revisar en un pullR con respecto al uso de aserciones:

Descripción| Ejemplo correcto | Ejemplo incorrecto
:----------|:-----------------|:--------------------
En las aserciones se debe usar los métodos relacionados con la librería de: "org.hamcrest" y No las de la libreria: "org.junit.Assert". Esto debido al aprovechamiento de las ventajas de la primera libreria.|MatcherAssert.assertThat("El valor es diferente al esperado", verificacionPage.verificarValor(strValor))); | assertTrue("El valor no es correcto", verificacionPage.verificarValor(strValor));


## DATOS EMBEBIDOS ("QUEMADOS"):
A continuación se enuncian las características a revisar en un pullR con respecto al uso de información embebida:

Característica | Descripción | Ejemplo
:--------------|:------------|:-----
No es permitido el uso de **Tiempos embebidos**|
Recomendaciones sobre la espera de objetos en pantalla antes de interactuar con éstos:|Nunca esperar por un tiempo fijo (entiéndase como "fijo" el uso de tiempos quemados o valores dados por una Constante); en su lugar: esperar por una condición (ejm: .isCurrentVisible())
|Puede esperar que un elemento esté disponible en una página:|withTimeoutOf(10, ChronoUnit.SECONDS).waitFor(By.cssSelector("#city");
|Puede usar un método en la clase WebDriver de ExpectedConditions:|waitFor(ExpectedConditions.presenceOfElementLocated(By.name("city")));
Evitar al máximo **datos embebidos** dentro de una clase.|Tratar en lo posible de evitar los datos embebidos en el código, en caso de que sea estrictamente necesario favor argumentar la razón por la cual se "quema" el dato mediante la etiqueta **//TODO:** seguida de una aclaración (explicación).|//TODO: Pendiente reemplazar dato

>NOTA: El uso de la etiqueta **//TODO:** se recomienda usar cuando se genere un método y presente una implementación nula, o parcial.


## REVISIÓN DE LÓGICA DE PROGRAMACIÓN ##
Aunque la revisión de la lógica implementada en un determinado método está ligado a un caracter muy subjetivo y dependiente de varios factores, *como por ejemplo del: conocimiento de negocio, especificidad del método, entre otros*; a continuación se citan algunos escenarios donde es **importante que se realice una revisión de la lógica implementada** :

Criterio | Descripción           |Imagen relacionada
:--------|:----------------------|:-----------------:
Cuando se asume el comportamiento o estado "válido" de un objeto en pantalla|Esto ocurre por ejemplo cuando sobre un objeto que puede presentar 2 o más estados durante la prueba (Ejemplo: una casilla de verificación): se realiza una acción sin contemplar dichos estados (Ejemplo: seleccionada o sin selección)
Cuando se incurra en la automatización inconsciente de una falla de la aplicación|Esto ocurre por ejemplo cuando se ejecuta una acción sobre un objeto sólo sí el objeto está presente en la pantalla.|ver imagen 2 - tabla de Imágenes
|También puede ocurrir cuando se implementa sentencias que conllevan a continuar con la ejecución sin pensar en el comportamiento normal que debe tener la aplicación (Ejemplo: dar clic en dos o más ocasiones a un botón para que éste efectúe su función, realizar una acción que permita refrescar/visualizar objetos cuando no es el comportamiento esperado del objeto)|
|Otro ejemplo puede ser cuando se implemente un condicional "IF" sin sentencia "ELSE" y que la importancia del comportamiento para una posible sentencia "ELSE" sea relevante para la prueba.|
|Otro ejemplo es cuando se implementa una sentencia "SWITCH" sin un caso por defecto ("default:")|
Cuando se afecte el objetivo de cada capa del proyecto BDD|Esto ocurre por ejemplo cuando en la capa de Definitions se implementa una lógica que indique el "Cómo" se realiza la ejecución. En este caso puntual: la lógica debe ser delegada a la capa de Steps|
Cuando se implementen líneas innecesarias|Esto ocurre por ejemplo cuando se invocan métodos que tienen un retorno de datos; pero no se captura o no se usa dicho resultado.|imagen 3 - tabla de Imágenes

> **Nota aclaratoria**: El autor del pullR, *en cualquiera de los casos* ; puede argumentar su implementación debido a cualquier comentario realizado por algún revisor; y entre ambos se debe llegar a la mejor solución.


## CREACIÓN DE ENUM Y/O CONSTANTES ##
A continuación se enuncian las características a revisar en un pullR con respecto al uso de valores constantes:

Característica | Observación         
:--------------|:-------------------------------------
__NO__ adicionar una constante que ya pre-exista|Antes de agregar una nueva constante: Por favor __buscar la pre-existencia__ del valor constante que se requiere.
__NO__ adicionar una constante que su valor haga parte de un conjunto de posibles valores|En lugar de crear la constante realice lo siguiente:
|- Consulte sí pre-existe un Enum donde pueda agregar un atributo que posea el valor constante que se requiere.
|- Evalue sí la creación de un Enum facilita el uso del valor constante que se requiere y otros más que sean de la misma índole (o agrupación).
Ejemplos __incorrectos__|
<private/public> static final String FORMATO_FECHA_DMA = "dd/MM/yyyy";| El valor constante puede hacer parte de un Enum que almacene formatos de fecha (o formatos en general)
<private/public> static final String TERMINACION_CON_ERRORES = "Terminación con Errores";| El valor constante puede hacer parte de un Enum que almacene los diferentes estados de un proceso
|
Usar un modificador apropiado para una constante|Si una constante sólo se usa en una clase: declare la constante con un modificador privado
Ubicar las constantes según su modificador|Si una constante sólo se usa en una clase: declare la constante en esa clase.
Un Enum debe crearse por lo menos con 3 atributos|


##EXCEPCIONES

- Crear excepciones propias en los Steps (si es necesario) y en los Pages, para que los errores sean más claros en el reporte de Serenity y que queden en el log del proyecto usando **Logger.error**. Estas excepciones deben ir especificamente en los objetos que causen impacto en el negocio y no a todos los objetos.

### ------------------------------------------------------------------------------ ###

## **Tabla de imágenes** ##
Nombre imagen   | Imagen           
:---------------|:----------------------
imagen 1|![info_metodo_Page_interaccion_conjunta_2_objetos_20190710.PNG](https://bitbucket.org/repo/akpjXAz/images/2961337114-info_metodo_Page_interaccion_conjunta_2_objetos_20190710.PNG)
imagen 2| ![condicional_que_puede_ser_perjudicial_20190220.png](https://bitbucket.org/repo/akpjXAz/images/3182461876-condicional_que_puede_ser_perjudicial_20190220.png)
imagen 3| ![invocar_metodo_con_return_sin_capturar_resultado_20190307.PNG](https://bitbucket.org/repo/akpjXAz/images/1519184669-invocar_metodo_con_return_sin_capturar_resultado_20190307.PNG)



## **Tabla de métodos Obsoletos** ##
A continuación se presentan las clases o métodos que NO pueden ser usados debido a su obsolescencia:

Obsoleto | Correcto | Razón de obsolescencia
:--------|:---------|:----------------------
/steps/general/GenericsStep.getFilasModelo|/utils/UtilidadesCSV.obtenerDatosPrueba|*La función que presta el método es una función más util a nivel de proyecto que puede ser usada en cualquier Step (paso), y no un Step (paso) que tenga que ser invocado desde un Definitions o desde otro Step*.
/utils/EnumConstantes|Adicione la nueva **constante** según sea su uso [*Ejemplo*: sí es verdaderamente una constante (de clase o de proyecto) o hace parte de una Enumeración (nueva o pre-existente)]|*La clase posee valores constantes que corresponden a una enumeración*.


[Volver al menu](politicasAutomatizacionProyectoBDD)