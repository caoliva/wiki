# Query Validator

[Volver al home](Home)


Estas son los métodos sobre los cuales el QueryValidator revisa ejecución:
>**first**​. Este método debe ser reemplazado por FirstResult, esto debido a que el FirstResult internamente solo trae el primer resultado brindando así menor carga al sistema.

>**toList, toSet, toTypedArray, toCollection​**. Estos métodos convierten el IQueryBeanResult en alguna colección trayendo de manera inmediata todos los resultados. Se recomienda en su lugar iterar (*.each* o *for*) el IQueryBeanResult.

>**firstWhere, where, hasMatch, countWhere​**. El uso de estos métodos se da para condiciones que no fueron evaluadas en los queries, de esta manera teniendo que acceder a más registros de los realmente necesarios. Se debe agregar las respectivas condiciones al query para un mejor rendimiento y menor carga del sistema. En caso del **countWhere** despues del *.select()* se puede hacer un *.Count*.

>**partition​, subtract, maxBy​**. Estos métodos se pueden suplir relizandolos en los queries directamente con subconsultas, in, not in, order by.

>**iterator, each, for, map​**. Estos métodos quedan como Failures, no van a ser bloqueantes pero se debe revisar si dentro de la iteración de los resultados se está realizando evaluación de condiciones o alguna transformación que se pudo realizar desde el select (por ejemplo traer solo un campo).



[Volver al home](Home)