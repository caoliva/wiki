#PASOS PARA LA AUTOMATIZACIÓN DE PRUEBAS POR SERVICIO A PARTIR DEL SERVICE CONECTOR

[Volver al menu](politicasAutomatizacionProyectoBDD)

## IMPORTANTE ## Si el consumo del servicio hace parte de una prueba transversal únicamente debe hacerse llamado del step que realiza el consumo en el Definition. Este servicio no tendria un .feature, runner y definition independiente. Esto solo sucede si es una prueba por servicio que se ejecuta **(Ejemplo Tarifa autos individuales)**

### Los siguientes pasos son para la creación del consumo de un servicio desde el Service Conector ###

* Las capas esenciales que debe tener un servicio dentro de la estructura BDD es igual a la utilizada para realizar una automatización por front pero se reemplaza la capa **Pages** por la capa **Services**. Ya que en este tipo de pruebas no tenemos una vista a la cual apuntar para consumir un servicio.

# Paso 1.

Crear un feature con la redacción del escenario al que se le aplicara el consumo del servicio, adicional crear el Definition relacionado al feature. 

Crear un archivo CSV con la data correspondiente a los escenarios que se desean probar por medio del servicio. **RUTA test/resource/data/dato_nombre_csv.csv**. Tener en cuenta el valor para realizar el filtrado se hace por medio de la columna idFiltro.

**IMPORTANTE** El CSV se crea para datos específicos del servicio. Ya que parte de la data puede ser enviada por medio de otros archivos CSV ya creados. Ejemplo. Para tarifa de Autos. Se puede enviar la información del vehículo desde el CSV datos_vehiculo.csv.

![2018-08-28_14h56_14.png](https://bitbucket.org/repo/Lodj655/images/3157086192-2018-08-28_14h56_14.png)

*(Imagen de ejemplo)*

# Paso 2.
Sea que se haya usado archivos CSV ya existentes o creado uno nuevo es recomendable tener un modelo al cual relacionar la información de este archivo y centralizar la data para el Request que se usará para consumir el servicio. El modelo en su constructor debe estar relacionando los atributos con el nombre de la columna definida en el archivo CSV para que la data quede en una lista del tipo del modelo creado.

![csv modelo.png](https://bitbucket.org/repo/Lodj655/images/35531066-csv%20modelo.png)

*(Imagen de ejemplo)*

        - Definimos la variable donde vamos almacenar el valor del nuevo campo. Ejemplo* private String Descuento;*
        - Se hace el llamado del valor del campo del archivo en el constructor y lo almacenamos en la variable creada en el paso anterior. 
          Ejemplo: *this.descuento = datosTarifa.get("descuento");*
        - En la misma clase TarifaAuto.java definimos sólo el get (Los Set no son necesarios). Ejemplo
             public String getDescuento() 
             {
                   return this.descuento;
              }
# Paso 3 

Colocamos el nuevo valor del campo en los objetos del servicio en la clase TarifaAutoFactory.java Definimos la variable donde vamos almacenar el valor del campo. Ejemplo* private String descuentoRiesgo;*  En la clase TarifaAutoFactory.java creamos los setter y getters del campo definido en el paso anterior.Ejemplo:

public String getDescuentoRiesgo(){return this.descuentoRiesgo;}
public void setDescuentoRiesgo(String descuento){this.descuentoRiesgo=descuento;}

# paso 4 #. Consumimos los valores en ConsumoServicioTarifaAutoPage.java. 
         - Invocamos los métodos de get y set realizados en los pasos anteriores para tomar 
           y colocar los valores del campo configurado en el archivo csv. Ejemplo:  
           tarifaAutoFactory.setDescuento(otarifa.getDescuento());

# paso 5 #. Significado de los campos en el archivo. 

- **Id**: identificador de la fila. 
- **Lobs**: Linea de negocio, en este caso es autos
- **valorCotizar**: Valor del activo que se pretende cotizar
- **Method**: La accion que pretendemos solicitar al servicio; en este caso le enviamos "rate" para tarifar
- **Version**: Version del json 
- **TarifaTotal**: Valor de la prima que pretendemos cobrar. Valor a comparar con lo que devuelva el servicio
- **Fasecolda**: Valor de fasecolda con que pretendemos tarifar
- **Año**: Año en que el vehículo salio de la planta de producción
- **CiudadCirculacion**: Código de la ciudad donde el vehículo circula. Estos códigos los podemos consultar en:
https://bitbucket.org/suracore/bddcoreservicios/src/develop/Tarifa/Base%20Parametrizacion.txt

- **Accesorios**: Valor de los accesorios del vehículo
- **BonificacionTecnica**: Valor de la bonificación tecnica (perfil)
- **BonificacionComercial**: Valor de la bonificación comercial(perfil)
- **ValorLimiteRC**: Valor limite de la responsabilidad civil
- **CoberturaDañosPP**: Valor de la cobertura de daños mínimo deducible
- **CeroKms**: Si el vehículo es cero kms. variable boleana
- **ValorHurtoParcial**: Valor parcial en caso de hurto
- **ValorHurtoTotal**: Valor total en caso de hurto
- **ValorHurtoGasTrans**: Valor de gastos de transporte en caso de hurto
- **ValorDañosCarroTrans:Valor** de transporte en caso de daños al carro,
- **ValorDañosMinDeducible**: Valor de deducible daños a terceros

# paso 6 #. Uso del enum produccion\util\ValoresServicio.java y el diccionario de parámetros (valueDictionary)

- **Para usar el **enum** Se coloca las constantes  que no se envían como parámetro en los escenarios del archivo csv. Por ejemplo:VALOR_PA_HURTO_COB("PAHurtoCov") y se invoca en la clase TarifaAutoFactory.java así: ValoresServicio.VALOR_PA_HURTO_COB.getValue()

- **Para usar el diccionario(clave, valor) **valueDictionary** de los parámetros que se encuentran en el archivo data\datos_tarifa_autos.csv se debe tener en cuenta que en el archivo está la clave y en el TarifaAuto.java se encuentra la estructura (valueDictionary) con el valor (códigos de policy center). En el archivo .csv colocamos el valor que será la clave y sobre la clase colocamos la pareja. Ejemplo:

![img2.png](https://bitbucket.org/repo/Lodj655/images/4158044772-img2.png)

...y en la clase configuramos el valor así:

![img3.png](https://bitbucket.org/repo/Lodj655/images/1196352397-img3.png)