## **PRUEBAS DE RENDIMIENTO**

Las pruebas de performance se realizan con el objetivo de eliminar degradación en el rendimiento de la aplicación y así mitigar riesgos en ambientes productivos. Con estas pruebas se analiza el desempeño en velocidad, escalabilidad y estabilidad de la aplicación a partir de métricas como tiempos de respuesta, procesos ejecutados por segundo y tasa de errores a peticiones realizadas en la aplicación, además es importante monitorear el consumo de IO, CPU y memoria de la infraestructura.


### **TIPOS DE PRUEBA**

1. **Pruebas de carga:** Éste es el tipo más sencillo de pruebas de rendimiento. Una prueba de carga se realiza generalmente para observar el comportamiento de una aplicación bajo una cantidad esperada de peticiones. Esta carga puede ser el número esperado de usuarios concurrentes, utilizando la aplicación que realizan un número específico de transacciones, durante el tiempo que dura la carga. Esta prueba puede mostrar los **tiempos de respuesta** de todas las transacciones importantes de la aplicación. También se monitorizan otros aspectos como el comportamiento de transacciones a bases de datos, el desempeño del servidor de aplicaciones, etc., para así encontrar los cuellos de botella de la aplicación.

2. **Pruebas de stress:** Este tipo de prueba se utiliza normalmente para conocer la máxima capacidad de procesamiento de la aplicación. En esta prueba se va aumentando el número de usuarios que consumen la aplicación hasta que esta llega a su limite y se rompe. Este tipo de prueba se realiza para determinar la **solidez de la aplicación** en los momentos de carga extrema. Esto ayuda a los administradores para determinar si la aplicación rendirá lo suficiente en caso de que la carga real supere a la carga esperada.

3. **Pruebas de rendimiento:** Consiste en verificar la capacidad de una aplicación al escalar cualquiera de sus características, como por ejemplo la carga que soporta, número de transacciones, volúmenes de datos, entre otros. En primera instancia se prueba con niveles de demanda bajos, y poco a poco se incrementa los niveles de demanda y finalmente prueba con altos niveles de carga. De esta manera se puede determinar que tan bien **escala la aplicación** y los problemas que comienzan a surgir en distintos niveles.

#### Cuando se deben realizar las pruebas?
- Se crea una nueva funcionalidad en la aplicación que aumenta la cantidad de transacciones y peticiones.
- La aplicación cambia su infraestructura (Cambio de servidores, base de datos, actualización de versiones, ...)
- Migración de infraestructura a la nube (Prueba sobre infraestructura en la nube deshabilitando auto-escalamiento).
- La aplicación aumenta la cantidad de usuarios concurrentes o el volumen y flujo de datos en la plataforma.
- Cambios en el flujo de negocio.
- La aplicación genera documentación o informes masivamente.
- Se desea conocer los límites de IO, CPU y consumo de memoria en la infraestructura al ejecutar la aplicación.
- Se desea conocer si la aplicación realiza uso eficiente de los recursos de infraestructura.

### **METODOLOGÍA**

En el momento en el que se realiza el análisis de los atributos de calidad, el QA o el líder de TI debe hablar con su equipo y plantear la necesidad de la prueba, Esto se puede dar al encontrar alguno de los casos anteriormente mencionados y así crear una HU para el diseño, implementación y ejecución de la prueba. Es responsabilidad de los líderes de TI/Calidad velar que en la planeación de las pruebas se contemplen las pruebas de rendimiento.


### **ROLES**


- **Diseñadores:** Genera listado de componentes a probar.
- **Diseñadores y Devops:** Prioriza los componentes.
- **Diseñadores, Devops y Negocio:** Diseña y prioriza los escenarios de pruebas.
- **Diseñadores, Devops y Negocio:** Selección de tipo de prueba.
- **Diseñadores y negocio:** genera set de datos
- **Diseñadores/Desarrolladores/QA:** Implementa y construye .jmx se guarda en bitbucket en repositorio performance en la carpeta de la app
    * los que se realizan por demanda
    * los que se realizan semanal
- **Devops:** Aprovisiona el ambiente. Job en jenkins por aplicación
- **Diseñadores/Desarrolladores/Infraestructura:** Monitoreo de la ejecución.
- **QA/Desarrolladores/diseñadores:** revisa ejecución y analiza el reporte
- **Desarrollador:** Solución de errores


[Proceso y Roles](https://drive.google.com/uc?export=view&id=1ZDqALuBSkzhlj1tX1SwLq-AdA4vqFZnQ)

##### TIPO DE PRUEBA:
Es necesario identificar los flujos de negocio cómo funcionan a nivel de integraciones y componentes internos, de acuerdo a esto, se deberá realizar algunas pruebas desde la parte funcional que garanticen el flujo completo con escenarios que permitan medir el performance de la aplicación.

Se deben realizar preguntas de negocio para conocer el comportamiento de la aplicación:
- Existiran epocas de alta demanda? Existirán picos? (Cambios muy altos) (Prueba de estrés)
- La aplicación tiene cantidad de usuarios estándar con un crecimiento fijo en tiempos largos?(Prueba de carga)
- Existen horarios de mayor demanda? (Prueba de performance )
- Crecen las transacciones en la aplicación gradualmente en tiempos muy cortos? (Prueba de performance)
- Aplicaciones que son internas por procesos internos de negocio y que aumenta demanda gradualmente en el tiempo (prueba de performance) 
- Aplicaciones a las que acceden usuarios externos (prueba de stress ) 
- Aplicaciones que cuentan con volúmenes y flujos altos de datos (Prueba de carga)
- Cuántos son los usuarios máximos que soporta la aplicación?(Prueba de estrés)


Para los componentes que se identifique alta prioridad de pruebas independientes, se deben generar pruebas de rendimiento contemplando la siguiente información tanto para horario normal como para horario pico
- Cantidad de agentes
- Cantidad de transacciones esperadas
- Tiempo de respuesta esperado


##
1. **Identificar los criterios de aceptación de rendimiento**. Determinar el tiempo de respuesta, el rendimiento, la utilización de los recursos y los objetivos y limitaciones. En general, el tiempo de respuesta concierne al usuario, el rendimiento al negocio, y la utilización de los recursos al sistema.  Identificar cuáles serían criterios de éxito de rendimiento del proyecto para evaluar qué combinación de la configuración da lugar a un funcionamiento óptimo. Se debe tener en cuenta que las pruebas de rendimiento se ejecutan sobre el ambiente de laboratorio el cual cuenta con el **30%** aproximadamente de los recurosos de producción, lo que quiere decir que los usuarios a probar debería ser el 30% de los esperados en producción.
2. **Set de datos para probar**. Basados en lo anterior, con el 30% de los usuarios esperados en Producción para probar, calculamos que se debería tener el doble para los registros que conforman el set de datos. Por ejemplo, si en producción se esperan 100 usuarios, deberíamos probar con 30 usuarios, y para el set de datos deberíamos tener como minimo 60 registros para probar. Lo anterior aplica para las pruebas de **carga** y de **rendimiento**, para las pruebas de **stress** deberíamos probar con la cantidad máxima posible de datos para probar.

3. **Planificar y diseñar las pruebas**. Identificar los principales escenarios, determinar la variabilidad de los usuarios y la forma de simular esa variabilidad, definir los datos de las pruebas y establecer las métricas a recoger. 
4. **Configurar el entorno de prueba**. Preparar el entorno de prueba, las herramientas y recursos necesarios para ejecutar cada una de las estrategias, así como las características y componentes disponibles para la prueba. 
5. **Aplicar el diseño de la prueba**. Desarrollar las pruebas de rendimiento de acuerdo con el diseño del plan.
6. **Ejecutar la prueba**. Ejecutar y monitorear las pruebas. Validar las pruebas, los datos de las pruebas y recoger los resultados. 
7. **Analizar los resultados**. Analizar los datos, en el caso que se identifiquen falencias en el servicio por tiempos de respuesta se debe realizar el respectivo ajustes y volver a priorizar el resto de las pruebas y a ejecutarlas. Cuando todas las métricas estén dentro de los límites aceptados, ninguno de los umbrales establecidos hayan sido rebasados y toda la información deseada se ha reunido, las pruebas han acabado para el escenario definido por la configuración.

### **HERRAMIENTA**

La herramienta para realizar las pruebas de rendimiento es Jmeter en la versión 4.0 o superior.

El instructivo se encuentra [aqui](ManualPruebasRendimiento).