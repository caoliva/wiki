**TOMA DE EVIDENCIAS**

Para facilitar la toma de evidencias,en especial los casos en que se dificulta detallar los pasos para reportar el error, se recomienda que mientras se este llevando a cabo la ejecución de pruebas, en la máquina se este ejecutando la herramienta en toma de vídeo.

La herramienta a utilizar es **Screen2swf**, de la cuál se puede resaltar que permite toma de vídeos por largos tiempos, es posible generar imágenes a partir de sus vídeos.

Tanto la herramienta como el manual de uso puede descargarse de: https://www.screen-record.com/screen2exe.htm