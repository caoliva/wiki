# CONSIDERACIONES GENERALES #

[Volver al menu](politicasDesarrolloPipeLineAsCode)

## Tiempos de Respuesta revisión de Pull Request ##
- Se tendrá máximo una semana para dar respuesta sobre la revisión de un Pull Request.
- Se tendrá máximo dos días para responder un comentario.

> Nota: Estos tiempos serán solo durante el proceso de sincronizanción, e ir bajando estos tiempos.

## Aprobadores de Pull Request ##
- Se debe aprobar por mínimo dos personas, uno de cada equipo.

## Modelo de Merges Pull Request ##
- Los merge se realizaran de un branch hacia master del repositorio.
- Los ajustes sobre comentarios en pull request se realizara mediante actualizacion del pull request.

> Recordar consultar y/o descargar **constantemente** los últimos cambios de la rama de master.


[Volver al menu](politicasDesarrolloPipeLineAsCode)