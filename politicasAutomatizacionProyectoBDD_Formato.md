# FORMATO #

[Volver al menu](politicasAutomatizacionProyectoBDD)

## CLASES ##

- No debe existir líneas vacías dentro del contenido de los métodos.

- Debe existir un (1) solo salto de línea entre cada método.

- Debe existir dos (2) espacios entre el último import y la declaración de la clase.

- Debe existir un (1) solo espacio después de la declaración del package

- Debe existir un (1) solo espacio entre cada declaración de los steps, pages u otra anotación. Tal como se muestra en la siguiente imagen:

![imgSteps.png](https://bitbucket.org/repo/Lodj655/images/1490119585-imgSteps.png)

- No debe existir comentarios al interior de los archivos. Hacer uso de ellos en caso estrictamente necesario.


## FEATURE ##

Característica | Observación         
:--------------|:-------------------------------------
Gherkin | La redacción de cualquier escenario de prueba (incluyendo consumo de servicios web): debe ser a nivel de negocio ya que el fin de este lenguaje es que cualquier persona (con o sin conocimiento técnico) pueda interpretar qué se está probando.
Ortografía| La redacción de cualquier elemento en un archivo *.feature (narrativa, nombre escenario, descripción del escenario, datos prueba, entre otros) debe estar libre de errores de ortografía.
Ortografía| Los datos de prueba relacionados en un archivo *.feature deben estar libres de errores de ortografía.
| **Por lo anterior**, Cuando existan datos de prueba acentuados :
|Se debe analizar las posibles opciones de "cómo" implementar la aserción del **dato esperado** (dato prueba en *.feature) versus el **dato obtenido** (durante la ejecución de la prueba).
|Evite al máximo el uso de una utileria que elimine los acentos de un *dato de prueba* antes de la comparación con el *dato obtenido* de la ejecución.
|En caso de dudas sobre la implentación de la aserción de datos con acento; el revisor debe despejar las dudas con el Autor del pull request, y así hallar la mejor implementación.

[Volver al menu](politicasAutomatizacionProyectoBDD)