**REPORTE DE BUGS EN JIRA**

[Volver al menu](PolíticasReporteBugs)

Todos los bugs deben quedar reportados en la herramienta JIRA, en el tablero que tenga asignado cada uno de los equipos de trabajo.

El reporte de un bug se puede llevar a cabo de dos maneras,** de acuerdo a la complejidad del mismo:**

**Errores que se generan en una prueba extensa o que son algo complejos de describir:**

Cuando se están ejecutando pruebas que pueden ser complejas de correr nuevamente, ya sea por extensas, complejidad del escenario, entre otras, se recomienda tener activo en pantalla una grabación para que facilite la toma de evidencia en el momento en que se genere error.

En este caso el reporte del bug en jira debe llevar lo siguiente:

1. **Resumen(campo propio de jira):** resumen concreto del error. Con el formato
[App+ "-" + Solución + "-" + operación+ "-" + Error que sale en pantalla]
GC-Fraude-Poliza nueva-Null pointer exception.   
ATR-Hogar-Crear siniestro-Null pointer exception.

2. **Datos usados durante la prueba:** todos los datos relevantes sobre la prueba como por ejemplo 
identificadores de transacciones, tipos de clientes específicos, entre otros.

3. **Tipo de incidente:** campo en donde se relaciona el tipo de error, incluso se puede indicar cuando un 
 reporte que se generó no era bug.

4. **Adjuntos:**para este tipo de errores, no se genera una descripción del paso a paso ni se hace obligatorio 
indicar la funcionalidad en que se presenta la falla pero **es totalmente necesario adjuntar el vídeo en 
donde se pueda observar claramente el paso a paso de la prueba y el punto de falla**.


**Cuando se generan errores que son de fácil réplica para el tester, aplican los puntos 1, 2 y 3, del apartado anterior, contemplando que:**

1. **En la descripción del error:** se debe indicar el paso a paso para replicar el bug, incluyendo la descripción del error puntual que se genera en pantalla.

2. **Evidencias:**en este caso se pueden adjuntar imágenes de los errores.



***PARA TENER EN CUENTA* **![AtenerEncuenta.png](https://bitbucket.org/repo/akpjXAz/images/2947682233-AtenerEncuenta.png)

A continuación, se relaciona una breve descripción de algunos campos nuevos en jira que deben ser diligenciados al momento de reportar un error:

1. **Tipo bug: ** en este campo se puede seleccionar qué tipo de error es el que se está reportando, se debe tener en cuenta: 

    * **Funcional**: aquellos errores que afectan como tal la prueba en cuanto al flujo de negocio.

    * **Usabilidad: ** todos aquellos errores/mejoras que se consideran atentan contra la facilidad de uso de la 
    aplicación. 

    * **Performance: aquellos errores que de alguna manera afecten el rendimiento de la aplicación; en esta 
    clasificación se incluyen las pruebas de carga, estrés y rendimiento.

    * **Seguridad: ** cualquier error que se identifique que es un hueco de seguridad.

    * **Definición: ** se debe utilizar/modificar cuando un bug no es por falencias en el código sino porque fue mal 
    definida la funcionalidad. Este tipo de bug puede ser reportado por un QA o un desarrollador.

    * **No bug: **esta clasificación debe ser utilizada cuando se identifica que un error reportado, realmente no es 
    un error, ya sea porque se ejecutó la prueba con datos erróneos, porque no se contempló bien el escenario, entre 
    otras razones.
  **Para este caso, el cambio de estado se puede dar por parte de la persona de calidad o por parte del 
    desarrollador, en cualquiera que sea el caso se debe diligenciar un comentario justificando por qué razón no es 
    bug. **

2. ** Numero de ocurrencias: ** se debe utilizar por ejemplo para los bugs que son **intermitentes** o ** re-aperturados**. Es un contador, lo cual quiere decir que por cada vez que se presente le suman 1.

3. **Origen del error: ** campo en el cual se indica a partir de qué tipo de historia de genera el error. Los posibles valores son:

    * **HU: ** utilizado cuando el error es detactado en pruebas de un desarrollo de una historia. Para los casos de 
        errores identificados en las pruebas transversales se debe utilizar dicho valor.

    * **Raizal: ** utilizado cuando el error es detactado en pruebas de un desarrollo de una historia. Para los casos 
        de errores identificados en las pruebas transversales se debe utilizar dicho valor.

    * **Regresión: ** se debe utilizar cuando son bugs que se identifican en una regresión, ya sea manual o 
        automática.