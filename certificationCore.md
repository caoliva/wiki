# Certificación Core

[Volver al home](Home)

## Para Asesoría y Venta:

Para realizar la certificación para Asesoría y venta remitirse al siguiente link:

[Material completo](https://www.somossura.com/sites/negocio/seguros/core-seguros/Documents/Forms/AllItems.aspx?RootFolder=%2Fsites%2Fnegocio%2Fseguros%2Fcore-seguros%2FDocuments%2F3-Tecnolog%C3%ADa%2F3%2E7-Arquitectura%2FCertificaci%C3%B3n%20GroupCenter&FolderCTID=0x012000880874CE2997EE47BB0C157C5FEF5AAF&View=%7BC2B3250C-9A78-4EE6-98EB-BC26A57D9F22%7D)

Se debe empezar por el siguiente documento:

**SUGW ARQ Certificación Sura GroupCenter.docx**

##Para Guidewire:

Para realizar la certificación para Guidewire remitirse al siguiente link:

[Material completo](https://www.somossura.com/sites/negocio/seguros/core-seguros/Documents/Forms/AllItems.aspx?RootFolder=%2Fsites%2Fnegocio%2Fseguros%2Fcore-seguros%2FDocuments%2F3-Tecnolog%C3%ADa%2F3%2E7-Arquitectura%2FCertificaci%C3%B3n%20CORE&FolderCTID=0x012000880874CE2997EE47BB0C157C5FEF5AAF&View=%7BC2B3250C-9A78-4EE6-98EB-BC26A57D9F22%7D)

Se debe empezar por el siguiente documento:

**SUGW ARQ Certificación Sura CORE.docx**



[Volver al home](Home)