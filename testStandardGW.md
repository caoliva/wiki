# Estándares de Pruebas

[Volver al home](Home)

## Lista a tener en cuenta por los commiters

1. No se pide unitarias para métodos que retornan instancias de clases.
2. Se deben cubrir todos los escenarios testeables de lo creado o editado.
3. Se deben probar todos los escenarios de toogles.
4. Se deben quitar los comentarios generados automáticamente por el IDE.
5. Cuando se modifican clases de Caja, debe quedar el comentario Sura. (Estándar de desarrollo)
6. Se define como **NO** obligatorio que el verify.times tenga los parámetros.
7. Update suite: Solo se puede subir actualización de la suite si se modificó la suite, de lo contrario dar la explicación. Quien modifique la suite es responsable de subir la actualización de los módulos.
8. Unitarias para verificar mensajes deben comparar con DisplayKey.
9. Todas las pruebas de la función editada deben quedar con EESuraTestBase.
10. No se deben crear pruebas nuevas con SuraTestBase ni con TestBase.
11. Se debe adjuntar en el pull request pantallazo de que corrió y finalizó sin fallas: Unitarias, Condenarc, estándar nombres, soap api, java api, batch4. Que se vea fecha y hora de ejecución.

## Políticas definidas para unitarias en Guidewire

•Para el nombramiento de un test unitario en una aplicación GW, debe tenerse en cuenta que debe iniciar con la palabra test, esto debido a que el acelerador de pruebas unitarias utiliza esto para detectar los test a ejecutar en una clase. Por ejemplo un nombre correcto para un test en GW sería: ***testRatingMustReturn0WhenInsuranceValueIs0***.

•Toda clase de test debe extender de la clase ***EESuraTestBase***, ya que el acelerador de GW usa esto para detectar cuales son las clases que contienen test. Puedes ver esto en el “Ejemplo de Test Unitarios usando políticas GW”.

•Al construir una clase de Test, se debe especificar un RunLevel, para esto se debe hacer uso de **RunLevel (NONE)**, porque son los test más rápidos de todos en tiempo de ejecución.

•El uso de la clase ***EESuraTestBase*** permite que se puedan hacer instancias nuevas de cualquier entidad sin necesidad de declarar transacciones, las instancias entregadas son mocks. Adicionalmente se permite hacer mocks para las invocaciones a propiedades / métodos de entidades en enhancement.

•La instancia de la clase a probar se debe encontrar como un Mockito.spy() en el beforeMethod, esto se debe a posibles problemas de ClassLoader que se presenten entre Gosu y Java.

•Por defecto estás pruebas son Paralelas (*ParallelTestInterface*).

•En caso de sobreescribir el beforeClass debe hacer un llamado al super.beforeClass()

•Todo el código escrito en las aplicaciones GW debe estar en **inglés**, por lo tanto los test deben ser codificados en **inglés**.

•No se realizan pruebas unitarias para ejecutar Eventos o GosuRules, Para ello, cada lógica del evento debe estar en una función dentro de una clase y a ésta si se le implementa los escenarios en las pruebas unitarias. Si se requiere modificar una regla u evento existente cuya lógica se encuentra dentro de la misma, se debe trasladar la lógica a una clase, siguiendo las buenas prácticas de codificación, y dependiendo de su complejidad se establecen los escenarios a implementar en las pruebas unitarias.

•Si presenta algún inconveniente al realizar uso de la clase de pruebas por favor diríjase al área de arquitectura para evaluar el escenario y la correcta implementación. Esta clase se ha diseñado con el fin de hacer las pruebas de manera unitaria sin dependencias del servidor, BD y evitar mover código de enhancements, por lo tanto hay características que pueden no estar disponibles pero se puede evaluar la viabilidad de implementarse.

### Ejemplo usando entidades

```java
class RICalculationEngineImpl implements RICalculationEngine {

    public static var INSTANCE: RICalculationEngine as readonly Instance = new RICalculationEngineImpl()

    private var _reinsurerExemptUtil: RIReinsurerExemptUtil

    /**   * returns total sura   * */
    public function totalSuraUtility(riWorksheetItem: RIWorksheetItem_Ext) : BigDecimal {
        if (riWorksheetItem.ProgramItem) {
            return 0bd
        }
    }
    var sum = SuraNullSafeMatch.nsAdd({riWorksheetItem.FranchiseeCommissionRate, riWorksheetItem.BrokerCommissionRate})
    return SuraNullSafeMatch.nsSub(riWorksheetItem.CommissionReinsurerCeded, sum)
}
```


```java
@RunLevel(NONE)
class RICalculationEngineImplTest extends EESuraTestBase {
  private var _engine: RICalculationEngine
  private var _validationReinsurerExemp: RIReinsurerExemptUtil

  override function beforeMethod() {
    _engine = Mockito.spy(RICalculationEngineImpl.INSTANCE)
    _validationReinsurerExemp = Mockito.spy(new RIReinsurerExemptUtil())
  }

  function testTotalSuraUtilityWithAllNeededDataForSum() {

    var riWorksheetItem = new RIWorksheetItem_Ext()
    Mockito.when(riWorksheetItem.ProgramItem).thenReturn(false)
    Mockito.when(riWorksheetItem.FranchiseeCommissionRate).thenReturn(1bd)
    Mockito.when(riWorksheetItem.BrokerCommissionRate).thenReturn(1bd)
    Mockito.when(riWorksheetItem.CommissionReinsurerCeded).thenReturn(5bd)
    var result = _engine.calculateTotalSuraUtility(riWorksheetItem)
    assertThat(result.doubleValue()).isEqualTo(3.0d)
  }
```

En el código anterior se puede apreciar:

•El RunLevel(NONE) que debe ser especificado en todas las pruebas

•La función beforeMethod que hace spy de la instancia de las clase a probar

•Creación de un mock de una entidad (a partir de un new RIWorksheetItem_Ext())

•Parametrización del mock  con los valores deseados

•Assert sobre el resultado


Cuando la entidad es EffDated se debe tener un PolicyPeriod para utilizar en el constructor:
```java
@RunLevel(NONE)
class AddlInterestDetailTest extends EESuraTestBase {
  private var _policyPeriod: PolicyPeriod
  private var _addlInterestDetail: AddlInterestDetail

  override function beforeMethod() {
    _policyPeriod = new PolicyPeriod()
    _addlInterestDetail = new PAVhcleAddlInterest(_policyPeriod)
  }

  ...
}
```

Cuando la entidad es abstracta se tienen dos alternativas:

• Se puede hacer un new de uno de los subtipos de la entidad
```java
@RunLevel(NONE)
class AddlInterestDetailTest extends EESuraTestBase {
  private var _policyPeriod: PolicyPeriod
  private var _addlInterestDetail: AddlInterestDetail

  override function beforeMethod() {
    _policyPeriod = new PolicyPeriod()
    _addlInterestDetail = new PAVhcleAddlInterest(_policyPeriod)
  }

  ...
}
```
• Se puede hacer la nueva instancia a través del bundle
```java
@RunLevel(NONE)
class AddlInterestDetailTest extends EESuraTestBase {
  private var _addlInterestDetail: AddlInterestDetail

  override function beforeMethod() {
    _addlInterestDetail = _bundle.newBeanInstance(AddlInterestDetail) as AddlInterestDetail
  }

  ...
}
```

#### **Ejemplo usando enhancements**

```java
  protected function calculateGrossCedingRateNetPremium(riWorksheetItem: RIWorksheetItem_Ext): BigDecimal {
    var retVal = 0bd
    var rateForeignReinsurer = RIWorksheetUtil.isForeingReinsurer(riWorksheetItem.Participant) and !RIReinsurerExemptUtil.isReinsurerExempt(riWorksheetItem.Participant)  ? getRFReinsurersExempt() : 0bd
    var operation1 = SuraNullSafeMath.nsDiv(riWorksheetItem#NetPremium.get().Amount, riWorksheetItem#TotalExposedValue.get().Amount)
    var operation2 = SuraNullSafeMath.nsAdd({riWorksheetItem.CommissionReinsurerCeded, riWorksheetItem.FireDepartmentTax, rateForeignReinsurer})
    var operation3 = SuraNullSafeMath.nsSub(1, SuraNullSafeMath.nsDiv(operation2, 100))
    retVal = SuraNullSafeMath.nsDiv(operation1, operation3)
    retVal = SuraNullSafeMath.nsMult(retVal, 1000)
    return retVal
  }
```

```java
@RunLevel(NONE)
class RICalculationEngineImplTest extends EESuraTestBase {
  private var _engine: RICalculationEngine
  private var _validationReinsurerExemp: RIReinsurerExemptUtil

  override function beforeMethod() {
    _engine = Mockito.spy(RICalculationEngineImpl.INSTANCE)
    _validationReinsurerExemp = Mockito.spy(new RIReinsurerExemptUtil())
  }

  function testTotalSuraUtilityWithAllNeededDataForSum() {

    var riWorksheetItem = new RIWorksheetItem_Ext()
    Mockito.when(riWorksheetItem.ProgramItem).thenReturn(false)
    Mockito.when(riWorksheetItem.FranchiseeCommissionRate).thenReturn(1bd)
    Mockito.when(riWorksheetItem.BrokerCommissionRate).thenReturn(1bd)
    Mockito.when(riWorksheetItem.CommissionReinsurerCeded).thenReturn(5bd)
    var result = _engine.calculateTotalSuraUtility(riWorksheetItem)
    assertThat(result.doubleValue()).isEqualTo(3.0d)
  }
```

En el código anterior se puede apreciar:
•El RunLevel(NONE) que debe ser especificado en todas las pruebas
•La función beforeMethod que hace spy de la instancia de las clase a probar
•Creación de un mock de una entidad (a partir de un new RIWorksheetItem_Ext())
•Parametrización del mock  con los valores deseados
•Llamado al método mockEnhancementProperty para hacer mock de la respuesta, enviándole el Property que se desea (Entidad#Propiedad), instancia del mock, objeto de  respuesta.
•Assert sobre el resultado
•Se aclara que la forma de invocar la propiedad del enhancement cambia para poder habilitar la capacidad de mock, normalmente se llamaría **riWorksheetItem.TotalExposedValue** pero en su lugar se debe llamar **riWorksheetItem#TotalExposedValue.get()**

```java
uses gw.testharness.RunLevel
uses sura.suite.utility.EESuraTestBase

@RunLevel(NONE)
class EnhacenmentMethod extends EESuraTestBase {
  function testEnhancementMethodWithoutArgs() {
    var policyPeriod: PolicyPeriod
    var expectedPolicyPeriod: PolicyPeriod
    var result: PolicyPeriod
    var params: Object[]
    policyPeriod = new PolicyPeriod()
    expectedPolicyPeriod = new PolicyPeriod()
    params = {}
    mockEnhancementMethod(PolicyPeriod#regenerateRisks(), policyPeriod, params, expectedPolicyPeriod)
    
    result = policyPeriod#regenerateRisks().invoke()
    
    assertEquals(expectedPolicyPeriod, result)
    verifyEnhancementMethod(PolicyPeriod#regenerateRisks(), policyPeriod, params, Mockito.times(1))
  }
  function testEnhancementMethodWithArgs()
  {
    var policyPeriod: PolicyPeriod
    var expectedReinsurable: Reinsurable
    var result: Reinsurable
    var params: Object[]
    var riskNumber: String
    policyPeriod = new PolicyPeriod()
    expectedReinsurable = new PolicyRisk(policyPeriod)
    riskNumber = "riskNumber"
    params = {riskNumber}
    mockEnhancementMethod(PolicyPeriod#getReinsurable(), policyPeriod, params, expectedReinsurable)
    
    result = policyPeriod#getReinsurable().invoke(riskNumber)
    
    assertEquals(expectedReinsurable, result)
    verifyEnhancementMethod(PolicyPeriod#getReinsurable(), policyPeriod, params, Mockito.times(1))
  }
}
```

En el código anterior se puede apreciar:
-El RunLevel(NONE) que debe ser especificado en todas las pruebas
-Creación de un mock de una entidad (a partir de un new PolicyPeriod(),new Reinsurable()))
-Llamado al método mockEnhancementMethod para hacer mock de la respuesta, enviándole el Method que se desea (Entidad#Método()), instancia del mock, parámetros, objeto de  respuesta.
-Assert sobre el resultado
-Se aclara que la forma de invocar el método del enhancement cambia para poder habilitar la capacidad de mock, normalmente se llamaría **policyPeriod.getReinsurable(riskNumber)** pero en su lugar se debe llamar **policyPeriod#getReinsurable().invoke(riskNumber)**
-Para realizar la verificación de las invocaciones en lugar de utilizar **Mockito.verify(...)** se debe utilizar los métodos **verifyEnhancementMethod** y **verifyEnhancementProperty** según sea el caso

#### **Ejemplo usando permisos del sistema**

```java
class Audit {
  function completeAudit() {
    if (perm.System.completeaudit) {
      completeAuditOnExternalSystem()
    } else {
      throw new Exception()
    }
  }
  
  protected function completeAuditOnExternalSystem() {
    ...
  }
}
```

```java
@RunLevel(NONE)
class AuditTest extends EESuraTestBase {
  private var _audit: Audit
  
  override function beforeMethod() {
    _audit = Mockito.spy(new Audit())
  }

  function testCompleteAuditMustCallCompleteAuditOnExternalSystemWhenUserHavePermission() {
    mockPerm(perm.System, "completeaudit", true)
    Mockito.doNothing().when(_audit).completeAuditOnExternalSystem()

    _audit.completeAudit()

    Mockito.verify(_audit, Mockito.times(1)).completeAuditOnExternalSystem()
  }
  
  function testCompleteAuditMustCallCompleteAuditOnExternalSystemWhenUserHavePermission() {
    mockPerm(perm.System, "completeaudit", false)

    assertExceptionThrown(\-> {
      _audit.completeAudit()
    }, Exception)
  }
}
```

En el código anterior se puede apreciar:
•El RunLevel(NONE) que debe ser especificado en todas las pruebas
•La función beforeMethod que hace spy de la instancia de las clase a probar
•Parametrización del mock para permisos con los valores deseados

#### **Ejemplo con métodos que dependen de estáticos**

```java
class Audit {
  static function greet(name: String): String {
    ...
    return processName(name)
  }
  
  protected static function processName(name: String): String {
    ...
  }
}
```

```java
@RunLevel(NONE)
class AuditTest extends EESuraTestBase {
  
  function testGreetMustReturnSameValueAsProcessName() {
    mockStatic(Audit, {Audit#processName(String)})
    var name = "Caloma"
    var expectedResult = "Resultado de procesar el nombre"
    Mockito.when(Audit.processName(name)).thenReturn(expectedResult)

    var result = Audit.greet(name)

    assertEquals(expectedResult, result)
    verifyStatic(Audit, Audit#processName(String), Mockito.times(1), {name})
  }
  
}
```

En el código anterior se puede apreciar:

1. El RunLevel(NONE) que debe ser especificado en todas las pruebas

2. La función **mockStatic** que recibe la clase y un listado de métodos estáticos que se quieren mockear

3. Parametrización del mock para permisos con los valores deseados

4. La función **verifyStatic** que recibe la clase y el método estático que se quieren verificar, el modo de verificación y el listado de argumentos con el que se quiere verificar la invocación

Tener en cuenta:

* Luego de la ejecución de todos los test **(afterClass)**, EESuraTestBase restaura la definición original de la clase para que no hayan inconvenientes con otros escenarios de pruebas. Sin embargo si necesita hacerlo en algún escenario puede utilizar la funcion **restoreClass** específicando la clase.


## Generador de Pruebas Unitarias

Con el propósito de disminuir los tiempos de construcción de pruebas y brindar un esquema base se ha realizado un generador de pruebas unitarias **scripts/UnitTestGenerator.gsp**. Para ejecutar el generador se debe copiar el contenido de dicho archivo en Scripter o Gosu Scratchpad (Local), teniendo en cuenta los siguientes 4 parámetros:

•**classToGenerate**. Clase a la que se le va a generar las pruebas unitarias.

•**methods**. Opción para específicar a cuales métodos (solo el nombre) se les va a generar pruebas. Si no se especifica ninguno se genera para todos por defecto.

•**debug**. Opción para imprimir comentarios acerca de cada sentencia analizada. Esta opción solo es para búsqueda de errores o implementación de nueva funcionalidad.

•**debugAAA**. Opción para imprimir comentarios separando cada etapa de la prueba (Arrange, Act, Assert).

```java
var classToGenerate = sura.suite.gw.consatrack.webservice.ConsatrackFacade.Type
var methods: List<String> = {}
var debug = false
var debugAAA = true
```

La ejecución del generador imprime como resultado una clase con las siguientes características:

•Contiene los **uses** declarados en la clase a probar

•Extiende de **EESuraTestBase** (específica del módulo)

•Está anotada con **@RunLevel(NONE)**

•Contiene un spy() de la instancia de clase a probar generado en el beforeMethod

•Un método de pruebas por cada uno de los branches detectados a partir del código fuente

```java
package test.generator

uses gw.testharness.RunLevel
uses org.mockito.Mockito
uses sura.suite.utility.EESuraTestBase

@RunLevel(NONE)
class ClaseParaProbarTest extends EESuraTestBase {
  var _aClaseParaProbar: ClaseParaProbar
  function beforeMethod() {
    var aClaseParaProbar = new ClaseParaProbar()
    _aClaseParaProbar = Mockito.spy(aClaseParaProbar)
  }

  function testObtenerMasterPolicyPeriodMustReturnXXXXWhen() {
    // Arrange
    var pp = new entity.PolicyPeriod()
    var branchName = "0qsFf"
    Mockito.doReturn(branchName).when(_aClaseParaProbar).funcionQueDevuelveUnString()
    var accountNumber = "aLdGg"
    Mockito.doReturn(accountNumber).when(_aClaseParaProbar).funcionQueDevuelveUnStringPeroConParametros(pp.BranchName)
    // Act
    var result = _aClaseParaProbar.obtenerMasterPolicyPeriod(pp)
    // Assert
    assertTrue(result typeis entity.PolicyPeriod)
  }

  function testConVariosParametrosMustReturnXXXXWhen() {
    // Arrange
    var b = new entity.BPCBatch_Ext()
    var c = new entity.ProducerCode()
    var r = new entity.Group()
    var expectedResult = b.getPolicyListFromOldProducer(c, r)
    // Act
    var result = _aClaseParaProbar.conVariosParametros(b, c, r)
    // Assert
    assertEquals(expectedResult, result)
  }

  function testFuncionQueDevuelveUnStringPeroConParametrosMustReturnXXXXWhen() {
    // Arrange
    var param = "C7upm"
    var expectedResult = "Otro Valor Parametrizado en Codigo"
    // Act
    var result = _aClaseParaProbar.funcionQueDevuelveUnStringPeroConParametros(param)
    // Assert
    assertEquals(expectedResult, result)
  }

  function testFuncionQueDevuelveUnStringMustReturnXXXXWhen() {
    // Arrange
    var expectedResult = "Valor Parametrizado en Codigo"
    // Act
    var result = _aClaseParaProbar.funcionQueDevuelveUnString()
    // Assert
    assertEquals(expectedResult, result)
  }

  function testFuncionQueNoHaceNadaMustReturnXXXXWhen() {
    // Arrange
    // Act
    _aClaseParaProbar.funcionQueNoHaceNada()
    // Assert
  }
}
```

El generador pretende facilitar la escritura de las pruebas pero **en ningún momento reemplaza la revisión que debe realizar el desarrollador**, por esto se deben tener presente varios aspectos:

•**Comentarios //ERR:**. Estos comentarios se colocan como manera rápida de identificar cuales sentencias (con número de línea) no fueron procesadas por el generador, y deben ser escritas de manera manual por el desarrollador.

•**Nombre de los métodos**. El nombre debe ajustarse para que describa el escenario.

•**Branches**.  Cada método generado por un branch (ej. If, Else, Case, etc) debe ajustarse en el arrange para que cumpla las condiciones necesarias para entrar en cada caso.

•**Asserts**. Para cada método según las sentencias se agregan por defecto una serie de assertEquals, assertTrue o verify(), pero estos se deben validar y complementar en casos como por ejemplo asignaciones.

•**Enhancements**. No se diferencia entre llamados de métodos comunes y enhancements, por lo que deben realizarse cambios en caso de ser necesario para utilizar las funciones mockEnhancementProperty y mockEnhancementMethod.

•**Estáticos**. Los métodos estáticos existentes en la clase a probar no son tenidos en cuenta en el generador, por lo que se deben realizar manualmente*.

***El generador de unitarias al igual que EESuraTestBase son utilidades generadas por el proyecto, por lo tanto se espera que contribuyan de manera activa con el ciclo de desarrollo y cualquier sugerencia o error que pueda retro-alimentar por favor reportarlo al área de Arquitectura.***

## Anexos pruebas unitarias 

### Probar funciones de clases abtractas

```
@RunLevel(NONE)
class xTest extends EESuraTestBase {

  function testFuno() {
    // La clase abstracta no se puede instanciar directamente
    var aClass = Mockito.mock(AClass)
    
    // Debemos indicarle al mock que se llame el metodo real  
    Mockito.doCallRealMethod().when(aClass).funo(1, 1)

    var result = aClass.funo(1,1)
    assertTrue(result == 2)
  }

  abstract class AClass {
    function funo(a: int, b: int): int {
      return a+b
    }
  }
}
```
**Ejemplo Establecer valor para un ScriptParameter**
Dentro de la función de Test se debe llamar el método *setScriptParameter* con los parámetros de Nombre del Parametro y Valor deseado
```
setScriptParameter("NombreScript","Valor")

```
**Ejemplo Obtener un Plugin**
Dentro de la función de Test se debe llamar el método *_pluginConfig.getPlugin* con el parámetro de la interface de Plugin deseado

```
function testMustRunCreatePolicyPeriodWhenCallSend(){
    ...
    var plugin:IBillingSystemPlugin
    ...
    plugin = Mockito.mock(IBillingSystemPlugin)
    ...
    Mockito.when(***_pluginConfig.getPlugin(IBillingSystemPlugin)***).thenReturn(plugin)
    ...

  }

```

[Volver al home](Home)