**Fecha de Creación:** 18 de Diciembre de 2019

1. Se hace Check out de la rama en la que se van a mezclar los cambios (Siempre se toma hacía dónde van los cambios)
![1er Sourtree.png](https://bitbucket.org/repo/akpjXAz/images/3889336419-1er%20Sourtree.png)

2. Se hace fetch con prune de la rama en la que se van a mezclar los cambios
![2do Sourtree.png](https://bitbucket.org/repo/akpjXAz/images/780094352-2do%20Sourtree.png)

3. Se debe crear un Branch
![3roSourtree.png](https://bitbucket.org/repo/akpjXAz/images/1212270194-3roSourtree.png)
 
4. Se identifica el commit que generó el conflicto
![4toSourtree.png](https://bitbucket.org/repo/akpjXAz/images/410877696-4toSourtree.png)
 
5. Parado en la rama en la que se quiere solucionar el conflicto, se da click en el botón merge
![5toSourtree.png](https://bitbucket.org/repo/akpjXAz/images/1192043242-5toSourtree.png)

6. En la opción Jump to, se busca el commit del pipeline con el SHA, una vez encontrado se valida que seleccionada la primera opción y le damos OK
![6toSourcetree.png](https://bitbucket.org/repo/akpjXAz/images/1517569066-6toSourcetree.png)
![7moSourcetree.png](https://bitbucket.org/repo/akpjXAz/images/2123578223-7moSourcetree.png)
![8tosourcetree.png](https://bitbucket.org/repo/akpjXAz/images/1645742364-8tosourcetree.png)

7. Aparece el mensaje de que existe un conflicto, y queda el cambio sin realizar el commit
![9noSourcetree.png](https://bitbucket.org/repo/akpjXAz/images/2985158213-9noSourcetree.png)
![10mosourcetree.png](https://bitbucket.org/repo/akpjXAz/images/1702376013-10mosourcetree.png)

8. Al momento de resolver los conflictos siempre la prelación es lo que viene de la otra rama (En este caso develop)
![11Sourcetree.png](https://bitbucket.org/repo/akpjXAz/images/3086080679-11Sourcetree.png)

9. Se confirman los cambios
![12Sourcetree.png](https://bitbucket.org/repo/akpjXAz/images/2096081554-12Sourcetree.png)
 
10. Se realiza el commit del branch y se hace push del branch

11. Se crea el Pull Request contra la rama que se seleccionó en el primer punto.