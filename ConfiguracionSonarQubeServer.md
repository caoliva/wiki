# Integración de IntelliJ IDEA con Servidor SonarQube Sura #

[Volver al home](Home)

### Requisitos: ###

* IntelliJ IDEA

* Plugin de SonarLint

## Instalación de SonarLint en el IDE ##

1- Ir al Menú *File, Settings*

![image002.png](https://bitbucket.org/repo/akpjXAz/images/3650085716-image002.png)

2- Seleccionar la opción *plugins* y validar en la búsqueda si está *SonarLint* instalado. De no ser así, instalarlo y reiniciar el IDE para completar la instalación.

![image006.png](https://bitbucket.org/repo/akpjXAz/images/237109855-image006.png)

![image004.png](https://bitbucket.org/repo/akpjXAz/images/2597244106-image004.png)

![image008.png](https://bitbucket.org/repo/akpjXAz/images/500772983-image008.png)

![image011.png](https://bitbucket.org/repo/akpjXAz/images/1330817188-image011.png)

![image012.png](https://bitbucket.org/repo/akpjXAz/images/3438920934-image012.png)

En este punto tendremos **SonarLint** instalado y listo para analizar el código con las reglas básicas.

## Enlazar SonarLint con el Servidor SonarQube de Sura ##

1- Ir al Menú *File, Settings*

![image002.png](https://bitbucket.org/repo/akpjXAz/images/3650085716-image002.png)

2- En la ventana emergente hacer clic en la opción *Other Settings* y posteriormente hacer clic sobre el hipervínculo *SonarLint General Settings*

![image018.png](https://bitbucket.org/repo/akpjXAz/images/1093000886-image018.png)

3- En la nueva ventana emergente, establecer un nombre para la configuración, seleccionar la conexión de tipo *SonarQube* y poner la URL del servidor http://srintcp04:4748/

![SonarQube Server Configuration_ Server Details.png](https://bitbucket.org/repo/akpjXAz/images/1946506197-SonarQube%20Server%20Configuration_%20Server%20Details.png)

4- Hacer clic en *Next* y seleccionar el tipo de autenticación por *Login / Password* 

![image023.png](https://bitbucket.org/repo/akpjXAz/images/3582669249-image023.png)

5- Usar las siguientes credenciales y hacer clic en *Next*:

**user**: *coredeseguros*

**password**: *Coredeseguros*

![image024.png](https://bitbucket.org/repo/akpjXAz/images/460334401-image024.png)

6- Hacer clic en *Finish* para terminar la configuración del servidor.

![image026.png](https://bitbucket.org/repo/akpjXAz/images/1400366061-image026.png)

Con esto quedará enlazado el SonarLint con el servidor SonarQube de Sura.

## Enlazar SonarLint con el proyecto que se desea analizar ##

1- Ir al Menú *File, Settings*

![image002.png](https://bitbucket.org/repo/akpjXAz/images/3650085716-image002.png)

2- En la ventana emergente hacer clic en la opción *Other Settings* y posteriormente hacer clic sobre el hipervínculo *SonarLint Project Settings*

3- Seleccionar la casilla *Enable binding to remote SonarQube Server*

![image028.png](https://bitbucket.org/repo/akpjXAz/images/3360545842-image028.png)

4- Seleccionar el servidor previamente configurado

![image030.png](https://bitbucket.org/repo/akpjXAz/images/3415824274-image030.png)

5- Hacer clic en el botón *Search in List* para buscar el proyecto que se requiere escanear.

![image032.png](https://bitbucket.org/repo/akpjXAz/images/963615363-image032.png)

6- En la ventana emergente, buscar el nombre exacto del proyecto y hacer clic en *OK*

![image034.png](https://bitbucket.org/repo/akpjXAz/images/3990621391-image034.png)

7- Hacer clic en OK nuevamente para finalizar la configuración del proyecto.

![image036.png](https://bitbucket.org/repo/akpjXAz/images/4002715809-image036.png)

Con esto tendremos SonarLint conectado al servidor de SonarQube de Sura y enlazado con las reglas del proyecto en el cual estamos trabajando.

A medida que se van abriendo los archivos .java y se va escribiendo código, SonarLint deberá ir mostrando el análisis estático realizado sobre ese archivo y mostrará la falta en la que se está incurriendo.

![sonarLintEx.jpg](https://bitbucket.org/repo/akpjXAz/images/1188117823-sonarLintEx.jpg)

En la parte derecha mostrará detalladamente la falta y arrojará una posible solución para eliminar la deuda técnica.

![sonarLintEx2.jpg](https://bitbucket.org/repo/akpjXAz/images/2497356239-sonarLintEx2.jpg)