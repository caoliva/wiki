# Estándares de Desarrollo en Scala

[Volver al home](Home)

## Calidad del desarrollo

En cuanto a la calidad de las implementaciones en Scala se enmarca en los siguientes puntos:

* Descomposición del código, orientado a reducir su complejidad y facilitando su mantenimiento
* Nomenclatura y manejo de variables, métodos, clases y demás artefactos
* Estilo de programación
* Estructura y distribución de directorios
* Gestión de excepciones y fallos en general
* Pruebas unitarias

Con respecto a los aspectos técnicos...

* No repetir código - Reutilizar en vez de duplicar
* Responsabilidad única – Los métodos y clases solamente deben hacer referencia a un propósito
* Simplicidad – Código entendible
* Limpieza – Código manejable y mantenible
* Métodos pequeños
* Que el código permita fácilmente cambios futuros
* Clases agrupadas por funcionalidad en sus respectivos paquetes
* Evitar acoplamiento entre módulos y/o paquetes
* Uso de patrones de diseño
* Evitar pérdida de encapsulación
* Identificar posibles cuellos de botella
* Hacer uso adecuado de las funcionalidades provistas por el lenguaje (Ejemplo, maps, flatMaps, etc)
* Escritura en Logs eficientemente
* Dependencias externas debidamente manejadas. Por ejemplo, las conexiones a la base de datos
* Identificar memory leaks. Muy posiblemente se requiera ejecutar pruebas de estrés en la medida de lo posible


## Estándares de nombramiento

* Variables, evitar nombres como a, b, c, d, e, x
* Nombrar los métodos de acuerdo a su función, evitando en lo posible incluir nombres de librerias (por ej. "consultarPersonaConSlick") o parámetros (por ej "consultarPersonaPorId")
* Evitar combinación español con algún otro idioma. El idioma oficial es español.


## Manejo de Base de Datos

* Incluir los filtros en la medida de lo posible en el query, en lugar de hacerlo en el lenguaje
* Evaluar los planes de ejecución en las consultas que implican varios Join
* Evaluar claves primarias y demás índices a disposición
* Evitar a toda costa los FullScan
* Evaluar la cantidad de registros estimada (peor escenario / escenario promedio)
* Evaluar separación de queries para realizar multiples ejecuciones pequeñas en lugar una muy grande, siempre llegando a un balance aceptable
* Realizar ejecuciones de múltiples sentencias como INSERT, UPDATE, DELETE en el motor en lugar de hacer llamados individuales
* Separar el pool de conexiones de incersiones y el pool de conexiones de consultas


## Manejo de hilos

* No utilizar el ExecutionContext.global de Scala para no competir con la operaciones nativas del lenguaje
* Validar la capacidad de infraestructura para aprovisionar correctamente los pool de conexiones y de procesamiento, e inclusive de CPU y memoria


## Manejo de librerías

* Para incluir nuevas librerías, debe validarse con el grupo de arquitectos del equipo y sustentarse su uso, sea porque no hay una alternativa incluida en el proyecto o la que esta no cumple con la funcionalidad


[Volver al home](Home)