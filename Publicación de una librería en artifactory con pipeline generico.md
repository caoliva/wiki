Para realizar la publicación de librerías en el repositorio de la compañía (http://artifactory.suramericana.com.co) solo es posible realizarlo desde Jenkins. Por tal motivo desde el equipo de mejora continua se ha creado un pipeline genérico (Pipeline as a Code) con el objetivo de poder hacer la publicación de librerías sin la necesidad de configurar un pipeline para cada desarrollo.

### Notas a tener en cuenta: ###

* El pipeline mencionado aquí solo funciona para compilar una librería con gradle o sbt, por el momento no soporta otras tecnologías.
* El pipeline mencionado aquí solo funciona con repositorio git, específicamente bitbucket.


### Pasos para la publicación de librerías con Pipeline As a Code ###


1.	Para publicar la librería se debe crear un archivo llamado Jenkins.properties en la raíz del proyecto:

 ![image1.png](https://bitbucket.org/repo/akpjXAz/images/457404092-image1.png)


En este archivo se encuentran los siguientes parámetros:

 ![image2.png](https://bitbucket.org/repo/akpjXAz/images/260867306-image2.png)

Donde:

**Buid**: es la manera en la que se empaqueta el aplicativo, puede ser sbt o gradle.
**RepoPath**: Es la ruta donde se almacenará el jar en artifactory

![image3.png](https://bitbucket.org/repo/akpjXAz/images/598427054-image3.png)

**ArtifactId**: Es el nombre del artefacto en el repositorio artifactory

![image4.png](https://bitbucket.org/repo/akpjXAz/images/393730590-image4.png)

**Versión**: es la versión en la que se encuentra el artefacto y se publicará en el repositorio.

 ![image5.png](https://bitbucket.org/repo/akpjXAz/images/2652797354-image5.png)

2.	Pipeline genérico.

El pipeline se encuentra en esta ruta: 
https://jenkinscoreseguros.suramericana.com.co/blue/organizations/jenkins/Core_Seguros%2FPipeline%2FLibrerias%2FPipeline/activity/

para ejecutar este pipeline buscando publicar una librería se debe:
a.	Ejecutar el paso anterior.
b.	Realizar push del archivo creado en el paso anterior a la rama de la cual se va a generar 
c.	Dar clic al botón iniciar:

 ![image7.png](https://bitbucket.org/repo/akpjXAz/images/467446829-image7.png)

d.	El pipeline solicitará la siguiente información:

 ![image8.png](https://bitbucket.org/repo/akpjXAz/images/185550258-image8.png)

En el campo url se coloca la url del repositorio desde el cual se descargará los fuentes para generar el jar.
En el campo branch se colocara el nombre de la rama desde la cual se genera el jar.

e.	Dar clic al botón inciar y si todo queda bien configurado se publicará la librería en artifactory.

Información adicional

* documento devOps Sura sobre artifactory, dar clic [aquí](https://suramericana.sharepoint.com/:w:/r/sites/devopssura/_layouts/15/Doc.aspx?sourcedoc=%7B1364b51a-2822-4fd2-908c-2574a5ea4763%7D&action=default)
* usuario de visualización en artifactory: artifactory/artifactory