# Manual de Uso Plugin Datamodel Upgrade

[Volver al home](Home)

## Introducción Plugin Datamodel Upgrade

Guidewire posee la interfaz IDatamodelUpgrade para realizar cambios en el modelo de datos físico y lógico.
Está interfaz se implementa como un plugin dentro de las aplicaciones y se ejecuta durante la fase actualización de la base de datos. Con este plugin se puede:

1. Realizar cambios en el modelo de datos físicos de la base de datos, ejecutar comandos DDL:
    * Modificar una columna para coincidir con el modelo lógico.
    * Modificar columnas de no anulables a anulables y viceversa.
    * Crear Columnas.
    * Borrar Columnas.
    * Renombrar Columnas.
    * Establecer el valor de una columna para un subtipo específico.
    * Crear Tablas.
    * Renombrar Tablas.

2. Realizar cambios en los datos almacenados en la base de datos, ejecutar comandos DML:
    * Eliminar registros.
    * Insertar registros.
    * Actualizar registros.
    * Insertar registros seleccionados desde otra tabla. (Insert Select)

Los cambios de la base de datos se realizan a través de la implementación de dos triggers, los cuales deben tener como clases base BeforeUpgradeVersionTrigger  o AfterUpgradeVersionTrigger. Ambas clases proveen el método getTable, el cual retorna un objeto del tipo IBeforeUpgradeTable o IAfterUpgradeTable, el cual posee las funciones para ejecutar las operaciones de DDL y DML.

## Implementación Sura Datamodel Upgrade

La implementación del Datamodel Upgrade para Sura se encuentra en el plugin SuraDatamodelUpgrade, el cual ejecuta las instrucciones con las clases SuraBaseAfterUpgradeVersionTrigger y SuraBaseBeforeUpgradeVersionTrigger.

Existen tres formas de ejecutar las instrucciones para los cambios en base de datos:
- **A**. Archivo de Propiedades: Es un archivo con entradas del tipo clave valor, donde la clave indica la operación y la clave los cambios a aplicar.  Solo permite ejecutar instrucciones DDL.
- **B**. Archivo de Script: Es un archivo codificado en Gosu que contiene el script a ejecutar en la base de datos. Permite ejecutar instrucciones DDL y DML.
- **C**. Personalizada: Clases que cumplen las interfaces e herencia para ejecutar las instrucciones según el plugin Datamodel Upgrade de GW.

## Cambios por Archivos de Propiedades

En archivo de propiedades es un archivo de texto, donde cada línea representa una instrucción a ejecutar y está compuesta por Clave y Valor, donde, La Clave índica la instrucción a ejecutar, y el valor los parámetros para su ejecución.
En el archivo de propiedades se programaron los casos más comunes a ejecutar en la base de datos, los cuales son:
- **A**. Renombrar Tabla.
- **B**. Renombrar Columna.
- **C**. Borrar Tabla.
- **D**. Borrar Columna.
- **E**. Cambiar Columna para admitir valores nulos.
- **F**. Cambiar Columna para no aceptar valores nulos.
- **G**. Aplicar cambios en el tipo de dato.

Dentro de un archivo puede contener múltiples instrucciones y las cuales se ejecutan en el orden del contenido.
La sintaxis de las instrucciones soportadas es:

*drop.column.TableName=columnName1, columnName2*
*drop.table=TableName1, TableName2*
*rename.table=oldTableName1:newTableName1,oldTableName2:newTableName2*
*rename.column.TableName=oldColumnName1:newColumnName1, oldColumnName2:newColumnName2*
*alter.column.TableName=columnName1:ToNullable, columnName2:ToNullable*
*alter.column.TableName= columnName1:NonNullable:InitialValue, columnName2:NonNullable:InitialValue*
*alter.column.TableName= columnName1: TypeToMatchDatamodel, columnName2: TypeToMatchDatamodel*

Es importante tener presente que el nombre de la tabla o nombre de la columna no necesariamente son iguales al nombre de la entidad. Para el nombre de la columna se debe utilizar el valor del atributo ColumnName en la definición de la columna dentro de la entidad. 

![pluginDataModel.jpg](pluginDataModel.jpg)

En la imagen el nombre del campo en la entidad es **PrimaryAddress** del tipo **Address**, pero el nombre en el modelo físico es  **PrimaryAddressID**.

El nombre de la tabla es un valor compuesto por:

1. Prefijo de la aplicación, el cual se define la parámetro **PublicIDPrefix** del archivo config.xml, por defecto es ab, bc, cc y pc.
2. Identificador de tabla, typelist o extensión, los posibles valores son:
    - **Vacío**, si es una tabla de OOTB.
    - **X**, Si es una tabla nueva.
    - **TL**, si es un typelist.

3. Valor del atributo table de la definición de la entidad.

Ejemplo

| **Modelo Lógico**      | **Modelo Físico**           |
|------------------------|-----------------------------|
| AccountingInfo_Ext     | BCX_ACCOUNTINGINFO_EXT      |
| Contact                | BC_CONTACT                  |
| IdentificationType_Ext | BCTL_IDENTIFICATIONTYPE_EXT |

![pluginDataModel1.jpg](pluginDataModel1.jpg)

**Ejemplo 1**:  Borrar Columna

*drop.column.ABX_SampleTable=columnName1, columnName2*

**Ejemplo 2**:  Borrar Tabla
*drop.table= ABX_SampleTable*

**Ejemplo 3**:  Cambiar el tipo de dato de una Columna

*alter.column. ABX_SampleTable= columnName1: TypeToMatchDatamodel*

**Ejemplo 4**:  Cambiar una Columna para no admitir nulos.

*alter.column. ABX_SampleTable = columnName1:NonNullable:Test*

## Cambios Por Archivos de Script

Para los cambios en base de datos por Script se debe crear un archivo de texto con la extensión dus (DatamodelUpgradeScript). El cual debe cumplir con las reglas del lenguaje Gosu.

El archivo Script admite condicionales, loops, queries, update, insert, delete y cualquier otra operación soportada por el plugin de GW.

**Ejemplo 1**: Actualizar un Registro

```java
var table=getTable("AB_Contact") 
var update = table.update() 
update.set("FirstName","HOLA_MUNDO").compare("TaxID",Equals,"1425896301")
update.execute()
```

**Ejemplo 2**: Aplicar cambios en el tipo de dato, uso de condicionales.

```java
var table = getTable("AB_Contact")
  if (table.exists()) {
     var column = table.getColumn("FirstName")
     if (column.exists()) {
       column.alterColumnTypeToMatchDatamodel()
       print("alter column successfull")
     }
  } else{
    print ("Cannot alter column ")
  }
```

**Ejemplo 3**: Eliminar los datos de una tabla

```java
var sampleTable1 = getTable("ABX_SampleTable1")
var deleteBuilder1=sampleTable1.delete()
deleteBuilder1.execute()
```

**Ejemplo 4**: Eliminar los datos de una tabla con base a una consulta

```java
var count=1
var querySampleTable1 = Query.make(SampleTable1)
var sampleTable2 = getTable("ABX_SampleTable2")
querySampleTable1.compare("Code",Equals,"Code: ${count}")
var result=querySampleTable1.select()

 result.each( \ row ->{
        var deleteSampleTable2 = sampleTable2.delete()
        deleteSampleTable2.compare("ColumnFK",Equals,row.ID.Value)
        deleteSampleTable2.execute()
  } )
```

**Ejemplo 5**: Insertar Registros relacionados

```java
var sampleTable1 = getTable("ABX_SampleTable1")
var sampleTable2 = getTable("ABX_SampleTable2")

    for(var count in 1 .. 10){
      var insertBuilder1 = sampleTable1.insert()
      insertBuilder1
          .mapColumn("Code", "Code: ${count}")
          .mapColumn("Name", "Name: ${count}")
      insertBuilder1.execute()

      var query = Query.make(SampleTable1)
      query.compare("Code",Equals,"Code: ${count}")
      var rowInserted=query.select().first()

      var insertBuilder2 = sampleTable2.insert()
      insertBuilder2
          .mapColumn("Column1", "Column1: ${count}")
          .mapColumn("Column2", "Column2: ${count}")
          .mapColumn("ColumnFK", rowInserted.ID.Value)
      insertBuilder2.execute()
      }
```

**Ejemplo 6**: Actualizar un registro por un TypeCode

```java
var sampleTable1 = getTable("ABX_SampleTable1")
var typeCodeID = getTypeKeyID("typelist name", "typelist code")
var updateBuilder = sampleTable1.update()

 updateBuilder
      .set("Column1", "some value")
      .compare("subtype", Equals, typeCodeID)
    updateBuilder.execute()
```

Para entender con más detalle como crear un script es necesario referise a la documentación de Guidewire del producto en la sección.

**Upgrade Guide**
  - **Customizing the Upgrade**
  - **Starting the Server to Begin Automatic Database Upgrade**

## Procedimientos de Ejecución 

Para la ejecución de los archivos se debe suministrar la ubicación de un directorio con dos subdirectorios llamados  after y before, dentro del cual se deben ubicar los archivos según el tiempo de ejecución.

Al iniciar el proceso de cambios en base de datos los archivos serán movidos entre cuatro posibles directorios, los cuales identifican el estado de su ejecución.

  - **Failed**: Archivos con fallos incluyendo la causa del error.
  - **InExecution**: Archivos que están en ejecución.
  - **Pending**: Archivos cargados en memoria pendientes de iniciar la ejecución.
  - **Successful**: Archivos ejecutados con éxito.

En el ambiente local la ruta del directorio raíz es siempre será *C:\Guidewire\Datamodel\Upgrade*, en los demás ambientes la ubicación es indicada por el argumento de java PathDatamodelUpgrade:
  
  - **BillingCenter**:-DPathDatamodelUpgrade=/app/product/guidewire/bc/datamodel/upgrade
  - **ClaimCenter**: -DPathDatamodelUpgrade=/app/product/guidewire/cc/datamodel/upgrade
  - **ContactManager**:-DPathDatamodelUpgrade=/app/product/guidewire/ab/datamodel/upgrade
  
  - **PolicyCenter**:-DPathDatamodelUpgrade=/app/product/guidewire/pc/datamodel/upgrade

Los archivos creados se deben subir al repositorio de git https://bitbucket.org/suracore/suradatamodelupgrade.git, y de allí serán descargados en las rutas de los manejados de weblogic, para ser ejecutados durante el proceso de deploy.

## Notas Importantes


    1. El proceso de cambio de base de datos no se debe detener, siempre se debe esperar hasta que finalice su ejecución, ya sea con éxito o con errores. Si se detiene de forma abrupta el proceso la aplicación podría no volver a subir por que la base de datos está corrupta o porque detecta que hay una actualización en progreso.

    2. Se debe prestar mucha atención en las instrucciones ejecutadas, porque los cambios no son reversibles. 

    3. En caso que la aplicación no suba indicando que hay una actualización en proceso, es necesario ejecutar directamente en la base de datos la siguiente instrucción. 
    DELETE <TABLE_OWNER><PublicIDPrefix>_SYSTEMPARAMETER WHERE NAME='UpgradeInProgress'

[Volver al home](Home)