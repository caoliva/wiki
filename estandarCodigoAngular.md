# Estándares de Desarrollo para angular

[Volver al home](Home)

## Calidad del desarrollo

En cuanto a la calidad de las implementaciones en Angular se enmarca en los siguientes puntos:

* Descomposición del código, orientado a reducir su complejidad y facilitando su mantenimiento
* Nomenclatura y manejo de variables, métodos, clases y demás artefactos
* Estructura y distribución de directorios
* Gestión de excepciones y fallos en general
* Pruebas unitarias

Con respecto a los aspectos técnicos...

* No repetir código - Reutilizar en vez de duplicar, inclusive en la maquetación (Por ejemplo si se detecta algo que siempre se repite al maquetar, ej: los cards, en vez de duplicar la misma estructura, considerar el crear un componente que defina esta maquetación)
* Responsabilidad única – Los métodos y clases solamente deben hacer referencia a un propósito : [Referencia en documentación de angular](https://angular.io/guide/styleguide#single-responsibility)
* Simplicidad – Código entendible
* Limpieza – Código manejable y mantenible 
    * Evitar banderas o variables del tipo 'esEditable', en caso de requerir algo así debería ser un @Input 
    * Se recomienda el uso del async pipe de Angular, por ejemplo, en vez de definir una variable 'lista' que se pasa a un componente 'X', dicha variable que se asigna en el componente contenedor luego que un observable 'Y' se resuelva, se debería usar algo como <componente-x/> [propiedad-lista]="Y | async"></componente-x>
    * Usar [pipeable operators](https://github.com/ReactiveX/rxjs/blob/master/doc/pipeable-operators.md)
    * Para rxjs 5.5.x Si se usa Observable.of, Observable.throw, Observable.merge, se deben incluir en los import "rxjs/observable/of" etc.
* Métodos pequeños
* Que el código permita fácilmente cambios futuros
* Enfocarse en optimizar el rendimiento de la aplicación
    * Reducir el número de peticiones concurrentes al mismo dominio, por ejemplo, si se tiene que invocar un servicio que consulta personas, en lugar de invocarlo uno a uno, considerar el requerir que el back reciba muchas personas y devuelva la misma cantidad, esto aplica a cualquier petición.
    * Evitar imports que traigan más funcionalidades que las que se necesitan (Ejemplo los import rxjs/add, loadash, etc)
* Clases agrupadas por funcionalidad en sus respectivos archivos
* Evitar acoplamiento entre módulos y/o paquetes
* Correcto uso de patrones de diseño
* Evitar pérdida de encapsulación
* No subir **nunca** console.log en el código productivo
* Asegurar el uso adecuado del enrutamiento y la navegación y el Lazy Loading (https://v5.angular.io/guide/router). Adicionalmente, toda ruta nueva debe estar asegurada con un Guard.
* Siempre asegurarnos de cancelar las suscripción de forma apropiada utilizando operadores como take, takeUntil, unsubscribe, etc.
* Evitar tener suscripciones dentro de suscripciones.
* Evitar el uso de 'any', tiparlo todo.



## Estándares de nombramiento

* Variables, evitar nombres como a, b, c, d, e, x en expresiones lambda o como parámetros
* Seguir estándar de nombramiento definido en https://angular.io/guide/styleguide#naming
* Evitar la combinación con algún otro idioma. El oficial es español. 


## Creación de pruebas unitarias
* Probar desde las funcionalidades más simples, es decir, enfocarse a que las pruebas sean realmente **unitarias**
* Importar **solo lo que se necesita** para la ejecución de la prueba, es decir, no importar todo el módulo shared si lo que se quiere es probar un componente X que usa los componentes Y y Z de shared, en la configuración del TestBed se deben declarar solo los componentes Y y Z, no todo el módulo shared.
* Los servicios que se reciban en el constructor de lo que se va a probar (los que inyecta el proceso de DI de angular en Componentes, servicios, pipes, directivas, etc) deben ser mockeados.
* **Ninguna** prueba debe hacer uso de HttpClientModule, siempre se debe usar HttpTestClientModule.
* Si en el template se tienen invocaciones de ngIf, ngFor, ngSwitch se deben validar todas las posibles combinaciones para que los componentes se rendericen de manera adecuada.
* Evitar la constante compilación de componentes. Esto contribuye a la optimización del tiempo que toma la ejecución de las pruebas unitarias. Hacer "beforeAll" en vez de "beforeEach", procurando capturar los fallos al final. También, previo al "beforeAll", ejecutar siempre "configureTestSuite()", quien es el que tiene la implementación correspondiente para evitar compilar los componentes después de que cada test corra. (ver testing.ts en el proyecto)
Ejemplo:
```
#!Angular
configureTestSuite();
    beforeAll(done => (() => {
      dialogServiceMock = jasmine.createSpyObj('dialogServiceMock', ['confirm', 'error']);
      TestBed.configureTestingModule({
        imports: [FormsModule, RouterTestingModule.withRoutes(config), SimpleNotificationsModule],
        declarations: [TestComponent,
          NavbarComponent, AppComponent,
          HomeComponent, AboutComponent, HeaderComponent, FooterComponent],
        providers: [
          { provide: APP_BASE_HREF, useValue: '/' },
          { provide: MenuService, useClass: MenuServiceMock },
          { provide: LoginService, useClass: LoginServiceMock },
          { provide: DialogModalService, useValue: dialogServiceMock },
          { provide: MonitoreoService, useClass: MonitoreoServiceMock }
        ]
      });
      return TestBed.compileComponents();
    })().then(done).catch(done.fail));
```



## Manejo de dependencias adicionales

* Para incluir nuevos paquetes de npm, debe validarse con el grupo de arquitectos del equipo y sustentarse su uso, sea porque no hay una alternativa incluida en el proyecto o la que esta no cumple con la funcionalidad


[Volver al home](Home)