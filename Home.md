# Core Wiki

En esta wiki encontrarás todo lo que debes saber del proceso de desarrollo para el Core de Seguros.

# ESTÁNDARES PARA DESARROLLO

1. [Estándares de Desarrollo en Scala](codeStandardScala)
2. [Estándares de Desarrollo en Angular](estandarCodigoAngular)
3. [Estándares de Desarrollo en Guidewire](https://www.somossura.com/sites/negocio/seguros/core-seguros/_layouts/15/WopiFrame.aspx?sourcedoc=/sites/negocio/seguros/core-seguros/ArquitecturaeInfraestructura/SUGW%20ARQ%20Est%C3%A1ndares%20de%20desarrollo.docx&action=default)
5. [Estándares de Pruebas Unitarias en general](testStandardGeneral)
6. [Estándares de Pruebas Unitarias Guidewire](testStandardGW)
7. [Estándares de Pruebas Unitarias Scala](testStandardScala)


# POLÍTICAS PARA APROBAR PULL REQUEST (PR)

1. [CheckList para commiters en automatización proyectos BDD](politicasAutomatizacionProyectoBDD)
2. [CheckList para commiters en Front-end](FrontCommiterCheckList)
3. [CheckList para commiters en Back-end](BackCommiterCheckList)
4. [CheckList para commiters en Guidewire](GwCommiterCheckList)
5. [CheckList para commiters en Pipeline as Code](politicasDesarrolloPipeLineAsCode)

# GUÍAS PARA PROCEDIMIENTOS EN PIPELINES

1. [Descripción del proceso para Hotswaps](procesoHotswap)
2. [Publicación de una librería en artifactory con pipeline genérico](Publicación de una librería en artifactory con pipeline genérico)
3. [Revisión de pruebas de Aceptación](https://bitbucket.org/suracore/corewiki/wiki/Revisi%C3%B3n%20de%20pruebas%20de%20Aceptaci%C3%B3n)
4. [Manejo de Toggles en Hotfix](hotfixWithToggles)

# PROCEDIMIENTOS CERTIFICACIÓN 

1. [Manual Regresión Autos](https://bitbucket.org/suracore/corewiki/wiki/Manual%20Regresi%C3%B3n%20Autos)
2. [Políticas para reportar bugs](PolíticasReporteBugs)

# PROCEDIMIENTO DE DISPONIBLE PARA SALIDA A PRODUCCIÓN

1. [Procedimiento para disponible en la salida a producción](manualDisponiblePC)
2. [Manual para resolver conflictos de merge desde SourceTree](Manual para resolver conflictos de merge desde SourceTree)

# ENTRENAMIENTO CORE

1. [Entrenamiento Core](certificationCore)
2. [Guidewire QueryAPI](https://www.somossura.com/sites/negocio/seguros/core-seguros/ArquitecturaeInfraestructura/QueryAPI.pdf)
3. [Receta de Integraciones](https://www.somossura.com/sites/negocio/seguros/core-seguros/_layouts/15/WopiFrame.aspx?sourcedoc=/sites/negocio/seguros/core-seguros/ArquitecturaeInfraestructura/SUGW%20ARQ%20Receta%20de%20integraciones.docx&action=default)
4. [Crear pruebas de integración - GW](https://www.somossura.com/sites/negocio/seguros/core-seguros/_layouts/15/WopiFrame.aspx?sourcedoc=/sites/negocio/seguros/core-seguros/ArquitecturaeInfraestructura/SUGW%20ARQ%20Crear%20pruebas%20de%20integraci%C3%B3n.docx&action=default)
5. [Crear y consumir mock services - GW](https://www.somossura.com/sites/negocio/seguros/core-seguros/_layouts/15/WopiFrame.aspx?sourcedoc=/sites/negocio/seguros/core-seguros/ArquitecturaeInfraestructura/SUGW%20ARQ%20Crear%20y%20consumir%20MockServices.docx&action=default)
6. [Anti-Patrones Performance - GW](https://www.somossura.com/sites/negocio/seguros/core-seguros/ArquitecturaeInfraestructura/GosuAntiPatternsThatAffectPerformance.pdf​)
7. [Presentación Dojo Queries - GW](https://www.somossura.com/sites/negocio/seguros/core-seguros/ArquitecturaeInfraestructura/Dojo%20Queries.pdf)
8. [Manual pruebas de performance](PruebasRendimiento)

# OTROS

1. [Conectar IntelliJ IDEA con Servidor SonarQube](ConfiguracionSonarQubeServer)
2. [Configuración e Instalación de SonarQube local](InstalacionSonarQubeLocal)
3. [Manual de Uso Plugin Datamodel Upgrade](usePluginDatamodel)
4. [Configuración de TSLint para Visual Studio Code](InstalacionTslintVisualStudioCode)
5. [Manual Uso de Logs Guidewire](Manual%20Uso%20de%20Logs%20Guidewire)
6. [Query Validator](Query%20Validator)
