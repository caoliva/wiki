# Check List de Revisión de Pull Request en Back-end

[Volver al home](Home)


•Verificar que el funcionamiento implementado cumple con lo especificado en la Historia de Usuario.
•Validar que la información recibida y entregada por los servicios controladores, cumple con las necesidades funcionales.
•Validar que los logs generados den valor, no deben haber println, si son logs que van a splunk (co.com.sura.ms.base.logger.logger) deben enviar solo información valiosa (Excepciones o erroes por ejemplo), para otro tipo de logs usar un logger diferente (Por ejemplo el logger de play framework play.api.Logger )
•Adopción e interpretación de patrones.
•Manejo correcto de métodos HTTP en archivo **routes**.
•Definición correcta de rutas, de acuerdo a lineamientos HTTP y Play, en archivo **routes**.
•Validar que los estados HTTP que devuelven los controladores, deben ser acordes a la realidad de cada caso puntual.
•Manejo optimizado de recursos en los diferentes accesos que se hacen a la base de datos en cada uno de los repositorios.
•En los casos de error cuando solo se use un único case con variable como: "error" o "_" se debe tipar. Por ejemplo: (".onErrorRecover { case error: Throwable => ")

# Tener en cuenta a la hora de revisar pruebas unitarias #
•Verificar elaboración de pruebas unitarias.
•Verificar que las pruebas unitarias evalúan todos los escenarios posibles en cada uno de los escenarios.
•Validar que cada escenario de prueba cumple con la estructura **"""[Un hecho] ... Cuando ... Debería"""**, ayudándonos de las palabras clave de ScalaTest (verbos). No aplica concatenar usando el operador + porque causa advertencias en tiempo de compilación, lo que ralentiza dicho proceso.

```
#!scala

"Al consultar los estados agrupados de tarifacion" when {
      "- La consulta a BD de estados trae 3 estados" must {
        "- Devuelve los tres estados de tarifa en una lista y el resultado es exitoso" in new WithApplication( testApp ) {
          val dependencia = app.injector.instanceOf[FalseConfigurations]( classOf[FalseConfigurations] )
          mockearFuncionObtenerEstadosTarifaAgrupadosConExito( app, dependencia )
          val Some( result ) = route( app, FakeRequest( GET, "/autos-gc/consultas/operaciones/123456789/acciones/estadosTarifacion" ) )
          val json = Json.parse( contentAsString( result ) )
          val info = readsRespuestaExitoHTTP[List[EstadoTarifacion]]( json ).get
          logger.warn( info.toString, getClass )
          status( result ) mustBe OK
          info.respuesta mustBe List( TARIFADO, SIN_TARIFAR_ERROR, POR_TARIFAR )
        }
      }
}
```

•Si realiza algún ajuste sobre un metodo que tiene pruebas unitarias con malas practicas, se debe arreglar dichas pruebas unitarias
•Validar que lo que se puebe en la prueba unitaria sea coherente con el nombre de la prueba
•Se debe utilizar siempre testKit.withRouter con el fin de que cuando levante la app haga uso del application.conf definado y que puede tener modulos desactivados y no el de la applicación que es el que levanta al usar server.withRouter, esto se hace con el fin de mejorar la inicialización de la app en las pruebas.
•En las pruebas de los retrieve solo se debe validar que se carguen los datos, no se debe llamar el retrieve
•Los repositorios y servicio como object impiden el mockeo de las pruebas. Esto Causa:
    
       * Probar 2 capas de la aplicación en una sola prueba
       * Obliga a levantar el router para la app, solo para simular la respuesta del servicio del edge, lo que degrada 
       los tiempos de ejecución de las pruebas
       * Cubrir todos los posibles caminos se hace demasiado complejo y no siempre el resultado es satisfactorio

     Se recomienda entonces, 
* tener los repositorios y servicios en dependencias, 
* en las pruebas que no requieren usar FakeRequest usar la clase FalseConfigurations directamente (`new FalseConfiguration(...)`) en lugar de hacer  

```
#!scala

in new WithApplication(testApp) {
       val dependencia = app.injector.instanceOf[FalseConfigurations]( classOf[FalseConfigurations] )
}
```
Hacer
```
#!scala

in {
       val dependencia = new FalseConfigurations(parametrosmockeadosanecesidadonull)
}
```
 la sobreescribe y mockea estas dependencias, logrando así:

       * Simular las respuestas
       * Cubrir los diferentes caminos
       * Cubrir todas las ramas de ejecución
       * Evita la invocación del EDGE
       * Mejora mucho los tiempos de respuesta

•Validar que no se esté haciendo uso en las aserciones del "any" sin especificar el tipo de dato cuando es un parámetro simple o firma del método cuando se pasa una función como parámetro.

•Validar que no se esté cargando inecesariamente la aplicación como recurso en los diferentes mocks y set de pruebas definidos.

[Volver al home](Home)