# Estándares de Pruebas

[Volver al home](Home)

## Políticas definidas para unitarias en Scala

•Para la construcción de los test Unitarios en Scala se recomienda usar ScalaTest y PlaySpec, sin embargo otras librerías o frameworks podrán ser usadas pero después de ser analizadas con el equipo de Arquitectura del proyecto, esto debido a que se debe tomar la decisión analizando todos los drivers que requiera el proyecto y para no tener muchas herramientas que impidan tener una propiedad colectiva del código.

•Las capas de la aplicación que requieren pruebas unitarias son: Repositorios, Comandos, Controladores, Lógica de Negocio, Transformadores que no sean de BD.

•Para la preparación de data en el Arrange, se debe usar el Patrón “Object Mother” o el patrón “Factory” para dejar métodos que fácilmente entreguen la data de test para otros desarrolladores, habilitando fuertemente el reúso. Sin embargo, se debe tener en cuenta que no se deben usar las instancias retornadas por estas fábricas, ya que podrían hacer que los test sean dependientes, por lo tanto deben usarse copias de estas instancias y realizar las modificaciones que se requieran.

•Reducir el uso de Objects para permitir la realización de pruebas unitarias, en su lugar puede hacerse inyección de dependencias.

•Generar la mayor cobertura posible (de escenarios que generen valor) en pruebas unitarias por su bajo costo en ejecución, de ahí pruebas unitarias** con dependencias y luego pruebas de integración (que prueban más de una funcionalidad).

•Cada escenario de prueba debe cumplir con la estructura """[Un hecho] ... Cuando ... Debería""", ayudándonos de las palabras clave de ScalaTest (verbos). De esta manera podemos tener una clase de prueba más ordenada con subconjuntos de test que le apunten a un mismo propósito o funcionalidad. No aplica concatenar usando el operador + porque causa advertencias en tiempo de compilación, lo que ralentiza dicho proceso.

### Ejemplo de Test Unitario aplicando las políticas Scala

```scala
package co.com.sura.gc.mrc.comandos

import co.com.sura.gc.mrc.dto.ResultadoComandosAccion
import co.com.sura.gc.mrc.formats.FormatsDTO.formatResultadoDMLAccionDTO
import co.com.sura.gc.mrc.AppTestKit
import co.com.sura.gc.mrc.mocks.MockFuncionesRepoAccionBorrador
import play.api.libs.json.Json
import play.api.test.Helpers.{ POST, contentAsString, route, status, _ }
import play.api.test.{ FakeRequest, WithApplication }

import scala.io.Source

class ComandoActualizarAccionesTest extends AppTestKit with MockFuncionesRepoAccionBorrador {

  "Comando ActualizarAccion" should {

    "modificar Accion correctamente" in new WithApplication( testApp ) {

      mockearFuncionModificarAccionConExito( app )

      val stringAccion: String = Source.fromFile( "test/resources/comandos/accion/peticionActualizarAcciones.json" ).getLines.mkString
      val Some( result ) = route( app, FakeRequest( POST, "/mrc-gc/comando/actualizarAcciones" ).withBody( Json.parse( stringAccion ) ) )
      val info = contentAsString( result )
      println( "La info es: " + info )
      val resultadoDTO = Json.parse( info ).validate[ResultadoComandosAccion].get
      status( result ) mustEqual OK
      1 mustBe resultadoDTO.listaAccionesOK.length
    }
  }

  "Comando ActualizarAccion" should {

    "modificar error si falla al guardar la accion en la BD" in new WithApplication( testApp ) {

      mockearFuncionModificarAccionConError( app )

      val stringAccion: String = Source.fromFile( "test/resources/comandos/accion/peticionActualizarAcciones.json" ).getLines.mkString
      val Some( result ) = route( app, FakeRequest( POST, "/mrc-gc/comando/actualizarAcciones" ).withBody( Json.parse( stringAccion ) ) )
      val info = contentAsString( result )
      val resultadoDTO = Json.parse( info ).validate[ResultadoComandosAccion].get
      status( result ) mustEqual OK
      1 mustBe resultadoDTO.listaAccionesFail.length
    }
  }

  "Al consultar los estados agrupados de tarifacion" when {
      "- La consulta a BD de estados trae 3 estados" must {
        "- Devuelve los tres estados de tarifa en una lista y el resultado es exitoso" in new WithApplication( testApp ) {
          val dependencia = app.injector.instanceOf[FalseConfigurations]( classOf[FalseConfigurations] )
          mockearFuncionObtenerEstadosTarifaAgrupadosConExito( app, dependencia )
          val Some( result ) = route( app, FakeRequest( GET, "/autos-gc/consultas/operaciones/123456789/acciones/estadosTarifacion" ) )
          val json = Json.parse( contentAsString( result ) )
          val info = readsRespuestaExitoHTTP[List[EstadoTarifacion]]( json ).get
          logger.warn( info.toString, getClass )
          status( result ) mustBe OK
          info.respuesta mustBe List( TARIFADO, SIN_TARIFAR_ERROR, POR_TARIFAR )
        }
     }
  }
```

•Tratar de especificar detalladamente los parámetros de las aserciones, sin hacer uso de los "any" genéricos. En caso tal que no se conozcan los valores específicos en ciertos escenarios hacer uso del "any" con la indicación del tipo manejado y la firma para cuando es una función la que se envía como parámetro. Incluso si se conocen algunos de los valores, se puede hacer la mezcla haciendo uso del "eq" de Mockito (Se debe escribir con la extensión completa, para no tener problemas con la compilación).

Ejemplo de la manera incorrecta:
```scala
"No se generan evento de prima minima. Se generan eventos de finalizacion de tarifa" must {
        "Debe retornar la lista de eventos generada" in {

          val dependencia = new FalseConfigurations( null, null, null, null )
          val idOperacion = IdOperacion( "123" )
          val listaEventos = List( TarifaSolicitadaFactory.crearTarifaSolicitada )
          val spyServicio = spy( new MockServicioProcesamientoTarifaOperacion )

          mockearFuncionConsultaResumenDatosPolizaExito( dependencia )

          doReturn( Reader { _: Dependencia =>
            EitherT.rightT[Task, ErrorServicio]( Nil )
          } ).when( spyServicio ).generarEventosTasaGlobal( idOperacion )

          doReturn( Reader { _: Dependencia =>
            EitherT.rightT[Task, ErrorServicio]( Nil )
          } ).when( spyServicio ).generarEventoPrimaMinimaSolicitada( idOperacion )

          doReturn( Reader { _: Dependencia =>
            EitherT.rightT[Task, ErrorServicio]( listaEventos )
          } ).when( spyServicio ).generarEventosFinalizacionTarifa( idOperacion )

          val resultado =
            waitForFutureResult( spyServicio.procesarTarifaOperacionOk( idOperacion ).run( dependencia ).value.runAsync )

          resultado mustBe Right( listaEventos )
          verify( spyServicio ).generarEventosTasaGlobal( any )
          verify( spyServicio ).generarEventoPrimaMinimaSolicitada( any )
          verify( spyServicio ).generarEventosFinalizacionTarifa( any )
          verify( spyServicio, times( 3 ) ).validadorGeneracionEventos( any, any, any )( any )
        }
}
```

Ejemplo de la manera correcta:
```scala
"No se generan evento de prima minima. Se generan eventos de finalizacion de tarifa" must {
        "Debe retornar la lista de eventos generada" in {

          val dependencia = new FalseConfigurations( null, null, null, null )
          val idOperacion = IdOperacion( "123" )
          val listaEventos = List( TarifaSolicitadaFactory.crearTarifaSolicitada )
          val spyServicio = spy( new MockServicioProcesamientoTarifaOperacion )

          mockearFuncionConsultaResumenDatosPolizaExito( dependencia )

          doReturn( Reader { _: Dependencia =>
            EitherT.rightT[Task, ErrorServicio]( Nil )
          } ).when( spyServicio ).generarEventosTasaGlobal( idOperacion )

          doReturn( Reader { _: Dependencia =>
            EitherT.rightT[Task, ErrorServicio]( Nil )
          } ).when( spyServicio ).generarEventoPrimaMinimaSolicitada( idOperacion )

          doReturn( Reader { _: Dependencia =>
            EitherT.rightT[Task, ErrorServicio]( listaEventos )
          } ).when( spyServicio ).generarEventosFinalizacionTarifa( idOperacion )

          val resultado =
            waitForFutureResult( spyServicio.procesarTarifaOperacionOk( idOperacion ).run( dependencia ).value.runAsync )

          resultado mustBe Right( listaEventos )
          verify( spyServicio ).generarEventosTasaGlobal( idOperacion )
          verify( spyServicio ).generarEventoPrimaMinimaSolicitada( idOperacion )
          verify( spyServicio, times( 3 ) )
            .validadorGeneracionEventos( org.mockito.Matchers.eq(idOperacion), org.mockito.Matchers.eq(Nil), org.mockito.Matchers.eq(true) )( any[IdOperacion => Reader[Dependencia, EitherTResult[List[Event]]]] )
        }
}
```
•Evitar la carga innecesaria de la aplicación tanto en la definición de mocks como en los diferentes set de pruebas, dejarlo sólo para los escenarios donde sea extrictamente necesario. Esto optimiza en gran medida el tiempo de las pruebas unitarias.

Ejemplo de la manera incorrecta:
```scala
def mockRepositorioGuardarRiesgosConExito( app: Application ) = {
    val dependencia = app.injector.instanceOf[FalseConfigurations]( classOf[FalseConfigurations] )
    dependencia.repoAuditoria.guardarRiesgos( any, any ) returns Reader { _ =>
      EitherT.rightT( Done )
    }
  }
```
```scala
class TareaServiceTest extends AppTestKit {
  "Comando Generar Tareas" should {
    "actualizar accion exitosa" in new WithApplication( testApp ) {
      val dependecia = app.injector.instanceOf[FalseConfigurations]( classOf[FalseConfigurations] )
      MockFuncionesTareas.mockearConsultaPaginadaSinDatos( app )
      MockFuncionesOpciones.mockearConsultaOpcionesSinDatos( app )
      val x = TareaService.listarTareas( "1", "1", None, None, None, None, None, None, None, None, None, None, None, None, None, None, true ).run( dependecia )
      val resultado: Seq[TareaDTO] = Await.result( x, Duration.Inf )
      resultado mustBe Nil
    }

    "consultar tarea exitosa" in new WithApplication( testApp ) {
      val dependecia = app.injector.instanceOf[FalseConfigurations]( classOf[FalseConfigurations] )
      MockFuncionesTareas.mockearConsultaPaginadaConDatos( app )
      MockFuncionesOpciones.mockearConsultaOpcionesConDatos( app )
      val x = TareaService.listarTareas( "1", "1", None, None, None, None, None, None, None, None, None, None, None, None, None, None, true ).run( dependecia )
      val resultado: Seq[TareaDTO] = Await.result( x, Duration.Inf )
      assert( resultado nonEmpty )
      assertResult( List( "url1/123", "url2/123", "url1/123", "url2/123", "url1/123", "url2/123" ) ) {
        resultado.flatMap( x => x.opciones.map( _.url ) )
      }
    }
  }
}
```

Ejemplo de la manera correcta:
```scala
def mockRepositorioGuardarRiesgosConExito( dependencia: Dependencia ) = {
    dependencia.repoAuditoria.guardarRiesgos( any, any ) returns Reader { _ =>
      EitherT.rightT( Done )
    }
  }
```
```scala
class TareaServiceTest extends AppTestKit with MockFuncionesTareas with MockFuncionesOpciones {
  "Comando Generar Tareas" should {
    "actualizar accion exitosa" in {
      val dependecia = getInstanceFalseConfigurations
      mockearConsultaPaginadaSinDatos( dependecia )
      mockearConsultaOpcionesSinDatos( dependecia )
      val x = TareaService.listarTareas( "1", "1", None, None, None, None, None, None, None, None, None, None, None, None, None, None, true ).run( dependecia )
      val resultado: Seq[TareaDTO] = Await.result( x, Duration.Inf )
      resultado mustBe Nil
    }

    "consultar tarea exitosa" in {
      val dependecia = getInstanceFalseConfigurations
      mockearConsultaPaginadaConDatos( dependecia )
      mockearConsultaOpcionesConDatos( dependecia )
      val x = TareaService.listarTareas( "1", "1", None, None, None, None, None, None, None, None, None, None, None, None, None, None, true ).run( dependecia )
      val resultado: Seq[TareaDTO] = Await.result( x, Duration.Inf )
      assert( resultado nonEmpty )
      assertResult( List( "url1/123", "url2/123", "url1/123", "url2/123", "url1/123", "url2/123" ) ) {
        resultado.flatMap( x => x.opciones.map( _.url ) )
      }
    }
  }
}
```
•**Paralelización de pruebas:** SBT por defecto corre las pruebas unitarias en paralelo, haciendo uso eficiente del número de CPUs disponibles en la máquina.
 

[Volver al home](Home)