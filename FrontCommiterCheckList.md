# Check List de Revisión de Pull Request en Front-end

[Volver al home](Home)


1. Verificar que el funcionamiento implementado cumple con lo especificado en la Historia de Usuario.
2. Validar que el funcionamiento de la aplicación es fluida.
3. Adopción e interpretación de patrones.
4. Verificar manejo de formularios. Deben hacer uso de reactive forms en lugar de template driven forms. Bajo ningún caso se deben mezclar ambos estilos en la misma plantilla. 
5. Verificar manejo de componentes.
6. Verificar manejo de servicios.
7. Validar manejo centralizado de constantes.
8. Validar manejo centralizado de validaciones.
9. Verificar nombramiento de recursos (variables, métodos, artefactos, etc.), seguir definiciones de estándar de nombramiento definido en: [Link Guía de estilos Angular](Link https://angular.io/guide/styleguide#naming)
10. Captura, manejo y propagación adecuadas de los errores en los diferentes artefactos involucrados. Por ejemplo, al hacer catch de un observable y luego usar Observable.throwError no agrega realmente nada al flujo.
11. Verificar elaboración de pruebas unitarias.

* Validar que estén importando solo lo necesario para las pruebas (Por ejemplo no importar todo el módulo Shared si solo se requieren un par de componentes o directivas)
* Validar que se prueben funcionalidades específicas y que no se busque solo dar cobertura probando funcionalidades que pasan por muchos puntos.
* Si se tienen ramificaciones(if, else, switch), validar que se tengan en cuenta todos los posibles caminos.
* Validar que también se prueben escenarios no esperados (valores undefined, null, etc)
* Validar que dependencias entre funcionalidades sean mockeadas, por ejemplo si la clase ServiceA hace uso de funcionalidades de ServiceB, para probar ServiceA, se debe mockear ServiceB para asegurar que la prueba es unitaria.
* Validar que no se esté incurriendo en la constante compilación de componentes, haciendo uso del "configureTestSuite()" y el "beforeAll".

12. Verificar que las pruebas unitarias evalúan todos los escenarios posibles en cada uno de los escenarios.
13. Validar la estructura de carpetas, validando que no se creen estructuras que dificulten la navegación, por ejemplo, si se tienen diez componentes que tienen la funcionalidad de dropdown, considerar ponerlos bajo la misma carpeta. 
14. Lineamentos en el uso de rxjs:

* Para versiones 5.5+: No debe existir ningún import del tipo rxjs/add/operadores, todos los imports de operadores deben ser en la forma de rxjs/operators/operador, y usando pipe.
* Para versiones menores a 6.0: Aunque se haga uso de Observable.of, Observable.throw, dejar en los imports referencias a estos métodos, es decir por ejemplo, especificar: `import {of} from 'rxjs/observable/of'` así no sea necesario para el funcionamiento en rxjs 5.x, simplifica la actualización.
* Se debe validar la desuscripción para evitar memory leaks.

15. Verificar que los elementos del html que sean testiables, ejemplo: input, label, button, tabs, etc. tenga una id asignada y asegurar que sea única.

[Volver al home](Home)