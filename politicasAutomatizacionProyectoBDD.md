# ACUERDOS PARA LA AUTOMATIZACIÓN DE PRUEBAS EN EL CORE DE SURAMERICANA

[Volver al home](Home)

![automation-testing.png](https://bitbucket.org/repo/akpjXAz/images/1547862878-automation-testing.png)

Ítems sobre: | Última actualización (DMA)
:------------------------ |:-------------------------------------:
1. [Estructura de proyecto](politicasAutomatizacionProyectoBDD_Estructura)|
2. [Consideraciones PullRequest](politicasAutomatizacionProyectoBDD_Varios)|__25/07__/2019 ![arrow.png](https://bitbucket.org/repo/akpjXAz/images/625815946-arrow.png)
3. [Nombramientos](politicasAutomatizacionProyectoBDD_Nombramientos)|__09/05__/2019
4. [Formato](politicasAutomatizacionProyectoBDD_Formato)|__16/07__/2019 ![arrow.png](https://bitbucket.org/repo/akpjXAz/images/625815946-arrow.png)
5. [Esquema de trabajo para Pull Request](politicasAutomatizacionProyectoBDD_EsquemaRevisores)|
6. [Tip - Adicionar certificados](politicasAutomatizacionProyectoBDD_TipAdicionarCertificados)|
7. [Tip - Consumir cliente que ejecuta servicio](politicasAutomatizacionProyectoBDD_TipConsumoClienteServicio)|
8. [Tip - Ejecutar solo un escenario incluido en .feature](politicasAutomatizacionProyectoBDD_TipEjecutarSoloEscenarioEspecifico)|