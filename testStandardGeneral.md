# Estándares de Pruebas

[Volver al home](Home)

## Políticas definidas para unitarias en general

•Una unidad de código para el proyecto se define como un método o función, es decir, las pruebas unitarias se construyen para métodos o funciones aisladas y deben ser aisladas utilizando mocks o haciendo refactors que permitan realizar los test. En el “Ejemplo de Test Unitario usando políticas Generales” puedes ver como se mockea la función ***getFactorTarifa*** para poder testear ***tarifar***.

•El nombramiento del Test debe ser diciente, es decir, con leer el test debe ser suficiente para determinar lo que se está testeando. Recuerda que un método o función test es diferente a uno de código productivo, por lo tanto no tenemos que ser egoístas con la cantidad de caracteres usados para nombrar el test. En el “Ejemplo de Test Unitario usando políticas Generales” puedes ver el test nombrado que fácilmente permite inferir lo que se está probando y el escenario específico: ***tarifarValorAseguradoCeroDebeRetornarCero***

•Los test deben ser construidos siguiendo el patrón conocido como “las 3 A”, para diferenciar todas las partes que componen el test. “Arrange” es el espacio donde se debe preparar todo lo requerido para el test, como los datos del escenario, los mocks, entre otros. El “Act” es el espacio donde se ejecuta la prueba, es decir, es el lugar donde se invoca la función a testear unitariamente. El “Assert” es el espacio donde se hacen las validaciones que requiere el test y que será la condición que indicará el resultado que tenga el test en tiempo de ejecución (Test OK o FAIL). Como se puede ver en el “Ejemplo de Test Unitario usando políticas Generales”, se marcan con comentarios los diferentes espacios que componen el test y se dejan saltos de línea para delimitar cada uno de los espacios y recalcar donde inicia y termina cada uno (Tener en cuenta que los comentarios son ilustrativos y no se espera que para cada test se pongan estos comentarios, pero si se recomienda usar el salto de línea para delimitarlos).

•Los test deben cumplir los principios FIRST que se explican a continuación:

>**Fast**: Los test deben ser rápidos y debemos buscar la manera de construirlos de la mejor forma posible para que cada vez sean más rápidos, esto buscando que se pueda recibir el feedback de la regresión en el menor tiempo posible y hacer que los bugs sean lo menos costoso posible para el equipo del proyecto.
>**Independent**: Cada test debe ser independiente del otro, esto para facilitar paralelizaciones o ejecuciones en órdenes aleatorios de los test. No se permite que los test tengan que correr en un orden específico para que pueda quedar OK. No se permite que los datos usados en un test, sean usados en otro.
>**Repeteable**: Los test deben ser repetibles y si se corren varias veces sin cambiar el código bajo test o los mismos test, siempre deben dar el mismo resultado.
>**Self-validating**: Cada test debe validarse por sí mismo, es decir que no se permite que una persona tenga que leer un log u otro artefacto para determinar si un test falló o no. Si cada test implementa los asserts correctamente, los test serán self-validating.
>**Timely**: Este principio indica que los test deben ser definidos y ejecutados en los momentos oportunos. La recomendación es que el test sea creado antes de crear el código productivo, es decir, se recomienda usar TDD pero no es obligatorio, usar TDD garantiza que el código construido sea testeable y evita que tengas que hacer grandes refactor posteriores para construir los test. Respecto a la ejecución siempre serán ejecutados en el ciclo de integración continua del equipo y es opcional el correrlos antes de hacer la integración del código, esta recomendación es para evitar que subas código al repositorio que puede llegar a fallar cuando se le haga la regresión de pruebas unitarias.

•Toda clase de test debe ser creada en los mismos paquetes que la clase a testear y debe ser nombrada con el mismo de la clase bajo test terminando con la palabra Test usando la notación de camello. Por ejemplo la clase de test de la clase ***com.sura.calculadora.CalculadoraTarifa*** será ***com.sura.calculadora.CalculadoraTarifaTest***.

•Una clase debe tener mínimo una clase de test y el criterio para dividir los test queda a discreción del equipo que trabaja en ella, pero recuerda que una clase de mil líneas está mal vista y aplica tanto para código productivo como para los test. Por ejemplo para el caso de la CalculadoraTarifa, podríamos tener 2 clases de test nombrados de la siguiente manera: ***com.sura.calculadora.CalculadoraTarifaTest1*** y ***com.sura.calculadora.CalculadoraTarifaTest2***.

•Toda función del código productivo debe ser testeada arduamente, es decir, debemos construir tantos test unitarios como escenarios podamos encontrar en el código bajo test, así si estamos testeando una sentencia IF, mínimo debemos testear cuando se cumpla la condición y otro escenario para cuando no se cumpla. Debemos tener en cuenta que no solo se testean los escenarios felices, también los alternos, de excepción y algunos extremos. El objetivo es tener la mayor protección posible para que cuando necesitemos hacer cambios o refactors, podamos estar protegidos por una regresión robusta de pruebas unitarias.

•Para reducir la deuda técnica en pruebas unitarias que pueda tener un proyecto, se define que cuando se modifica una función, se debe garantizar que esa función tenga construidos todos los escenarios posibles y en caso de que no los tenga, deben ser construidos por la persona que realizó la modificación (“Dejamos las cosas mejor que como las encontramos”).

### Ejemplo de Test Unitario aplicando las políticas generales

```java
package com.sura.calculadora;

public class CalculadoraTarifa {
    RepositorioFactores repositorioFactores = new RepositorioFactores();

    public double tarifar(double valorAsegurado, String idFactor){
        double factor = getFactorTarifa(idFactor);
        double resultado = valorAsegurado * factor;
        return resultado;
    }

    public double getFactorTarifa(String idFactor){
        return repositorioFactores.consultarFactor(idFactor);
    }
}
```

```java
package com.sura.calculadora;

import org.junit.Test;
import org.mockito.Mockito;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class CalculadorTarifaTest {
    @Test
    public void tarifarValorAseguradoCeroDebeRetornarCero() {
        //Arrange
        double valorAsegurado = 0;
        String idFactor = "Factor1";
        CalculadoraTarifa calculadora = new CalculadoraTarifa();
        calculadora = Mockito.spy(calculadora);
        Mockito.doReturn(1.0).when(calculadora).getFactorTarifa(Mockito.anyString());

        //Act
        double resultado =  calculadora.tarifar(valorAsegurado, idFactor);

        //Assert
        assertThat(resultado, equalTo(0.0));
    }
}

```

[Volver al home](Home)