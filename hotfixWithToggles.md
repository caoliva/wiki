# Manejo de Toggles en Hotfix

[Volver al home](Home)

Cuando en el pipeline de Quality se hace el merge de quality a master y resultan conflictos, estos normalmente los soluciona un desarrollador, por lo cual el commit en master queda a nombre del desarrollador y el pipeline de master cuando se ejecuta lo detecta como un Hotfix.

Es por eso que cuando se resuelvan los conflicos del merge y se lance el pipeline de master, aparecerá un **input** por 20 min donde se pregunta si el ajuste es para un paso normal de producción con regresión.

Esta implementación se hace para poder identificar cuando el pipeline detecte un Hotfix, poder saber si en realidad es un Hotfix o si fue por que estaban resolviendo conflictos del merge entre Quality y Master.

El uso sería:

- Cuando sea un "Hotfix" por resolver conflictos de un paso normal de producción con regresión, se debe dar clic en Proceed.

- Cuando sea un Hotfix por corregir un error en PDN, se debe indicar que **NO** aplique los toggles.


[Volver al home](Home)