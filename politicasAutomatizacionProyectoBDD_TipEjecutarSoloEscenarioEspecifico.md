#ASOCIAR UN RUNNER INDEPENDIENTE PARA UN ESCENARIO ESPECÍFICO CONTENIDO EN UN FEATURE

[Volver al menu](politicasAutomatizacionProyectoBDD)

## 1. Agregar un @tag en el escenario como se muestra a continuación: ##

![feature.png](https://bitbucket.org/repo/Lodj655/images/890960126-feature.png)


El nombramiento del @tag debe ser diciente y descriptivo. Dicho nombre podría ser el mismo nombre del feature y con un sufijo que indique un determinado orden o consecutivo de escenario (en caso que aplique - Por favor realizar análisis). Ejemplo correcto: @renovacion_poliza_auto_individual_01 | Ejemplo incorrecto: @escenario_01

## 2. Crear el runner para ese @tag agregado: ##

![archivo_runner.png](https://bitbucket.org/repo/Lodj655/images/2795901354-archivo_runner.png)

El nombramiento del runner debe contener el número del escenario antes de la palabra Runner.

**Ejemplo:** RenovacionPolizaAutoIndividualRunner

## 3. En el código del runner, agregar el @tag del escenario que se requiere ejecutar: ##

![codigo_runner.png](https://bitbucket.org/repo/Lodj655/images/2723844135-codigo_runner.png)

## **Notas**: ##

De esta forma los escenarios de un mismo feature se pueden distribuir entre los lotes que se tengan dispuestos para la ejecución; y así:

* Distribuir cargas de ejecución en diferentes lotes
* Optimizar el tiempo de ejecución de las pruebas automatizadas