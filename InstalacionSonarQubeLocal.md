# Instalación de SonarQube local #

[Volver al home](Home)

* Descargar el aplicativo del siguiente link:

SonarQube Windows y Linux: https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-6.7.6.zip

SonarScanner Windows: https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-windows.zip 

SonarScanner Linux:
https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-linux.zip

* Descomprimir el archivo preferiblemente en la raiz “C:/” deben quedar dos carpetas, sonarqube y sonar-scanner.

* Agregar la variable de entorno “SONAR_HOME” apuntando al directorio sonar-scanner

![VariableEntorno.png](https://bitbucket.org/repo/Lodj655/images/1510791685-VariableEntorno.png)

* Agregar en el path la variable creada. %SONAR_HOME%\bin

![Path.png](https://bitbucket.org/repo/Lodj655/images/378977470-Path.png)

## Configurar proyecto para su análisis ##
ir la ruta donde se instaló Sonar Scanner e ingresar a la carpeta conf (D:\sonar\sonar-scanner\conf)
editar el archivo *sonar-scanner.properties* de la siguiente manera:

Descomentar la línea

* sonar.host.url=http://localhost:9000

Agregar las siguientes líneas:

* sonar.projectKey=bddcoresuraproduccion 

* sonar.projectName=bddcoresuraproduccion

* sonar.projectVersion=1.0

* sonar.sources=src/

* sonar.language=java

* sonar.sourceEncoding=UTF-8

* sonar.java.binaries=.

(Reemplazar "bddcoresuraproduccion" por el nombre del proyecto que desee escanear)

## Ejecución de SonarQube ##

* Para iniciar el servicio de SonarQube, ejecutamos el archivo StartSonar.bat ubicado en la ruta C:\sonarqube\bin\windows-x86-64\StartSonar.bat
![rutasonar.png](https://bitbucket.org/repo/akpjXAz/images/4015984912-rutasonar.png)

* Esto abrirá una consola, la cual estará activa durante todo el tiempo que esté arriba el servicio de sonar, el servicio se puede detener usando las teclas CTRL+C.
![consolasonar.png](https://bitbucket.org/repo/akpjXAz/images/1728988634-consolasonar.png)

* Por último, abrimos sonar usando la URL: http://localhost:9000/ 

Realice la autenticación con las credenciales por defecto que son: admin/admin.

![sonar.png](https://bitbucket.org/repo/akpjXAz/images/1255007951-sonar.png)

## Instalar los plugins ##
Para instalar los plugins, se debe ir al menù administration, MarketPlace y buscar e instalar los siguientes plugins:

* Android
* FindBugs
* Checkstyle

una vez instalados es necesario reiniciar el servidor de SonarQube deteniendo la ejecución desde la consola (Crtl + C) y relanzando con el archivo StartSonar.bat.

Nota: este reinicio se puede hacer desde la misma página de instalación de plugins (ir a la parte alta de la página donde aparece la opción de reiniciar)

## Agregar reglas definidas por Sura ##

* Descargar el archivo XML con las reglas de este [Link](https://bitbucket.org/suracore/corewiki/src/master/ReglasSonarSuraCalidad.xml) 

* Ingresar nuevamente a la url del servidor (http://localhost:9000/) usando las credenciales admin/admin.

* Ingresar a la opción Quality Profiles, hacer clic en el botón ▼  (que está en la parte superior derecha, justo al lado del botón "Create") y seleccionar la opción de: Restore Profile.

![qualityprofiles.png](https://bitbucket.org/repo/akpjXAz/images/2727937014-qualityprofiles.png)

* En la ventana emergente deberá cargar el archivo XML con las reglas.

* Una vez importadas las reglas, deberá aparecer la opción SuraJava en el listado de profiles.

![qualityprofiles2.png](https://bitbucket.org/repo/akpjXAz/images/1142440907-qualityprofiles2.png)

* Realizar un escaneo inicial del proyecto como se especifica a continuación.
## Escanear proyecto con Sonar ##

Es importante que antes de escanear se esté actualizado con develop. Para escanear el proyecto con el fin de actualizar la medición del sonar una vez se hayan hecho las correcciones, abrimos una terminal de Windows y navegamos a la raíz del proyecto, luego ejecutamos el comando “sonar-scanner.bat”

![sonarscanner.png](https://bitbucket.org/repo/akpjXAz/images/728559084-sonarscanner.png)

Nota:
En ocasiones no se inicia la ejecución solo usando el comando: "sonar-scanner.bat"; cuando ocurra ésto (y estando en una ventana de línea de comando) se sugiere navegar hasta la ruta del proyecto que se desea escanear y ejecutar el comando pero anteponiendo la ruta completa donde se encuentra el: sonar-scanner.bat.

* Una vez escaneado el proyecto, el resultado se puede consultar en el servidor local de SonarQube.

![sonarscanner2.png](https://bitbucket.org/repo/akpjXAz/images/2004310186-sonarscanner2.png)

![sonarscanner3.png](https://bitbucket.org/repo/akpjXAz/images/2289954584-sonarscanner3.png)


Para continuar con la configuración de Sonar, realizamos los siguientes pasos:

* Volver a SonarQube y hacer clic en el perfil SuraJava, en la sección Projects hacer clic sobre el botón ChangeProjects, en la ventana emergente hacer clic en All y checkear la casilla para asociar el perfil con el proyecto BDDCoreSuraProduccion.

![changeprojects.png](https://bitbucket.org/repo/akpjXAz/images/961471883-changeprojects.png)
![asociar_proyecto_all.png](https://bitbucket.org/repo/akpjXAz/images/3454598191-asociar_proyecto_all.png)
![proyecto_asociado.png](https://bitbucket.org/repo/akpjXAz/images/238292671-proyecto_asociado.png)

* Por ultimo realizar nuevamente un escaneo para que éste se haga teniendo en cuenta las reglas de Sura.

## Integración de SonarQube con intelliJ ##

Una vez se tenga instalado SonarQube en forma local, se puede integrar a IntelliJ mediante el plugin SonarQube community.
* El plugin se instala mediante la opción, File-> Settings -> Plugins-> Browse repositories-> SonarQube Community Plugin.
* Una vez instalado el plugin, aparecerá en el menú “File-> Settings”, la opción “SonarQube”, allí se debe configurar el SonarQube Server y el SonarQube resources con los siguientes parámetros:

- SonarQube Server

![sonarplugin.png](https://bitbucket.org/repo/Lodj655/images/3000893056-sonarplugin.png)

- SonarQube Resources
Luego de agregar el SonarQube Server, se agrega el SonarQube resource, mediante las opciones “+-> Download resources-> bddsuracoreproduccion-> ok”

![sonarpluginresources.png](https://bitbucket.org/repo/Lodj655/images/3128141237-sonarpluginresources.png)

### Analizar el proyecto ###

Para analizar el código desde el IDE Intellij, damos click derecho a la raíz del proyecto, seleccionamos la opción “Analize -> Inspect Code” y parametrizamos el análisis de la siguiente manera:
Seleccionamos las opciones “Whole Project” e “Include test resources”

![sonarpluginanalisis.png](https://bitbucket.org/repo/Lodj655/images/760774542-sonarpluginanalisis.png)

En el apartado Inspection profile, dejamos marcadas solo las opciones “SonarQube” y “SonarQube (new issues)”

![sonarpluginanalisis2.png](https://bitbucket.org/repo/Lodj655/images/2212737890-sonarpluginanalisis2.png)

Al finalizar el análisis, aparecerán los resultados en la parte inferior con la posibilidad de acceder directamente al apartado del código donde se encuentra el error.
![resultados_sonar_intelliJ.png](https://bitbucket.org/repo/akpjXAz/images/925379422-resultados_sonar_intelliJ.png)

[Volver al home](Home)