# ESQUEMA DE TRABAJO PARA PULL REQUEST ( AUTORES / REVISORES ) #

[Volver al menu](politicasAutomatizacionProyectoBDD)

A continuación se describen las características sobre el esquema de trabajo y/o dinámica que se ejerce entre los autores y los revisores de pull Request generados sobre un proyecto BDD de automatización.

> **Nota**: en la presente documentación se llamará *Artefacto del proyecto BDD de automatización*, a todo archivo que tenga una relevancia para la implementación de un escenario automatizado de prueba. Ejemplo: cualquier archivo con extensión .java, archivos con extensión .csv, entre otros.

## TERMINOLOGÍA ##
En aras de mejorar el entendimiento común, a continuación se describe de forma breve cierta terminología que puede ser escuchada y/o usada durante el desarrollo del proyecto de automatización. Algunos de éstos términos son:

**AUTOR**: también llamado COMMITER. Es el analista que hace ajustes o nuevos artefactos dentro del proyecto BDD de automatización, siempre velando por:

- Conservar las buenas prácticas en la implementación.
- Cumplir con los acuerdos definidos para la automatización de escenarios de prueba en un proyecto BDD.
- Generación de artefactos estables.
- Generación de artefactos que presenten un buen nivel de reutilización (ejm: artefactos genéricos)

**REVISOR**: también llamado REVIEWER. Es el analista que hace la revisión sobre lo implementado por un Autor y quien cumple funciones como:

- Velar por una apropiada implementación de artefactos en el proyecto BDD de automatización
- Controlar que las buenas prácticas y acuerdos definidos para el proyecto BDD de automatización estén incorporadas en los artefactos desarrollados por el Autor.
- Hacer retroalimentación a los autores sobre temas que tienen oportunidad de mejora o que deben ser ajustados para mejorar la implementación
- Hacer acuerdos con los autores para llegar a la mejor forma de implementar un determinado artefacto
- Generar tareas en el repositorio del proyecto BDD para indicar un ajuste relevante y que la implementación de éste: pueda ser postergado durante un corto tiempo sin impactar la funcionalidad automatizada (Ejemplo: tarea para realizar una mejora)

**RAMA del proyecto BDD**: es el espacio de trabajo para que un Autor pueda hacer ( *de forma independiente* ) cambios y/o implementaciones sobre el proyecto sin impactar de forma negativa la estabilización de las pruebas previamente automatizadas

**Pull request** (pullR): es la solicitud que hace un Autor, con los cambios o implementaciones generados sobre el proyecto BDD de automatización (rama), para ser aprobados e incorporados de forma permanente a dicho proyecto.

**Commit**: es el almacenamiento de los cambios que paulatinamente se implementan en una rama que se tiene del proyecto BDD de automatización. Dicho almacenamiento debe ir acompañado de una descripción que describa de forma breve (pero concisa): los cambios efectuados en la rama.

> **NOTA**: se recomienda que cada descripción, en cada almacenamiento; cuente con:
- Un título que de forma corta enuncie el cambio
- Una descripción que amplíe lo enunciado en el título.
- y entre el título y la descripción: adicionar una línea en blanco para que actúe como separador de los textos y permita una mejor legibilidad del commit

> Nota **adicional**: un commit se guarda de forma local hasta NO sincronizar dicho commit con el repositorio remoto

**Comentario a pullR**: es la anotación que hace un revisor frente a algún detalle observado en los archivos contenidos en el pullR y con la intención de retroalimentar al Autor. Por lo general estos comentarios pueden ser:

- Una sugerencia o recomendación que permita mejorar la implementación seleccionada por el Autor.
- El ajuste requerido para que la implementación realizada por el Autor incorpore buenas prácticas.
- Plasmar, *eventualmente*, inquietudes que permitan al Autor hacer un análisis que concluya con alguna de las anteriores acciones (mejora o ajuste)
- Incluso, un comentario puede servir para felicitar al autor por una implementación favorable para el proyecto y por consiguiente para otros posibles Autores.


**Lo siguiente está pendiente por revisar por gobierno de Calidad** ![arrow.png](https://bitbucket.org/repo/akpjXAz/images/625815946-arrow.png)

## ESQUEMA DE PULL REQUEST PARA REVISOR NUEVO ##

A continuación se describe el proceso que debe seguir un revisor nuevo que ha ingresado al equipo de reviewer:


### NIVEL INICIAL ###

Esta etapa aplica para cualquier revisor nuevo.


 Responsable    | Acción                               | A tener en cuenta
:--------------:|:-------------------------------------|:------------------------------
Cada persona que intervenga en el proceso.|Lectura de los acuerdos que debe cumplir el proyecto (wiki)|**Indispensable**: que sada personas tenga claridad sobre los acuerdos de buenas prácticas que tiene el proyecto.
Autor|Generar e informar sobre pull request (pullR)|Incluir en el pullR a todos los revisores nuevos, y a algún revisor padrino (o a todos).
Revisor nuevo|Tomar pullR para su revisión|
Revisor nuevo|Gestionar la revisión conjunta con un revisor padrino.|
Revisor nuevo|Hacer revisión del pullR. **Indispensable** que dicha revisión sea **de forma conjunta** con un revisor padrino|*Importante**: La revisión completa debe ser principalmente del revisor nuevo, y el revisor padrino realiza un **acompañamiento y asesoria** para que el revisor nuevo adquiera (con el transcurso de las revisiones): una adquisición idóneo de conocomiento.
Revisor nuevo|Durante la revisión: Generar y solucionar dudas con el revisor padrino|
|Prestar mucha atención a la revisión de forma conjunta.|
Revisor padrino|Durante la revisión: Identificar en el revisor nuevo: oportunidades de mejora, refuerzo y/o de estudio|(*)
Revisor nuevo, Revisor padrino|En caso de presentarse: se deben generar **de forma conjunta** los comentarios que dé lugar la revisión.|
Revisor padrino|Cuando el pullR esté correcto para hacer merge: **solo el revisor padrino** debe realizar la aprobación y merge del pullR.|
Revisor padrino|(*) Finalmente: se debe retroalimentar al revisor nuevo sobre lo detectado durante la revisión|

> El revisor Padrino debe identificar, revisión a revisión: sí el revisor nuevo presenta una absorción de conocomiento idóneo para que realice revisiones de pullR sin un acompañamiento de un revisor Padrino (pero sí con el acompañamiento de otro revisor nuevo).

> En las reuniones de revisores se debe tener un breve espacio para que los revisores padrinos indiquen los revisores nuevos que pueden avanzar hasta el nivel intermedio.

### NIVEL INTERMEDIO ###

Esta etapa aplica **únicamente** cuando el revisor nuevo ya ha sido acompañado por un revisor padrino por lo menos en: **5 oportunidades** (es decir: a lo largo de 5 diferentes pullR)


 Responsable    | Acción                               | A tener en cuenta
:--------------:|:-------------------------------------|:------------------------------
Cada persona que intervenga en el proceso.|Lectura de los acuerdos que debe cumplir el proyecto (wiki)|**Indispensable**: que sada personas tenga claridad sobre los acuerdos de buenas prácticas que tiene el proyecto.
Autor|Generar e informar sobre pull request (pullR)|Incluir en el pullR a todos los revisores nuevos, y a algún revisor padrino (o a todos).
Revisor nuevo|Tomar pullR para su revisión|
Revisor nuevo|Gestionar la revisión conjunta con **otro revisor nuevo**.| *Nota*: En caso que no exista un segundo revisor nuevo; entonces la revisión la debe realizar solo el revisor nuevo.
Revisor(es) nuevo(s)|Hacer revisión del pullR de forma conjunta.| 
Revisor(es) nuevo(s)|Durante la revisión: En caso que se surgan dudas se deben listar para solucionarlas con un revisor padrino **antes de aprobar o hacer merge** del pullR.|(*)
|Hacer una revisión detallada del pullR.|
Revisor(es) nuevo(s)|En caso de presentarse: se deben generar **de forma conjunta** los comentarios que dé lugar la revisión.|
Revisor(es) nuevo(s)|(*) Antes de aprobar el pullR: se **deben solucionar**, con un revisor padrino; las **dudas listadas previamente** y obtener una especie de retroalimentación.|
Revisor(es) nuevo(s)|(*) Hacer una revisión puntual del pullR con base a las dudas listadas prevamente y que ya fueron soluciónadas.|
Revisor nuevo|Cuando el pullR esté correcto para hacer merge: el revisor nuevo, **que inicialmente tomó el pullR**; realiza la aprobación y merge del pullR.|

> El revisor Padrino que ha hecho acompañamiento al revisor nuevo, debe realizar una auditoria aleatoria sobre uno o varios pullR previamente aprobados y con merge de un revisor nuevo. Y con base a estas auditorias, el revisor Padrino debe identificar: sí el revisor nuevo presenta idoneidad para que realice **de forma independiente**; la revisión, aprobación y merge del pullR (sin ningún acompañamiento).

> En las reuniones de revisores se debe tener un breve espacio para que los revisores padrinos indiquen los revisores nuevos que pueden avanzar hasta el nivel independiente.

### NIVEL INDEPENDIENTE ###

Esta etapa aplica **únicamente** cuando el revisor padrino previamente ha comunicado a los equipos de commiter y reviewer que un determinado revisor nuevo se le ha permitido aprobar y hacer merge de pullR.

Nota:
Se usará el término de "revisor nuevo aprobador" para indicar que se trata de un revisor nuevo que ha sido promovido a revisor con permiso para aprobar y hacer merge de pullR.

 Responsable    | Acción                               | A tener en cuenta
:--------------:|:-------------------------------------|:------------------------------
Cada persona que intervenga en el proceso.|Lectura de los acuerdos que debe cumplir el proyecto (wiki)|**Indispensable**: que sada personas tenga claridad sobre los acuerdos de buenas prácticas que tiene el proyecto.
Autor|Generar e informar sobre pull request (pullR)|Incluir en el pullR a todos los revisores nuevos, y a algún revisor padrino (o a todos).
Revisor nuevo aprobador|Tomar pullR para su revisión|
Revisor nuevo aprobador|Hacer revisión del pullR de forma independiente.| **Importante** que se solucione cualquier duda que se pueda presentar (por mínima que parezca).
|Hacer una revisión detallada del pullR.|
Revisor nuevo aprobador|En caso de presentarse: se deben generar los comentarios que dé lugar la revisión.|
Revisor nuevo aprobador|Cuando el pullR esté correcto se hace aprobación y merge.|

> Los revisores Padrino deben realizar una auditoria aleatoria sobre uno o varios pullR previamente aprobados y con merge de un revisor nuevo aprobador.







**Lo siguiente está pendiente por re-ajustar y revisar por gobierno de Calidad** ![arrow.png](https://bitbucket.org/repo/akpjXAz/images/625815946-arrow.png)

## ESQUEMA DE PULL REQUEST ##

A continuación se describe brevemente el flujo del proceso asociado a un pullR del proyecto BDD de automatización:

# Paso| Responsable    | Acción                               | A tener en cuenta
:----:|:--------------:|:-------------------------------------|:------------------------------
0.    |Autor, Revisor, Revisor nuevo | Leer y entender los acuerdos del proyecto| __Indispensable__ que cada analista, previo a iniciar cualquier proceso de automatización o de revisión de pullR: tenga claridad sobre los acuerdos para la automatización de pruebas automatizadas (wiki)
1.    |Autor           | Generar una rama del proyecto         | De forma periódica se debe __actualizar la rama__ con las últimas cambios aprobados e incorporados a la rama principal del proyecto (Ejemplo: develop)
2.    |Autor           | Generar cambios y/o implementaciones  | Durante la implementación debe incorporar buenas prácticas de implementación y así cumplir con los acuerdos definidos.
      |Autor           |                                      | Durante la implementación se debe hacer el __guardado periódico__ de los cambios implementados (commit). Y para mayor seguridad: sincronizar el commit con el repositorio remoto.
3.    |Autor           | Verificar que la ejecución es correcta mediante uso de comandos | Garantizar que lo implementado se genera (compila) de forma correcta
4.    |Autor           | Solicitar / actualizar pullR         | Antes de la solicitud, se debe **verificar que los acuerdos y buenas prácticas están incorporadas** en los artefactos cambiados y/o implementados
5.    |Autor           | Informar al equipo, sobre la **creación** del pullR solicitado        | Comunicar de forma __oportuna__ tanto al equipo de autores como a los revisores; sobre la creación/solicitud de pullR (*de esta forma se mejora la agilidad y oportunidad en la toma y revisión de un pullR*).
6.    |Revisor         | Tomar pullR para su revisión         | __Ideal__ que se tomen los pullR de mayor impacto para cumplir con una determinada automatización (de lo contrario: se debe tomar en el orden de generación del pullR)
7.    |Revisor         | Informar al equipo, sobre la **toma** del pullR | Comunicar de forma oportuna tanto al equipo de revisores como al autor del pullR; sobre la toma de un determinado pullR (*de esta forma se mejora la comunicación y  evita la revisión de un mismo pullR por parte de dos diferentes revisores*).
8.    |Revisor         | Hacer revisión del pullR | Tener en cuenta los acuerdos para la automatización de pruebas automatizadas.
__8A.__   |Revisor nuevo   | Hacer revisión del pullR en conjunto con revisor padrino | Tener en cuenta los acuerdos para la automatización de pruebas automatizadas.
__8A.__   |Revisor nuevo   |                                                          | Aprovechar al máximo este espacio para solucionar dudas o aclaraciones frente a criterios o buenas prácticas desconocidas
9.    |Revisor         | Adicionar comentarios al pullR según los acuerdos y buenas prácticas | Los comentarios deben ser dicientes y entendibles tanto para el Autor como para cualquier revisor que pueda apoyar el proceso de revisión
9.    |Revisor         |                                      |Asignar tareas al Autor en caso que deba realizar: investigaciones, refuerzos, análisis y/o mejoras a corto plazo (y __que no sean impactantes ni indispensables para la automatización en curso__ )
9.    |Revisor         |                                      |En caso de crear tareas al Autor, originadas por un comentario: se debe __adicionar__ , en el comentario; __el número de la tarea creada__ al Autor (precedido por el signo "#")
__9A.__   |Revisor nuevo   | Adicionar comentarios al pullR según los acuerdos y buenas prácticas |
__9A.__   |Revisor nuevo   |                                      |Asignar tareas al Autor en caso que deba realizar: investigaciones, refuerzos, análisis y/o mejoras a corto plazo (y __que no sean impactantes ni indispensables para la automatización en curso__ )
__9A.__   |Revisor nuevo   |                                      |__Importante__ : La asignación de tareas al Autor debe ser en coordinación con el revisor padrino
10.   |Revisor         | Notificar sobre la finalización de la revisión|Comunicar de forma __oportuna__ la finalización de la revisión del pullR tanto al equipo como al autor.
10.   |Revisor         |                                       |En caso que se haya presentado comentarios: mencionar éste hecho en la notificación
11.   |Autor           | Revisar y solucionar comentarios      |En caso que se haya presentado comentarios: el Autor debe incorporar ajustes o implementaciones según los comentarios adicionados al pullR
12.   |Autor           |                                       |El autor debe devolverse, en el proceso; __al paso #: 2__
12.   |Revisor         |                                       |El revisor debe devolverse, en el proceso; __al paso #: 6__ ( *aplica igual para un revisor nuevo* )
13.   |Revisor         | Aprobar e incorporar pullR a la rama principal | En caso que __NO__ se tengan comentarios pendientes por ajuste: se aprueba y se incorpora pullR a rama principal
__13A.__  |Revisor nuevo   | NO aprobar ni incorporar pullR a la rama principal | __Importante__ : Un revisor nuevo __NO__ debe aprobar ningún pullR
14.   |Revisor         | Notificar sobre la aprobación del pullR |Comunicar de forma __oportuna__ la aprobación del pullR tanto al equipo como al autor.
15.   |Autor           | Solucionar tareas asignadas             |En la tarea adicionar (como comentario): las actividades realizadas y que sustentan el cierre de la tarea *por parte del Revisor*
16.   |Autor           | Notificar sobre la finalización de tarea asignada |Comunicar éste hecho, de forma __oportuna__; al revisor que generó la tarea
17.   |Revisor         | Hacer revisión de solución de tarea pendiente del Autor | Tener en cuenta los comentarios adicionados por el Autor y __corroborar que las actividades__ realizadas __cumplan__ con el objetivo de la tarea asignada
18.   |Revisor         | Cerrar tarea pendiente del Autor |


> **IMPORTANTE**:
>- Los pasos que están acompañados de la letra *A*: son los pasos que aplican únicamente para un revisor nuevo y cuya cantidad de acompañamientos de un revisor padrino deben ser de mínimo 3 (o una cantidad definida y coordinada entre analistas y/o equipo de revisores)

>- Luego que un revisor nuevo finaliza revisiones en compañía de un revisor padrino, este último le indicará al revisor nuevo que puede proceder a revisar pullR de forma individual; **pero sin realizar ninguna aprobación**.

>- Cuando se acuerde que un Revisor nuevo puede realizar revisión de pullR de forma individual, el revisor padrino deberá hacer una segunda revisión del pullR y hacer retroalimentación al Revisor nuevo (y al Autor en caso de ser necesario). Y en esta instancia, será **aún** el Revisor padrino quien realice la acción de aprobar el pullR (ver paso # 13)

>- En las reuniones de revisores: los revisores padrino deben indicar cuales revisores nuevos pueden pasar a ser revisores sin acompañamiento (revisión individual, pero sin aprobación de pullR) y cuales pueden realizar aprobación de pullR (sin necesidad de la segunda revisión de un revisor padrino)

>- A excepción de un revisor nuevo: Recuerde que la aprobación de **un pullR puede ser realizada por cualquier revisor**, incluso sí no es el revisor que tomó el pullR por primera vez. *Por esto es relevante que los comentarios sean entendibles por cualquier analista*

[Volver al menu](politicasAutomatizacionProyectoBDD)