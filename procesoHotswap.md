# Proceso Hotswap

[Volver al home](Home)

## Hotswap

El término cambio en caliente (hot swap en inglés), o sustitución en caliente, hace referencia a la capacidad de los modulos de la suite de seguros de Guidewire usados en el proyecto Core de Seguros de Suramericana para realizar cambios en funcionalidades sin necesidad de detener o alterar la operación normal del sistema.

Los hotswap en el proyecto se pueden aplicar para código gosu, esto quiere decir que lo podemos aplicar a ***clases, rules y enhancements***, ***NO se pueden aplicar*** para lo que no sea código gosu, por ejemplo ***entidades, xmls, displays keys***.

## Descripción del proceso

Cuando un equipo necesite realizar un Hotswap en los ambientes productivos de la suite de seguros de Guidewire, necesitará seguir los siguientes pasos:

1. Informar a los lideres y facilitadores del proyecto sobre la necesidad de crear el hotswap.
2. Crear un hotfix con el código que requiere incluir como hotswap.
3. Realizar un Pull Request del hotfix creado apuntando a ***master*** (Los revisores de este cambio serán los integrantes del equipo de arquitectura del proyecto).
4. Se debe GARANTIZAR que se despliega el hotfix al otro día
5. Esperar la revisión del Pull Request y realizar las modificaciones que sean recomendadas por el revisor.
6. Una vez sea aprobado el PR, debe dirigirse al puesto del revisor para realizar el Hotswap en conjunto con él.
7. Crear una incidencia en **Jira** de tipo *Hotswap* donde se especifique el cambio realizado en el Hotswap, esta incidencia permitirá gestionar la creación de pruebas unitarias y el refactor del código en caso de que sea necesario.

[Volver al home](Home)