**CONSIDERACIONES A TENER EN CUENTA PARA REPORTAR UN BUG**

[Volver al menu](PolíticasReporteBugs)

**Todo error que se presente durante una prueba debe ser reportado.**

**Las sugerencias/consideraciones se deben reportar como mejoras.**

**Los bugs se deben asignar al facilitador y él es el responsable de asignarlos a quien corresponda.**

**Al momento de realizar el reporte del bug y/o sugerencia, tener en cuenta:**
 
**El resumen debe ser claro:** ser lo más diciente posible en el resumen del bug, con el fin de que quien lo lea, comprenda rápidamente o por lo menos se haga una idea de qué se trata.

**Ser específico:** la descripción del error debe ser precisa, no adicionar comentarios de suposiciones sobre lo que está sucediendo puesto que puede llevar a que el desarrollador tome dichos comentarios en cuenta y pierda tiempo en análisis innecesarios.

**Tener claro el patrón que genera el error:** si tenemos claro el patrón que genera el error podemos ayudar a que el diagnóstico del desarrollador sea más rápido dado que podría identificar con mayor facilidad el bloque de código en donde se presenta el error. Adicionalmente, tener el patrón que genera el error puede clarificar los pasos a seguir para replicar el mismo.
 
**Detalle de los pasos realizados para generar el error:** no se debe obviar ningún paso que sea relevante para llegar al punto del fallo.