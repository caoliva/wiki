**POLÍTICAS PARA EL REPORTE DE BUGS EN EL PROYECTO CORE DE SURAMERICANA**

![ReporteBugsPpal.png](https://bitbucket.org/repo/akpjXAz/images/852104613-ReporteBugsPpal.png)


1.[Consideraciones al momento de reportar un bug](PolíticasReporteBugs_Consideraciones)

2.[Reporte en Jira y proceso](PolíticasReporteBugs_JiraProceso)

3.[Toma de evidencias]