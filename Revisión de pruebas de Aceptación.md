#Ajuste temporal en los pipelines del Core

[Volver al home](Home)

Para los pipelines que cuentan con pruebas de aceptación, tendrán una nueva forma de revisar cuando las Pruebas de Aceptación fallen, deberán revisar el log de cada uno de los stages que corren pruebas de aceptación para determinar si superó el umbral o no, como se indica en las siguientes imágenes. 

Este esquema se implementa ya que el plugin de jenkins que revisa estas pruebas teniendo en cuenta los umbrales, no funciona correctamente en pipeline as code y en el roadmap de mediano plazo del plugin no se tiene el ajuste, por lo tanto trabajaremos para tener una versión propia del plugin, pero mientras tanto necesitamos de su ayuda siguiendo los pasos indicados. 

Un Pipeline con algún lote de aceptación que no cumpla el umbral quedaría de la siguiente forma: 

![pipeline-aceptance-1](https://bitbucket.org/repo/akpjXAz/images/176865531-aceptance0.png)

El nuevo stage CheckAceptanceTest indicaría que alguna prueba falló, pero para identificar cual falló, debemos buscar en los logs de cada stage el siguiente mensaje: 

![aceptacion1.png](https://bitbucket.org/repo/akpjXAz/images/395726895-aceptacion1.png)

Un stage que no falló el umbral tendrá el siguiente mensaje, tener cuidado que aunque dice que marca el build como failure (marcado en Rojo) no significa que el stage superó el umbral:

![aceptacion2.png](https://bitbucket.org/repo/akpjXAz/images/2317340936-aceptacion2.png)

[Volver al home](Home)