# Instalación de TSLint para Visual Studio Code #

[Volver al home](Home)

* Abrir Visual Studio Code

![VSC1.png](https://bitbucket.org/repo/akpjXAz/images/2825808733-VSC1.png)

* Ingresar a la sección de Extensiones

![VSC2.png](https://bitbucket.org/repo/akpjXAz/images/2751694652-VSC2.png)

* En el campo de búsqueda ingresar "TSLint", dar click en el botón verde "Install" y luego reiniciar el VSC

![VSC3.png](https://bitbucket.org/repo/akpjXAz/images/1755760236-VSC3.png)

* Ingresar a la sección "Settings"

![VSC4.png](https://bitbucket.org/repo/akpjXAz/images/2080230596-VSC4.png)

* Cambiar la vista para JSON

![VSC5.png](https://bitbucket.org/repo/akpjXAz/images/1502694149-VSC5.png)

* En "USER SETTINGS" ingresar lo siguiente:


```
#!json
{
    "tslint.rulesDirectory": "node_modules/codelyzer",
    "typescript.tsdk": "node_modules/typescript/lib",
}

```

![VSC6.png](https://bitbucket.org/repo/akpjXAz/images/2141525625-VSC6.png)

* Reiniciar el VSC

* Al revisar cualquier artefacto, en donde falte algo de acuerdo a las reglas de análisis estático de código, se debe visualizar el error de compilación y en el tooltip debe aparecer la referencia de tslint para el error particular.

![VSC7.png](https://bitbucket.org/repo/akpjXAz/images/3864152703-VSC7.png)


[Volver al home](Home)