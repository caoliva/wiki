# ACUERDOS PARA EL DESARROLLO DE PIPELINE COMO CÓDIGO EN EL CORE DE SURAMERICANA

[Volver al home](Home)

![DevOps_QA.jpg](https://bitbucket.org/repo/akpjXAz/images/4245596898-DevOps_QA.jpg)

Ítems sobre: | Última actualización (DMA)
:------------------------ |:-------------------------------------:
1. [Estructura de proyecto](politicasDesarrolloPipeLineAsCode_Estructura)|Para revisar como equipo ![arrow.png](https://bitbucket.org/repo/akpjXAz/images/625815946-arrow.png)
2. [Consideraciones PullRequest](politicasDesarrolloPipeLineAsCode_pullR)|*Para diligenciar entre todos*
3. [Nombramientos](politicasDesarrolloPipeLineAsCode_Nombramientos)|__14/05/2019__  ![arrow.png](https://bitbucket.org/repo/akpjXAz/images/625815946-arrow.png)
4. [Formato](politicasDesarrolloPipeLineAsCode_Formato)|
5. [Esquema de trabajo para Pull Request](politicasDesarrolloPipeLineAsCode_EsquemaRevisores)|
6. [Tip - Probar función desarrollada](politicasDesarrolloPipeLineAsCode_TipEjecutarSoloEscenarioEspecifico)|