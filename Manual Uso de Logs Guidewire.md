Para el uso de logs en Guidewire existen varias clases que se pueden usar, en el presente manual explicaremos las principales y su modo de uso.

### Clases ###

1. *gw.api.system.PLLoggerCategory*: Ofrece instancias estáticas de clases para Logger predefinidos por la plataforma, ejemplo:

- *PLLoggerCategory.PLUGIN*: Logger usado en los Plugins de Guidewire.
- *PLLoggerCategory.API*: Logger usado en los diferentes API.
- *PLLoggerCategory.SERVER_BATCHPROCES*S: Logger usado en los procesos batch.

2. *sura.suite.gw.log.util.LoggerFactory*: Ofrece las funcionalidades para el uso de Logger, ejemplo:

- *LoggerFactory.GwCoreLogger*: Logger de uso General de Core Seguros, este logger es el oficial. Los demás están deprecados e internamente se comportan como "*GwCoreLogger*"
 

### Creación de nuevos Loggers ###

IMPORTANTE:Antes de crear un nuevo logger es importante validar si ya existe alguno que satisfaga la necesidad específica.

Los pasos para la creación de un nuevo loggers son:
1. En la clase se instancia el logger:

*private static final var _logger = LoggerFactory.GwCoreLogger*

2. En el archivo logging.properties definir la configuración de la nueva categoría. La categoría puede ser un paquete o una clase.

Para un paquete 

- log4j.category.gw=INFO

Para una Clase

- log4j.category.sura.suite.utility.webservice.SuraPayloadTransform=ERROR

Si se desea redirigir el Log a un archivo individual  sería:

*log4j.category.sura.suite.acc.gmc.integracion.path.traffic.PathTrafficWs=INFO, sura.suite.acc.gmc.integracion.path.traffic.PathTrafficWs
log4j.additivity.sura.suite.acc.gmc.integracion.path.traffic.PathTrafficWs=false
log4j.appender.sura.suite.acc.gmc.integracion.path.traffic.PathTrafficWs=org.apache.log4j.RollingFileAppender
log4j.appender.sura.suite.acc.gmc.integracion.path.traffic.PathTrafficWs.encoding=UTF-8
log4j.appender.sura.suite.acc.gmc.integracion.path.traffic.PathTrafficWs.File=${guidewire.logDirectory}/PathTraffic.log
log4j.appender.sura.suite.acc.gmc.integracion.path.traffic.PathTrafficWs.layout=org.apache.log4j.PatternLayout
log4j.appender.sura.suite.acc.gmc.integracion.path.traffic.PathTrafficWs.layout.ConversionPattern=%-10.10X{server} %-8.24X{userID} %d{ISO8601} %m%n
*


## 2. Registro de Log ##

2.1. Niveles de Log

**DEBUG(Prioridad 1)**: Mensajes con información que permite analizar el MAL funcionamiento del sistema, debe contener información adicional detallada, ejemplo:

- "Valor calculado de tarifa:10000 para el vehículo XYZ de la póliza 9000001"
- "Ahora se está procesando el registro ABC: 123456".

**INFO(Prioridad 2)**: Mensajes con información que permite analizar el BUEN funcionamiento del sistema, debe contener poca a información adicional, ejemplo: 
- "El componente XYZ comenzó".
- "Un usuario ha iniciado sesión en ClaimCenter".

**WARN(Prioridad 3)**: Mensajes que indican un problema potencial, debe contener poca información adicional, ejemplo:
- "Las reglas de asignación finalizaron pero la actividad no quedó asignada a un usuario".
- "La reclamación se creó pero asociada a una póliza no verificada".
- "La llamada del plugin tomó más de 90 segundos".

**ERROR(Prioridad 4)**: Mensajes que indican un problema definido, debe contener información adicional detallada, ejemplo: 
- "Problemas de conexión con un sistema externo XYZ : Usuario ABC no válido para consumir el servicio".
- "El proceso XYZ finalizó con los siguiente errores: -No se encontró contrato de reaseguro en la póliza 900001 al objeto asegurable X para el inicio de vigencia 1900/01/01".


2.2. El uso de los niveles de log varían según la necesidad y es responsabilidad del desarrollador su buen uso. consideraciones en la definición del nivel.:

- El nivel ERROR se debe utilizar en los bloques try/catch al capturar la excepción, siempre y cuando dentro del bloque catch SI se lance una nueva excepción, se debe incluir detalle de parámetros o salida y el error presentado. Un error es un problema que está ocurriendo en el sistema e impide su normal funcionamiento y el cual se convertirá en un ticket o caso para la mesa de soporte.

- El nivel WARN se debe utilizar en los bloques try/catch al capturar la excepción, siempre y cuando dentro del bloque catch NO se lance una nueva excepción o como el registro de una ruta alternativa ante el fallo de una validación de negocio. Un warn es un problema que está ocurriendo en el sistema pero se tiene una ruta alternativa para continuar con el proceso o se puede reintentar más tarde sin afectar la operación.

- Los niveles INFO se deben utilizar para el seguimiento de un flujo de proceso. deben incluir poco detalle o ninguno.

- El nivel DEBUG se deben utilizar para el seguimiento de un flujo de proceso con el fín de detectar un problema en la aplicación, debe incluir detalle de parámetros o salida.

- Se debe tener presente en todo momento que la información escrita en los logs ser lo suficientemente detallada para que las personas responsables de su monitoreo o soporte pueden solucionar el problema o replicar o escalar al responsable. Entre más alto la prioridad del nivel, mayor información adicional debe contener.

## 3. Información General ##

3.1 La nivel por defecto es:

- Para ambientes distintos a UAT y PDN

log4j.category.gw=**DEBUG**
log4j.category.sura=**DEBUG**
log4j.category.sura.suite.utility.webservice.SuraPayloadTransform=ERROR

- Para ambientes UAT y PDN

log4j.category.gw=**INFO**
log4j.category.sura=**INFO**
log4j.category.sura.suite.utility.webservice.SuraPayloadTransform=ERROR

3.2 El formato por defecto para todos los ambientes:

log4j.appender.layout.ConversionPattern=%-10.10X{server} %-8.24X{userID} %d{ISO8601} %c|PRI=%p%m%n|