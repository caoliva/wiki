# ESTRUCTURA DEL PROYECTO #

[Volver al menu](politicasDesarrolloPipeLineAsCode)

- **IMPORTANTE**! Por favor NO alterar la arquitectura (directorios, etc) definida para el proyecto. Si hay alguna sugerencia para cambiar o modificar dicha estructura: por favor comentarla al equipo para analizar el impacto en el proyecto.


## ESTRUCTURA DE DIRECTORIOS ##
La distribución de directorios se presenta a continuación:

- A nivel **general** se presentan a continuación la estructura de directorios que están incluidos tanto en: "**src**" como en "**test**":

Carpetas contraidas|Carpetas expandidas
:----:|:---------:
![estructura_proyecto_01.PNG](https://bitbucket.org/repo/akpjXAz/images/3827644898-estructura_proyecto_01.PNG)|![estructura_proyecto_02.PNG](https://bitbucket.org/repo/akpjXAz/images/2370663773-estructura_proyecto_02.PNG)


## ESTRUCTURA /src/ ##

Nombre carpeta| Archivos que se almacenan
:------------:|:-------------------------------------
<*paquete_específico*>|Contiene los archivos que incluyen funcionalidades dedicadas a un determinado fin. Por ejemplo: conexión a un cliente http
**util**|Contiene los archivos que incluyen funcionalidades genéricas y que pueden ser usadas en cualquier punto del proyecto
**validations**|Contiene los archivos que incluyen funcionalidades que validan un determinado valor (ejemplo: un rango de fechas) y que pueden ser usadas en cualquier punto del proyecto


## ESTRUCTURA /test/ ##

Nombre carpeta| Archivos que se almacenan
:------------:|:-------------------------------------
<*paquete_específico*>|Contiene las pruebas unitarias asociadas a las funcionalidades dedicadas a un fin determinado
**util**|Contiene las pruebas unitarias asociadas a las funcionalidades genéricas de utilerías 
**validations**|Contiene las pruebas unitarias asociadas a las funcionalidades que realizan algún tipo de validación




[Volver al menu](politicasDesarrolloPipeLineAsCode)