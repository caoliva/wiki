# NOMBRAMIENTOS #

[Volver al menu](politicasDesarrolloPipeLineAsCode)

## SUFIJOS ##
A continuación se presentan los sufijos que deben ser usados al momento de nombrar alguno de los siguientes artefactos:

Artefacto | Sufijo |Ejemplo correcto                     | Ejemplo incorrecto            
:----------|:-----:|:-------------------------------------|:------------------------------
Interface|Interface||
Servicio |Service||
Implementación |Imp||
Enumeración |Enum||
Archivo de propiedades|Properties||


### A continuación se presentan las consideraciones a tener en cuenta al momento de nombrar un determinado artefacto. ###


## PAQUETES ##

Característica  | Ejemplo correcto                     | Ejemplo incorrecto            
:---------------|:-------------------------------------|:------------------------------
Idioma: **Inglés**|validations|validaciones
Formato: **minúscula** sostenida |httpclient|httpClient
Formato: sin espacios ni caracteres separadores |httpclient|http_client
|httpclient|http client


## CLASES ##

Característica  | Ejemplo correcto                     | Ejemplo incorrecto            
:---------------|:-------------------------------------|:------------------------------
Idioma: **Inglés**|JenkinsHttpClient|ClienteHttpJenkins
Formato: UpperCamelCase||
Formato: **sin separadores** ni espacios en blanco||
Iniciar por un sustantivo||
Nombramiento en **singular**||
No incluir verbos||
Longitud del nombre: **20 caracteres**||
**Descriptivo y Diciente**: Indicar de forma natural la función que cumple el método, facilitando el uso o reuso por cualquier persona nueva en el proyecto||
**Nemotécnico**: De fácil recordación (y reutilización)||


## ATRIBUTOS / VARIABLES / PARÁMETROS DE MÉTODOS ##

Característica  | Ejemplo correcto                     | Ejemplo incorrecto            
:---------------|:-------------------------------------|:------------------------------
Idioma: **Inglés**|userAgent|UsuarioAgente
Formato: lowerCamelCase||
Formato: **sin separadores** ni espacios en blanco||
No incluir verbos||
Evitar abreviaturas que dificulten el entendimiento común y natural del valor que se almacena en el atributo||
**Descriptivo y Diciente**: Indicar de forma natural el valor que se almacena en el atributo, facilitando el uso o reuso por cualquier persona nueva en el proyecto||
**Nemotécnico**: De fácil recordación (y reutilización)||


## MÉTODOS ##

Característica | Ejemplo correcto                     | Ejemplo incorrecto            
:---------------- |:-------------------------------------|:------------------------------
Idioma: **Inglés**|createChangeOrder|creacionOrdenDeCambio
Formato: lowerCamelCase|buildParallelStage|BuildParallelStage
**Iniciar por verbo** en infinitivo|buildParallelStage|parallelStagebuild
Formato: **sin separadores** ni espacios en blanco|buildParallelStage|buildParallel_Stage
**Descriptivo y Diciente**: Indicar de forma natural la función que cumple el método, facilitando el uso o reuso por cualquier persona nueva en el proyecto||
**Nemotécnico**: De fácil recordación (y reutilización)||


> **Notas importantes**:

>- Cuando se trata de un **método genérico**: evitar palabras que indiquen especificidad sobre algún: modelo, área, frente de trabajo o similar.


### CONSTANTES Y ATRIBUTOS ENUM ###

Característica | Ejemplo correcto                     | Ejemplo incorrecto            
:------------------------|:-------------------------------------|:------------------------------
Idioma: **Inglés**|PATH_REPORT_ACEPTANCE_TEST|RUTA_REPORTE_PRUEBAS_ACEPTACION
Formato: Mayúscula sostenida|PATH_REPORT_ACEPTANCE_TEST|path_report_aceptance_test
Formato: usar guión bajo como separador de palabras|PATH_REPORT_ACEPTANCE_TEST|PATHREPORT_ACEPTANCETEST
Formato: No usar caracteres especiales NI caracteres que generen deuda técnica||
**Descriptivo y Diciente**: Indicar de forma natural el valor que se almacena en la constante, facilitando el uso o reuso por cualquier persona nueva en el proyecto||
**Nemotécnico**: De fácil recordación (y reutilización)||


### ARCHIVOS DE PROPIEDADES EN UNA APLICACIÓN ###

Característica  | Ejemplo correcto                     | Ejemplo incorrecto            
:---------------|:-------------------------------------|:------------------------------
Idioma: **Inglés**|jenkinsfile|configuracionjenkins
Formato: **minúscula** sostenida |jenkinsfile|jenkinsFile
Formato: sin espacios ni caracteres separadores |jenkinsfile|jenkins_file
Usar el sufijo adecuado|jenkinsproperties|jenkinsconfig

### PARÁMETROS DENTRO DE ARCHIVOS DE PROPIEDADES EN UNA APLICACIÓN ###

Característica  | Ejemplo correcto                     | Ejemplo incorrecto            
:---------------|:-------------------------------------|:------------------------------
**Pendiente**||


[Volver al menu](politicasDesarrolloPipeLineAsCode)