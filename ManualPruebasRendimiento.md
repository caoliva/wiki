##**MANUAL PRUEBAS DE RENDIMIENTO**

[TOC]

###1. Prerrequisitos

• Tener instalado Java versión 6 o superior. 
• Tener la última versión de Jmeter, la cual se puede descargar de la página oficial de Apache Jmeter: http://jmeter.apache.org/
• En éste link se encuentra la guía de instalación de Jmeter: http://jmeter.apache.org/usermanual/get-started.html

###2. Objetivos

1. Capacitar a los equipos de desarrollo de Suramericana en el diseño de planes de prueba con Jmeter, con el fin de habilitar capacidades para hacer pruebas básicas de rendimiento de las aplicaciones que construyan

2. Capacitar a los equipos para ejecutar e interpretar los resultados que generan las pruebas de performance realizadas. 

3. Dotar a los equipos del conocimiento necesario para que construyan Jobs en Jenkins que les permitan ejecutar, de manera automática, los testplan construidos con Jmeter.

###3. Jmeter

Para abrir la interfaz gráfica de Jmeter, debe correrse el archivo Jmeter.bat en Windows o Jmeter.sh para Linux. Estos archivos se encuentran en el directorio bin. Una vez se ejecute, debe aparecer la GUI de Jmeter como se ve a continuación: 

![Jmeter1](images/1.png)

La interfaz presenta un espacio básico de trabajo constituido por un plan de pruebas que contendrá todos los elementos del test plan, una barra de herramientas de Jmeter y un banco de trabajo que permite un espacio para almacenar temporalmente elementos de configuración de una prueba como el servidor proxy http que es un elemento clave para grabar scripts para escenarios web. Cabe aclarar que los elementos que se sitúen en este banco de trabajo solo persisten mientras se tenga abierto Jmeter, es decir, no se guardan con el plan de prueba.
Para adicionar un elemento, basta con hacer clic derecho en el punto del test donde se requiera y de allí se despliegan las opciones disponibles.

###4. Configuración del Test plan

![Jmeter2](images/2.png)

El espacio básico de trabajo en Jmeter está constituido por un plan de pruebas que contiene un grupo de hilos, y éste a su vez contiene todos los elementos necesarios para configurar un test de los escenarios que se deseen probar.

**Grupo de hilos**

El grupo de hilos es el elemento de partida del test plan; allí se configuran los elementos básicos de un test como lo son controladores, muestreadores, timers, assertions y listeners para tener un test plan completo. Desde el grupo de hilos es posible configurar el número de hilos a ejecutar (usuarios concurrentes), el periodo de subida de los hilos (tiempo que tardara Jmeter para lanzar la máxima concurrencia configurada), el número de ejecuciones de cada escenario o el tiempo que tardará la prueba.

**Samplers**

Desde la versión 4,0 de Jmeter, se cuenta con gran variedad de samplers, incluyendo casi cualquier tipo de petición que pueda hacerse a un servidor de aplicaciones o a una base de datos: 

![Jmeter3](images/3.png)

**Controladores**

Los controladores permiten personalizar la lógica que Jmeter usa para enviar éstas peticiones (ejemplo: un controlador de orden aleatorio permite ejecutar peticiones de manera aleatoria). Con estos elementos pueden llevarse a cabo acciones como cambiar el orden de las peticiones, modificar peticiones según las respuestas, repetir peticiones entre otras:

![Jmeter4](images/4.png)

**Listeners**

Los listeners recolectan la información que provee el servidor de aplicaciones según los requests ejecutados durante la ejecución de un test plan. Estos permiten almacenar la información de manera local para poder accederla posterior a la prueba y procesarla.

![Jmeter5](images/5.png)

**Timers**

Jmeter posibilita implementar diferentes tipos de temporizadores con el fin de causar delays entre las peticiones, según la identificación del patrón de uso de las aplicaciones a probar o con el fin de no saturar el servidor: 

![Jmeter6](images/6.png)

**Assertions**

Este elemento, permite validar que la solución retorne efectivamente la respuesta esperada: Desde validar un texto en particular, el tamaño de la respuesta, la estructura de un XML, entre otras validaciones: 

![Jmeter7](images/7.png)

**Elementos de configuración** 

Los elementos de configuración, no lanzan peticiones desde el test  plan, pero permiten modificarlas: Gestores de cabeceras, gestores de cookies, configuradores de sets de datos, configuradores de conexiones a base de datos, son algunas de los elementos que existen en ésta categoría: 

![Jmeter8](images/8.png)

###5. Ejemplo de configuración de un Test plan con Web Service

1. Nuevo proyecto en Jmeter:

![Jmeter9](images/9.png)

2. Agregar grupo de hilos:

![Jmeter10](images/10.png)

3. Añadir  Sampler “Petición HTTP”:

![Jmeter11](images/11.png)

En éste caso se añade un sampler de tipo http request y se configura como se muestra a continuación: 

![Jmeter12](images/12.png)

Para el ejemplo en particular, ingresamos el nombre del servidor, la ruta, el puerto, el método y el cuerpo del mensaje.

4. Ingresar cabeceras:

![Jmeter13](images/13.png)

5. Extraer parámetros:

Para el ejemplo, el servicio a probar requiere un token de auntenticación para poder consumirlo, el cual se consigue realizando una petición al servicio de auntenticación de Seus para ser extraido como variable Jmeter del parámetro tokenMus contenido en la respuesta.

![Jmeter14](images/14.png)

El extractor de expresiones regulares requiere especificar el nombre de la variable en donde se guardará temporalmente el resultado de la extracción, la parte de la respuesta en donde se deberá buscar, la expresión regular válida que realice la búsqueda y extracción y el template (generalmente $1$). 

![Jmeter15](images/15.png)

6. Usar parámetros extraídos:

Una vez se tiene el extractor de expresiones regulares configurado, llamamos la variable en donde corresponda:

![Jmeter16](images/16.png)

7. Usar un set de datos:

En caso de requerir el uso dinámico de otros parámetros, cuyos valores se pueden tener  en un archivo plano de texto o como valores separados por comas, podemos hacer uso del elemento configuración del csv data set:

![Jmeter17](images/17.png)

![Jmeter18](images/18.png)

En éste caso se requiere que sean especificados la ruta del archivo (en caso de que el archivo se encuentre en el mismo directorio del archivo .jmx del test plan, basta con especificar el nombre del archivo), los nombres de las variables y el tipo de delimitador. Una vez más, para usarlos se debe hacer el correcto llamado desde donde se requiera, reemplazando las variables con la expresión ${nombrevariable}.

8. Cookie Manager: 

Cuando existen peticiones http cuya respuesta contienen cookies, éste elemento realizará un manejo automático de las cookies, en primer lugar las almacena y en segundo lugar, las envía, tal como lo haría un navegador web:

![Jmeter19](images/19.png)

![Jmeter20](images/20.png)

9. Agregar un listener:

Por último agregamos un listener a nuestra eleccion para visualizar la respuesta del servidor:

![Jmeter21](images/21.png)

10. Configurar grupo de hilos:

Una vez se tengan todos los elementos básicos para un test completamente funcional, se configurará el grupo de hilos según el diseño de prueba requerido:

###6. Ejemplo pruebas de rendimiento

**Ejemplo**. Prueba de rendimiento escalonada hasta 10 usuarios, con escalas de 1 usuario cada 60 segundos, hasta 20 minutos de prueba:

![Jmeter22](images/22.png)

Para éste ejemplo, se especifica el número de hilos que lanzará Jmeter y cuánto tiempo tomará en alcanzar la concurrencia máxima especificada y se indica además el tiempo de duración de la prueba que es de 20 minutos; En este caso Jmeter alcanzará 10 usuarios al llegar a los 10 minutos, lanzando un usuario cada minuto y luego de alcanzar ese número de usuarios, se mantendrá por 10 minutos más lanzando peticiones a través de esos 10 hilos (usuarios). 

###7. Ejemplo pruebas de carga

**Ejemplo**. Prueba de carga de 20 usuarios, con una duración de 10 minutos.

![Jmeter23](images/23.png)

Para este ejemplo, definimos la cantidad de hilos en 20 y el periodo de subida en 1 segundo, esto para que desde que inicia la prueba estén los 20 usuarios activos durante toda la prueba.


###8. Grabación y configuración de un Test plan con aplicaciones Web y Web Services

Jmeter permite hacer una captura del tráfico http a través de un elemento llamado servidor proxy http: éste elemento se vale del proxy local habilitado a través de las opciones de configuración de LAN para escuchar un puerto especifico y construir con la información que recolecta, los elementos de configuración que quedarán incluidos en el testplan. 

![Jmeter24](images/24.png)

El proxy http recorder de Jmeter deberá tener especificado el puerto por el cual deberá escuchar (debe ser un puerto libre de la máquina desde donde se ejecutará Jmeter). Adicionalmente, en el campo llamado controlador objetivo, podremos especificar en qué parte de la estructura del testplan quedarán las peticiones y si especificamos el modo de agrupación, Jmeter agrupará las peticiones por algún criterio (lo más usado es agrupar por transacción):

![Jmeter25](images/25.png)

Paso seguido, nos dirigimos a las opciones de internet (panel de control o navegador web o en Preferences en el caso de SoapUI) para habilitar de forma manual el proxy para que Jmeter capture todos los requests para el script. En éste caso se especifica el puerto 8080:

![Jmeter26](images/26.png)

![Jmeter27](images/27.png)

Una vez se tengan las configuraciones básicas para el recorder y el proxy local habilitado, se da click al boton arrancar:

![Jmeter28](images/28.png)

Una vez el recorder arranca, está listo para capturar todas las peticiones realizadas a una aplicación web o un servicio web (en la imagen se observan las peticiones capturadas para el login del visor de aplicaciones). Teniendo todo esto, se procede a navegar por la aplicación y a realizar las funcionalidades que serán probadas o en el caso de SoapUI, se procede a consumir el servicio desde la aplicación. Al terminar, se deberá dar click en el botón parar para que el recorder termine la grabación. 
Luego de obtener los request necesarios para replicar un escenario de prueba, de deberá hacer las configuraciones de datos, cabeceras, hilos y demás que sean pertinentes para que el testplan funcione como se espera. 

###9. Correr Test plan de Jmeter

Modo GUI

Para ejecutar el testplan, basta con dar clic en el botón play para ejecutar inmediatamente:

![Jmeter29](images/29.png)

Modo Non GUI

Existe la posibilidad de iniciar un testplan sin necesidad de hacer uso de la interfaz gráfica de Jmeter, esta es la manera como se ejecutarán las pruebas de rendimiento desde Jenkins:

1. Abrir la consola de comandos y ubicarnos en el directorio bin de Jmeter:

![Jmeter30](images/30.png)

2. Escribir la siguiente línea: 
jmeter -n -t RutaTestPlan\NombreTestPlan.jmx -l RutaTestPlan\NombreTestPlan \Seus_result.jtl
En donde:
-n: Indica a Jmeter que debe ejecutarse en modo Non GUI
-t: Especifica la ruta donde se encuentra el testplan que se quiere ejecutar
-l: Indica a Jmeter guardar un archivo con los resultados en la ruta donde se encuentra el testplan:

![Jmeter31](images/31.png)

El test se ejecuta en mono non GUI y guarda los resultados en la ruta que le indicamos.


###10. Ejecución de pruebas desde jenkins

Para ejecutar las pruebas en Jenkins se debe tener en cuenta lo siguiente:

• Si son pruebas programadas se debe garantizar que en el grupo de hilos estén bien configurados la cantidad de hilos (usuarios), el periodo de subida y duración en segundos. 

![Jmeter32](images/32.png)

Luego de tener listo el .jmx se debe subir al repositorio [Performance Test](https://bitbucket.org/suracore/performance_tests/src/master/) e informar al equipo **DevOps** para la creación del Job en Jenkins con la debida programación.

• Si son pruebas a demanda en el Job parametrizable, el grupo de hilos debe quedar configurado de la siguiente manera:

![Jmeter33](images/33.png)

Luego de tener listo el archivo .jmx que genera Jmeter, se debe subir al repositorio [Performance Test](https://bitbucket.org/suracore/performance_tests/src/master/) en la carpeta correspondiente. Si es necesario crear una carpeta se debe informar al equipo de **DevOps** para actualizar el job. Despues de esto se ingresa al [Job en Jenkins](https://jenkinscoreseguros.suramericana.com.co/blue/organizations/jenkins/Core_Seguros%2FPipeline%2FPerformance%2FPerformanceTest/activity) y lanzar el job, allí se deben elegir los opciones adecuadas para lanzar las pruebas, se debe tener en cuenta que en el ultimo parametro se debe ingresar el nombre de la BD a la que le pegan los servicios a probar:

![Jmeter34](images/34.png)

###11. Análisis de resultados

Luego de ejecutar las pruebas en Jenkins, en la pestaña *Artefacto* está el reporte de Jmeter y el de base de datos.

![Jmeter35](images/35.png)

En el reporte de Jmeter (Reporte Performance "Nombre aplicacion") en Dashboard podemos evidenciar la hora de inicio y fin de la prueba, y el resumen con las pruebas realizadas con un cuadro de estadisticas de cada uno de los servicios probados:

![Jmeter36](images/36.png)

![Jmeter37](images/37.png)

En el reporte de **Base de datos** (Reporte Performance DB "Nombre aplicacion") podemos encontrar el top de queries en la BD durante el tiempo de la prueba: 

![Jmeter38](images/38.png)
