# ESTRUCTURA DEL PROYECTO #

[Volver al menu](politicasAutomatizacionProyectoBDD)

- IMPORTANTE! Por favor NO alterar la arquitectura (directorios, etc) definida para el proyecto. Si hay alguna sugerencia para cambiar o modificar comentar al equipo para analizar el impacto en el proyecto.

## ESTRUCTURA DE DIRECTORIOS ##
La distribución de directorios se presenta a continuación:

- A nivel **general** se presentan a continuación la estructura de directorios que están incluidos tanto en: "main" como en "test":

![EstructuraBDD_produccion_01_20190128.PNG](https://bitbucket.org/repo/akpjXAz/images/2025351637-EstructuraBDD_produccion_01_20190128.PNG)


### Defnitions ###
- A continuación se indica la estructura de directorios que están incluidos dentro de la capa de: "**definitions**". En esta capa se tienen directorios por cada release [autos / empresariales] y luego se tiene directorios por cada frente de trabajo:
[pendiente: evaluar sí se requiere un ajuste en esta estructura]

![EstructuraBDD_produccion_03Def_20190128.PNG](https://bitbucket.org/repo/akpjXAz/images/2838271634-EstructuraBDD_produccion_03Def_20190128.PNG)


### Pages ###
- A continuación se indica la estructura de directorios que están incluidos dentro de la capa de: "**pages**". En esta capa se tienen directorios por cada aplicación [cotizador / groupcenter / guidewire] y luego se tiene directorios por cada módulo [billingcenter / policycenter / claimscenter --> para lo que es: GuideWire ].

**Notas**:

- Se tiene un directorio llamado "general" que es donde se ubica el GeneralPage que es el archivo de donde se parte la automatización de nuevos Page.

- Recuerde que en el archivo GeneralPage se debe consignar los mapeos y métodos que apliquen para cualquier page que esté incluido en cualquier carpeta dentro de: /Pages/. Ejemplo: en el GeneralPage se deben incluir los métodos que tengan funcionalidades a nivel de todas las aplicaciones (tanto para Group Center como para Guidewire como para las demás).

- En la raíz de los directorios "guidewire" y "groupcenter" se ubican los Page genéricos para cada una de éstas aplicaciones (y en cada uno de dichos Page: se incluyen mapeos y métodos que prestan una funcionalidad a nivel de toda la aplicación). Ejemplo: en la raíz de la carpeta de "groupcenter" se deben incluir los Page que tengan funcionalidades a nivel de toda la aplicación de Group Center.

![EstructuraBDD_produccion_04Pages_20190128.PNG](https://bitbucket.org/repo/akpjXAz/images/3186800176-EstructuraBDD_produccion_04Pages_20190128.PNG)


### Steps ###
- A continuación se indica la estructura de directorios que están incluidos dentro de la capa de: "**steps**". En esta capa se tienen directorios por cada aplicación [cotizador / groupcenter / guidewire] y luego se tiene directorios por cada módulo [autos / empresariales --> para lo que es: GroupCenter ].

**Notas**:

A diferencia de la capa de "pages", para la capa de "steps" No se cuenta aún con un directorio llamado "general"

En la raíz de los directorios "guidewire" y "groupcenter" se ubican los Steps genéricos para cada una de éstas aplicaciones (y en cada una se incluyen los métodos que exponen pasos que pueden ser usados de forma transversal)

![EstructuraBDD_produccion_05Steps_20190128.PNG](https://bitbucket.org/repo/akpjXAz/images/3221060304-EstructuraBDD_produccion_05Steps_20190128.PNG)


## UBICACIÓN DE ARCHIVOS - Feature ##
- La distribución de los .feature en el proyecto están a nivel de Release (Autos y Empresariales) siendo esta la capa de CAPABILITIES, luego esta la capa de FEATURES que hace referencia a los procesos (asesoriaventa, procesosfinancieros, reaseguro, evaluacionexpedicion) y los .feature estaran dentro del proceso al que pertenezcan. 

![features.png](https://bitbucket.org/repo/Lodj655/images/2917502762-features.png)


## UBICACIÓN DE ARCHIVOS - Runner ##
- Los Runners estarán divididos a directorios a nivel de ambiente [desarrollo (dllo) y laboratorio (uat)], luego a nivel de aplicación (groupcenter, policycenter, billingcenter, entre otros), y por último a nivel de lotes [en este nivel se sugiere usar dos sufijos: uno que indique el release (autos o empresariales) y otro, al final, que indique la funcionalidad que es probada con los runner incluidos en el lote]. Ejemplo: java --> com.sura.produccion.runners --> uat --> policycenter --> lote1autospolizanuevariesgoestandar


## CAPAS DE AUTOMATIZACIÓN ##
- En la capa  de Steps deben estar los ASSERT a nivel de negocio y en la capa de los Page deben estar los ASSERT a nivel de aplicación [es decir a un nivel más técnico (objetos con los que se interactúa en pantalla)]

- Tener en cuenta los conceptos FIRST y A-A-A [Arrange - Act -Assert]

## ESTRUCTURA FEATURE ##
- La narrativa debe estar compuesta así: (Como [rol específico] -	Quiero [objetivo], Para [beneficio]).

- La descripción de cada uno de los pasos que conforman un escenario (Dado/Cuando/Entonces) deben contener lenguaje entendible por el negocio.

- En un escenario NO es obligatorio tener el "DADO", ya que el escenario puede llegar a carecer de precondiciones. El Cuando y el Entonces si son obligatorios. [Se recomienda hacer un análisis previo antes de omitir un paso "DADO")

- Dada la necesidad de incluir datos de prueba en el Gherkin de los escenarios (.feature) con fines **netamente informativos** (para ser mostrados en el reporte de las evidencias); se acuerda que se puede hacer uso de dichos datos de prueba **pero** se debe procurar que se reunan varios datos prueba en un mismo parámetro de un Definitions. A continuación un Ejemplo:
![Img_ejm_datosInformativos_en_Gherkin_20180808.PNG](https://bitbucket.org/repo/Lodj655/images/3578742230-Img_ejm_datosInformativos_en_Gherkin_20180808.PNG)

![Img_ejm_datosInformativos_en_Gherkin_20180808_b.PNG](https://bitbucket.org/repo/Lodj655/images/3411336240-Img_ejm_datosInformativos_en_Gherkin_20180808_b.PNG)


## ESTRUCTURA CSV ##
- Los archivos CSV deben estar separados por el caracter punto y coma ( ; )

- Los archivos CSV deben tener por lo menos las siguientes columnas: "id" (que sirve como conteo de los registros),  "idfiltro" (que corresponde a la columna por la cual se filtrarán los registros) y seguidamente: las columnas correspondientes a los datos de prueba que contendrá el archivo

## ESTRUCTURA MÉTODOS ##
- El número permitido de parámetros por cada método es de: **7**.  Excepción: Solo se permitirán más del tope en la capa de Definitions y **sólo** para automatización relacionada con consumo de servicios.

- Recomendaciones frente al tema de recibo de parámetros:
Se recomienda hacer un análisis para definir si es posible distribuir el envío de parámetros de una forma homogénea desde el .feature. También se recomienda analizar sí existen valores (parámetros) de menor relevancia para la ejecución del escenario y ser delegados a un archivo CSV


[Volver al menu](politicasAutomatizacionProyectoBDD)