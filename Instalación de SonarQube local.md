# Instalación de SonarQube local #

[Volver al home](Home)

* Descargar el aplicativo del siguiente link:
SonarQube:
https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-6.7.6.zip
SonarScanner: https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-windows.zip 
* Descomprimir el archivo preferiblemente en la raiz “C:/” deben quedar dos carpetas, sonarqube y sonar-scanner.

* Agregar la variable de entorno “SONAR_HOME” apuntando al directorio sonar-scanner

![VariableEntorno.png](https://bitbucket.org/repo/Lodj655/images/1510791685-VariableEntorno.png)

* Agregar en el path la variable creada. %SONAR_HOME%\bin

![Path.png](https://bitbucket.org/repo/Lodj655/images/378977470-Path.png)

## Configurar proyecto para su análisis ##
* ir la ruta donde se instaló Sonar Scanner e ingresar a la carpeta conf (D:\sonar\sonar-scanner\conf)
editar el archivo sonar-scanner.properties descomentando la línea 
sonar.host.url=http://localhost:9000 
agregar las siguientes líneas:
sonar.projectKey=bddcoresuraproduccion (Reemplazar "bddcoresuraproduccion" por el nombre del proyecto que desee escanear)
sonar.projectName=bddcoresuraproduccion (Reemplazar "bddcoresuraproduccion" por el nombre del proyecto que desee escanear)
sonar.projectVersion=1.0
sonar.sources=src/
sonar.language=java
sonar.sourceEncoding=UTF-8
sonar.java.binaries=.

## Ejecución de SonarQube ##

* Para iniciar el servicio de SonarQube, ejecutamos el archivo StartSonar.bat ubicado en la ruta C:\sonarqube\bin\windows-x86-64\StartSonar.bat
![rutasonar.png](https://bitbucket.org/repo/akpjXAz/images/4015984912-rutasonar.png)

* Esto abrirá una consola, la cual estará activa durante todo el tiempo que esté arriba el servicio de sonar, el servicio se puede detener usando las teclas CTRL+C.
![consolasonar.png](https://bitbucket.org/repo/akpjXAz/images/1728988634-consolasonar.png)

* Por último, abrimos sonar usando la URL: http://localhost:9000/ 
Las credenciales por defecto son admin/admin.
![sonar.png](https://bitbucket.org/repo/akpjXAz/images/1255007951-sonar.png)

## Instalar los plugins ##
* Para instalar los plugins, se debe ir al men{u administration, MarketPlace y buscar e instalar los siguientes plugins:

*Android
*FindBugs
*Checkstyle

una vez instalados es necesario reiniciar el servidor de SonarQube deteniendo la ejecuciòn desde la consola (Crtl + C) y relanzando con el archivo StartSonar.bat.

## Agregar reglas definidas por Sura ##

* Descargar el archivo XML con las reglas de este [Link](https://drive.google.com/open?id=1WV8B8yfmRSJWyVl9A116ZqaVZc0xWej1) 

* Ingresar nuevamente a la url del servidor (http://localhost:9000/) usando las credenciales admin/admopciòn Ingresar a la opción Quality Profiles, hacer clic en el botón ▼  y luego en Restore Profile.

![qualityprofiles.png](https://bitbucket.org/repo/akpjXAz/images/2727937014-qualityprofiles.png)

* En la ventana emergente deberá cargar el archivo XML con las reglas.

* Una vez importadas las reglas, deberá aparecer la opción SuraJava en el listado de profiles.

![qualityprofiles2.png](https://bitbucket.org/repo/akpjXAz/images/1142440907-qualityprofiles2.png)

* Realizar un escaneo inicial del proyecto como se especifica en el siguiente titulo.

* Volver a SonarQube y hacer clic en el perfil SuraJava, en la secciòn projects hacer clic sobre el botón ChangeProjects, en la ventana emergente hacer clic en All y checkear la casilla para asociar el perfil con el proyecto BDDCoreSuraProduccion.

![changeprojects.png](https://bitbucket.org/repo/akpjXAz/images/961471883-changeprojects.png)

* Por ultimo realizar nuevamente un escaneo para que éste se haga teniendo en cuenta las reglas de Sura.

## Escanear proyecto con Sonar ##

Es importante que antes de escanear se esté actualizado con develop. Para escanear el proyecto con el fin de actualizar la medición del sonar una vez se hayan hecho las correcciones, abrimos una terminal de Windows y navegamos a la raíz del proyecto, luego ejecutamos el comando “sonar-scanner.bat”

![sonarscanner.png](https://bitbucket.org/repo/akpjXAz/images/728559084-sonarscanner.png)

* Una vez escaneado el proyecto, el resultado se puede consultar en el servidor local de SonarQube.

![sonarscanner2.png](https://bitbucket.org/repo/akpjXAz/images/2004310186-sonarscanner2.png)

![sonarscanner3.png](https://bitbucket.org/repo/akpjXAz/images/2289954584-sonarscanner3.png)

## Integración de SonarQube con intelliJ ## 

Una vez se tenga instalado SonarQube en forma local, se puede integrar a IntelliJ mediante el plugin SonarQube community.
* El plugin se instala mediante la opción, File-> Settings -> Plugins-> Browse repositories-> SonarQube Community Plugin.
* Una vez instalado el plugin, aparecerá en el menú “File-> Settings”, la opción “SonarQube”, allí se debe configurar el SonarQube Server y el SonarQube resources con los siguientes parámetros:

- SonarQube Server

![sonarplugin.png](https://bitbucket.org/repo/Lodj655/images/3000893056-sonarplugin.png)

- SonarQube Resources
Luego de agregar el SonarQube Server, se agrega el SonarQube resource, mediante las opciones “+-> Download resources-> bddsuracoreproduccion-> ok”

![sonarpluginresources.png](https://bitbucket.org/repo/Lodj655/images/3128141237-sonarpluginresources.png)

- Analizar el proyecto
Para analizar el código desde el IDE Intellij, damos click derecho a la raíz del proyecto, seleccionamos la opción “Analize -> Inspect Code” y parametrizamos el análisis de la siguiente manera:
Seleccionamos las opciones “Whole Project” e “Include test resources”

![sonarpluginanalisis.png](https://bitbucket.org/repo/Lodj655/images/760774542-sonarpluginanalisis.png)

En el apartado Inspection profile, dejamos marcadas solo las opciones “SonarQube” y “SonarQube (new issues)”

![sonarpluginanalisis2.png](https://bitbucket.org/repo/Lodj655/images/2212737890-sonarpluginanalisis2.png)

Al finalizar el análisis, aparecerán los resultados en la parte inferior con la posibilidad de acceder directamente al apartado del código donde se encuentra el error

[Volver al home](Home)