# Check List de Revisión de Pull Request en Guidewire

[Volver al home](Home)


1. Validar buenas prácticas de programación.
2. Adopción e interpretación de patrones.
3. Verificar implementación de los diferentes accesos a datos a través de las entidades.
4. Verificar elaboración de pruebas unitarias y haciendo la debida extensión de EESuraTestBase.
5. Verificar que las pruebas unitarias evalúan todos los escenarios posibles.
6. Validar que cada escenario de prueba cumple con la estructura **"Cuando" + "Donde" + "Entonces"**
7. Validar que los logs generados den valor, no deben haber print, si son logs que van a splunk deben enviar sólo información valiosa (Excepciones o erroes por ejemplo).
8. Validar elaboración de pruebas de integración cuando sea el caso.
9. Validar que el código creado/modificado en el Pull Request no tiene hallazgos en el Analizador de Queries. [Query Validator](Query%20Validator)

[Volver al home](Home)